<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Guía interactiva para el Museo de Informática">
<meta name="author" content="GuIMI Enterprise">
<link rel="shortcut icon" type="image/png" href="recursos/img/favicons/favicon.ico" />

<!--Bootstrap v4.6.1-->
<link rel="stylesheet" href="../lib/css/bootstrap_4.6.1.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css" integrity="sha256-uxjsS9cYFLTjmlr8j5i+DqvOLCBugRzEeWxUMWZeYXQ=" crossorigin="anonymous">
<link rel="stylesheet" href="../lib/fontawesome/css/all.min.css">

<script type="text/javascript" src="../lib/js/jquery-3.6.0.min.js"></script>
<script src="../lib/js/bootstrap_4.6.1.bundle.min.js"></script>
<script src="../lib/js/funciones.min.js"></script>


<link rel="stylesheet" href="../lib/css/estilos.css">
<script type="application/javascript" src="../lib/fontawesome/js/all.min.js"></script>
