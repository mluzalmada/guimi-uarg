<?php

//FUNCIONES MANEJO DE ARCHIVOS
function getTamanioArchivo($archivo) {
    return $archivo['size'];
}

function getNombreArchivo($archivo) {
    return $archivo['name'];
}

function getNombreTemporalArchivo($archivo) {
    return $archivo['tmp_name'];
}

function getExtensionArchivo($archivo) {
    $extension = pathinfo(getNombreArchivo($archivo), PATHINFO_EXTENSION);
    return strtolower($extension);
}

function getTipoMime($archivo) {
    return $archivo['type'];
}

function getErroresArchivo($archivo) {
    return $archivo['error'];
}

function validarFormatoImagen($archivo) {
    $extension = getExtensionArchivo($archivo);
    if ($extension == "jpg") {
        return true;
    }
    if ($extension == "jpeg") {
        return true;
    }
    if ($extension == "png") {
        return true;
    }
    return false;
}

// GUARDADO DE ARCHIVOS
function guardarArchivo($archivo, $directorio, $nombre_nuevo) {
    $archivo_temporal = getNombreTemporalArchivo($archivo);
    if (!file_exists($directorio)) {
        if(!mkdir($directorio, 0777, true)) {
            return false; 
        }
    }
    if(move_uploaded_file($archivo_temporal, $directorio.$nombre_nuevo)) {
        chmod($directorio.$nombre_nuevo, 0666);
        return true;
    }
    return false;
}
// CREACIÓN DE DIRECTORIOS
function crearDirectorio($ubicacion) {
    if (!file_exists($ubicacion)) {
        mkdir($ubicacion, 0777, true);
    }
}

?>