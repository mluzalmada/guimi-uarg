<?php
include_once '../modelo/BDConexion.Class.php';

//ACCIONES: 1: Creación; 2: Edición; 3: Eliminación/archivación
function RegistrarActualizacionElemento($accion, $elemento_id, $comentarios = null) {
    $usuario_id = $_SESSION['usuario_id'];
    $fecha_hora_actual = date("Y-m-d H:i:s");
    $query_actualizacion = "INSERT INTO actualizacion_elemento ". 
        " (usuario_id, elemento_id, observacion, tipo, fecha_hora)".
        " VALUES('$usuario_id', '$elemento_id', '$comentarios', '$accion', '$fecha_hora_actual');";
    
    if ($actualizar_elemento = BDConexion::getInstancia()->query($query_actualizacion)) {
        return BDConexion::getInstancia()->insert_id;
    }
    else return false;
}

//ACCIONES: 1: Creación; 2: Edición; 3: Eliminación/archivación
function RegistrarActualizacionNovedad($accion, $novedad_id, $comentarios = null) {
    $usuario_id = $_SESSION['usuario_id'];
    $fecha_hora_actual = date("Y-m-d H:i:s");
    $query_actualizacion = "INSERT INTO actualizacion_novedad ". 
        " (usuario_id, novedad_id, observacion, tipo, fecha_hora)".
        " VALUES('$usuario_id', '$novedad_id', '$comentarios', '$accion', '$fecha_hora_actual');";
    if ($actualizar_novedad = BDConexion::getInstancia()->query($query_actualizacion)) {
        return BDConexion::getInstancia()->insert_id;
    }
    else return false;
}

?>