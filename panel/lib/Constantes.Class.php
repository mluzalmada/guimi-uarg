<?php

setlocale(LC_TIME, 'es_AR.utf8');

/**
 * 
 * Clase para mantener las directivas de sistema.
 * Deben coincidir con las configuraciones del proyecto.
 * 
 * @author Eder dos Santos <esantos@uarg.unpa.edu.ar>
 * 
 */
class Constantes {

    
    const NOMBRE_SISTEMA = "GuIMI";
    
    const WEBROOT = "/var/www/guimi/admin/panel";
    const APPDIR = "panel";
//        
//    const SERVER = "https://admin.guimi.com.ar";
//    const APPURL = "https://admin.guimi.com.ar";
//    const HOMEURL = "https://admin.guimi.com.ar/app/index.php";
//    const HOMEAUTH = "https://admin.guimi.com.ar/app/menu_principal.php";
//    
//    const BD_SCHEMA = "guimibd";
//    const BD_USERS = "guimibd";
    
    // Configuración local: 
    const SERVER = "http://localhost";
    const APPURL = "http://localhost/guimi-admin/panel";
    const HOMEURL = "http://localhost/guimi-admin/panel/app/index.php";
    const HOMEAUTH = "http://localhost/guimi-admin/panel/app/menu_principal.php";
    
    const BD_SCHEMA = "bdguimi";
    const BD_USERS = "bdguimi";
}
