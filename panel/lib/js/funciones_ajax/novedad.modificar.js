$(function () {
    console.log('modificar novedad ajax');
    $('#form-editar_novedad').bind('submit', function () {
        var id = $('#id').val(),
            formulario = $(this).get(0),
            formData = new FormData(formulario),
            foto_ruta = $('#foto_ruta')[0].files[0];
        console.log('editar' + id);
        formData.append('foto_ruta',foto_ruta);

        $.ajax({
            type: 'post',
            url: 'novedad.modificar.procesar.php',
            processData: false,
            contentType: false,
            data: formData,
            beforeSend: function () {
                $('.loader').show();
            },
            complete: function () {
                $('.loader').hide();
            },
            success: function (data) {
                console.log(data);
                var icon = '#modal-icon',
                    titulo = '#modal-titulo',
                    subtitulo = '#modal-subtitulo',
                    contenido = '#modal-form',
                    mostrar_error = '#modal-errores',
                    mostrar_detalle = '#modal-detalle',
                    boton_cerrar = '#modal-btn_cerrar';

                var resultado = jQuery.parseJSON(data);
                $(contenido).hide('slow');
                if (resultado.actualizado) {
                    $(icon).removeClass('fa-trash-can text-danger').addClass('fa-check text-success');
                    $(titulo).html('Novedad actualizada con &eacute;xito.');
                    $(mostrar_detalle).html('');
                    $(mostrar_error).hide();
                    $(boton_cerrar).show('slow');
                    if(resultado.transaccion) {
                        $(subtitulo).html('TRANSACCIÓN NÚMERO ' + resultado.transaccion).addClass('text-muted small');
                    }
                } else {
                    $(icon).removeClass('fa-trash-can text-success').addClass('fa-xmark text-danger');
                    $(titulo).html('Ha ocurrido un error.');
                    $(subtitulo).html('');
                    $(mostrar_detalle).html(resultado.datos_error);
                    $(mostrar_error).show();
                    $(boton_cerrar).show('slow');
                }
                $('#modal-respuesta').modal();
            },
            error: function (data) {
                console.log(data);
                var icon = '#modal-icon',
                    titulo = '#modal-titulo',
                    subtitulo = '#modal-subtitulo',
                    contenido = '#modal-form',
                    boton_cerrar = '#modal-btn_cerrar';

                $(icon).removeClass('fa-trash-can text-success').addClass('fa-xmark text-danger');
                $(titulo).html('Ha ocurrido un error.');
                $(subtitulo).html('');
                $(boton_cerrar).show('slow');
                $('#modal-respuesta').modal();
            },
        });
        return false;
    });
});
