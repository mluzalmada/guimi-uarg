$(function () {
    console.log('modificar elemento ajax');
    $('#form-editar_contenido').bind('submit', function () {
        var id = $('#id').val(),
            formulario = $(this).get(0),
            formData = new FormData(formulario),
            archivo = $('#archivo')[0].files[0];
        console.log('editar contenido:' + id);
        formData.append('archivo',archivo);

        $.ajax({
            type: 'post',
            url: 'contenido.modificar.procesar.php',
            processData: false,
            contentType: false,
            data: formData,
            beforeSend: function () {
                $('.loader').show();
            },
            complete: function () {
                $('.loader').hide();
            },
            success: function (data) {
//                console.log("SUCCESS = " + data);
                var icon = '#modal-icon',
                    titulo = '#modal-titulo',
                    subtitulo = '#modal-subtitulo',
                    contenido = '#modal-form',
                    boton_cerrar = '#modal-btn_cerrar';

                var resultado = jQuery.parseJSON(data);
                $(contenido).hide('slow');
                if (resultado.actualizado) {
                    $(icon).removeClass('fa-trash-can text-danger').addClass('fa-check text-success');
                    $(titulo).html('Contenido actualizado con &eacute;xito.');
                    $(subtitulo).html('TRANSACCIÓN NÚMERO ' + resultado.transaccion).addClass('text-muted small');
                    $(boton_cerrar).show('slow');
                } else {
                    $(icon).removeClass('fa-trash-can').addClass('fa-xmark');
                    $(titulo).html('Ha ocurrido un error.');
                    $(subtitulo).html('');
                    $(boton_cerrar).show('slow');
                }
                $('#modal-respuesta').modal();
            },
            error: function (data) {
//                console.log("error:" + data);
                var icon = '#modal-icon',
                    titulo = '#modal-titulo',
                    subtitulo = '#modal-subtitulo',
                    contenido = '#modal-form',
                    boton_cerrar = '#modal-btn_cerrar';

                $(icon).removeClass('fa-trash-can text-success').addClass('fa-xmark text-danger');
                $(titulo).html('Ha ocurrido un error.');
                $(subtitulo).html('');
                $(boton_cerrar).show('slow');
                $('#modal-respuesta').modal();
            },
        });
        return false;
    });
});
