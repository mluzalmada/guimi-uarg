$(function () {
    console.log('crear elemento ajax');
    $('#form-crear_marcador').bind('submit', function () {
        var formulario = $(this).get(0),
            formData = new FormData(formulario),
            imagen_ruta = $('#imagen_ruta')[0].files[0],
            patron_ruta = $('#patron_ruta')[0].files[0];
        formData.append('imagen_ruta',imagen_ruta);
        formData.append('patron_ruta',patron_ruta);

        $.ajax({
            type: 'post',
            url: 'marcador.crear.procesar.php',
            processData: false,
            contentType: false,
            data: formData,
            beforeSend: function () {
                $('.loader').show();
            },
            complete: function () {
                $('.loader').hide();
            },
            success: function (data) {
                console.log('success:' + data);
                var icon = '#modal-icon',
                    titulo = '#modal-titulo',
                    subtitulo = '#modal-subtitulo',
                    contenido = '#modal-form',
                    boton_cerrar = '#modal-btn_cerrar';

                var resultado = jQuery.parseJSON(data);
                $(contenido).hide('slow');
                if (!resultado.errores) {
                    $(icon).removeClass('fa-trash-can text-danger').addClass('fa-check text-success');
                    $(titulo).html('Marcador actualizado con &eacute;xito.');
                    $(boton_cerrar).show('slow');
                    if(resultado.transaccion) {
                        $(subtitulo).html('TRANSACCIÓN NÚMERO ' + resultado.transaccion).addClass('text-muted small');
                    }
                } else {
                    $(icon).removeClass('fa-trash-can').addClass('fa-xmark');
                    $(titulo).html('Ha ocurrido un error');
                    $(subtitulo).html(resultado.errores).addClass('text-muted small');
                    $(boton_cerrar).show('slow');
                }
                $('#modal-respuesta').modal();
            },
            error: function (data) {
                console.log('error:' + data);
                var icon = '#modal-icon',
                    titulo = '#modal-titulo',
                    subtitulo = '#modal-subtitulo',
                    contenido = '#modal-form',
                    boton_cerrar = '#modal-btn_cerrar';

                $(icon).removeClass('fa-trash-can text-success').addClass('fa-xmark text-danger');
                $(titulo).html('Ha ocurrido un error.');
                $(subtitulo).html('').addClass('text-muted small');
                $(boton_cerrar).show('slow');
                $('#modal-respuesta').modal();
            },
        });
        return false;
    });
});
