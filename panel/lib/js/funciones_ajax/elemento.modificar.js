$(function () {
    $('#form-editar_elemento').bind('submit', function () {
        var id = $('#id').val(),
            formulario = $(this).get(0),
            formData = new FormData(formulario),
            foto_ruta = $('#foto_ruta')[0].files[0];
        formData.append('foto_ruta',foto_ruta);

        $.ajax({
            type: 'post',
            url: 'elemento.modificar.procesar.php',
            processData: false,
            contentType: false,
            data: formData,
            beforeSend: function () {
                $('.loader').show();
            },
            complete: function () {
                $('.loader').hide();
            },
            success: function (data) {
//                console.log(data);
                var icon = '#modal-icon',
                    titulo = '#modal-titulo',
                    subtitulo = '#modal-subtitulo',
                    contenido = '#modal-form',
                    boton_cerrar = '#modal-btn_cerrar';

                var resultado = jQuery.parseJSON(data);
//                console.log(resultado);
                $(contenido).hide('slow');
                
                if (resultado.actualizado) {
                    $(icon).removeClass('fa-trash-can text-danger').addClass('fa-check text-success');
                    $(titulo).html('Elemento actualizado con &eacute;xito.');
                    $(subtitulo).html('TRANSACCIÓN NÚMERO ' + resultado.transaccion).addClass('text-muted small');
                    $(boton_cerrar).show('slow');
                } else {
                    $(icon).removeClass('fa-trash-can').addClass('fa-xmark');
                    $(titulo).html('Ha ocurrido un error.');
                    $(subtitulo).html('');
                    $(boton_cerrar).show('slow');
                }
                $('#modal-respuesta').modal();
            },
            error: function (data) {
                console.log(data);
                var icon = '#modal-icon',
                    titulo = '#modal-titulo',
                    subtitulo = '#modal-subtitulo',
                    contenido = '#modal-form',
                    boton_cerrar = '#modal-btn_cerrar';

                $(icon).removeClass('fa-trash-can text-success').addClass('fa-xmark text-danger');
                $(titulo).html('Ha ocurrido un error.');
                $(subtitulo).html('');
                $(boton_cerrar).show('slow');
                $('#modal-respuesta').modal();
            },
        });
        return false;
    });
});
