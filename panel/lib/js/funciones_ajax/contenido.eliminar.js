$(function () {
    $('form').bind('submit', function () {
        var id = $(this).attr('data-id'),
            formulario = $(this).serialize();
        $.ajax({
            type: 'post',
            url: 'contenido.eliminar.procesar.php',
            data: formulario,
            beforeSend: function () {
                $('.loader').show();
            },
            complete: function (data) {
                $('.loader').hide();
            },
            success: function (data) {
                console.log(data);
                var icon = '#modal-icon_' + id,
                    titulo = '#modal-titulo_' + id,
                    subtitulo = '#modal-subtitulo_' + id,
                    contenido = '#modal-form_' + id,
                    boton_cerrar = '#modal-btn_cerrar_' + id;

                var resultado = jQuery.parseJSON(data);
                $(contenido).hide('slow');
                if (resultado.eliminado) {
                    $(icon).removeClass('fa-trash-can text-danger').addClass('fa-check text-success');
                    $(titulo).html('Operaci&oacute;n realizada con &eacute;xito.');
                    $(subtitulo).html('TRANSACCIÓN NÚMERO ' + resultado.transaccion).addClass('text-muted small');
                    $(boton_cerrar).show('slow');
                } else {
                    $(icon).removeClass('fa-trash-can').addClass('fa-xmark');
                    $(titulo).html('Ha ocurrido un error.');
                    $(subtitulo).html('');
                    $(boton_cerrar).show('slow');
                }
            },
            error: function (data) {
                console.log(data);
                var icon = '#modal-icon_' + id,
                    titulo = '#modal-titulo_' + id,
                    subtitulo = '#modal-subtitulo_' + id,
                    contenido = '#modal-form_' + id,
                    boton_cerrar = '#modal-btn_cerrar_' + id;
                $(icon).removeClass('fa-trash-can').addClass('fa-xmark');
                $(titulo).html('Ha ocurrido un error.');
                $(subtitulo).html('');
                $(boton_cerrar).show('slow');
            },
        });
        return false;
    });
});
