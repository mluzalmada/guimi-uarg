$(function () {
    $('.loader').hide();
    if($('.input-estrellas:checked')) {
        ActualizarEstadoElemento();
        mostrarOcultarSegunTipoContenido();
    }

    $('.input-estrellas').change(ActualizarEstadoElemento);

    function ActualizarEstadoElemento() {
        var estado = $('.input-estrellas:checked').val();
        switch (estado) {
            case '1':
                $('#texto_estado').html("Estado Malo");
                break;
            case '2':
                $('#texto_estado').html("Estado Regular");
                break;
            case '3':
                $('#texto_estado').html("Estado Bueno");
                break;
            case '4':
                $('#texto_estado').html("Estado Muy bueno");
                break;
            case '5':
                $('#texto_estado').html("Estado Excelente");
                break;
        }
    }

    $('.label-tamanio_fisico').click(SeleccionarTamanioElemento);
    function SeleccionarTamanioElemento() {
        var tamanio = $(this).attr('data-tamanio');
        $('#tamanio_fisico').val(tamanio);
    }
    
    $('#tipo_contenido_id').change(mostrarOcultarSegunTipoContenido);
    function mostrarOcultarSegunTipoContenido() {
        switch($('#tipo_contenido_id').val()) {
            case '1': {
                $('#dimension_pixeles').parent().parent().show('slow').removeClass('col-md-4').addClass('col-md-12');
                $('#extension_caracteres').parent().parent().hide('slow');
                $('#duracion_tiempo').parent().parent().hide('slow');
                $('#archivo').attr('accept', 'image/*');
                break;
            }
            case '2': {
                $('#extension_caracteres').parent().parent().show('slow').removeClass('col-md-4').addClass('col-md-12');
                $('#dimension_pixeles').parent().parent().hide('slow');
                $('#duracion_tiempo').parent().parent().hide('slow');
                $('#archivo').attr('accept', '.txt');
                break;
            }
            case '3': {
                $('#duracion_tiempo').parent().parent().show('slow').removeClass('col-md-4').addClass('col-md-12');
                $('#extension_caracteres').parent().parent().hide('slow');
                $('#dimension_pixeles').parent().parent().hide('slow');
                $('#archivo').attr('accept', 'audio/*');
                break;
            }
            case '4': {
                $('#dimension_pixeles').parent().parent().show('slow').removeClass('col-md-4 col-md-12').addClass('col-md-6');
                $('#duracion_tiempo').parent().parent().show('slow').removeClass('col-md-4 col-md-12').addClass('col-md-6');
                $('#extension_caracteres').parent().parent().hide('slow');
                $('#archivo').attr('accept', 'video/*');
                break;
            }

        }

    }
    
    $('#info_adicional_texto').change(mostrarOcultarSegunInfoAdicional);
    $('#info_adicional_link').change(mostrarOcultarSegunInfoAdicional);
    mostrarOcultarSegunInfoAdicional();
    
    function mostrarOcultarSegunInfoAdicional() {
        var mostrar = $('input[name="tipo_info_adicional"]:checked').val(),
            info_bd = $('#informacion_adicional_guardada').val();
        switch(mostrar) {
            case 'link': {
                $('#informacion_adicional').val("");
                $('#informacion_adicional_link').val("");
                $('#informacion_adicional_texto').val("");
                $('#informacion_adicional').hide('slow');
                $('#informacion_adicional_link').show('slow');
                $('#informacion_adicional_texto').show('slow');
                break;
            }
            case 'texto': {
                $('#informacion_adicional').val(info_bd);
                $('#informacion_adicional_link').hide('slow');
                $('#informacion_adicional_texto').hide('slow');
                $('#informacion_adicional_vista_previa').hide('slow');
                $('#informacion_adicional').show('slow');
                break;
            }
        }
        false;
    }
    
    $('#informacion_adicional_texto').keyup(autocompletarInfoAdicional);
    $('#informacion_adicional_link').keyup(autocompletarInfoAdicional);
    
    function autocompletarInfoAdicional() {
        var link = $('#informacion_adicional_link').val(),
            texto = $('#informacion_adicional_texto').val(),
            html = '<a href="http://' + link + '" target="_blank">' + texto + '</a>';
        if($('#informacion_adicional_texto').val()) {
            $('#informacion_adicional_vista_previa').html("<span class='font-uppercase font-weight-bold text-muted'>VISTA PREVIA: </span>" + html);
            $('#informacion_adicional').val(html);
            $('#informacion_adicional_vista_previa').show('slow');
        }
    }

});
