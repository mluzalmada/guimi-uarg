<?php
include_once 'BDObjetoGenerico.Class.php';
include_once 'BDConexion.Class.php';

class Novedad extends BDObjetoGenerico {

    protected $id;
    protected $nombre;
    protected $descripcion;
    protected $lugar;
    protected $fecha_inicio;
    protected $fecha_fin;
    protected $fecha_notificar;
    protected $visible;
    protected $foto_ruta;
    protected $informacion_adicional;
    protected $existe = true;
    
    protected $datos; 


    function __construct($id = null, $datos = null) {
//        parent::__construct($id, "novedad");
        if(isset($datos)) { // Alta
            // Guardar en la BD -->
            $this->datos = $datos;
            BDConexion::getInstancia()->autocommit(false);
            BDConexion::getInstancia()->begin_transaction();

            $query_novedad = "INSERT INTO  ". Constantes::BD_SCHEMA.".novedad ".
                " (nombre, descripcion, lugar, fecha_inicio, fecha_fin, fecha_notificar, visible, informacion_adicional) ".
                " VALUES('{$datos["nombre"]}', '{$datos["descripcion"]}', '{$datos["lugar"]}', '{$datos["fecha_inicio"]}', '{$datos["fecha_fin"]}', '{$datos["fecha_notificar"]}', '{$datos["visible"]}', '{$datos["informacion_adicional"]}');";
            $insertar_novedad = BDConexion::getInstancia()->query($query_novedad);
            
            $id = (Int) BDConexion::getInstancia()->insert_id;
            
            BDConexion::getInstancia()->commit();
            BDConexion::getInstancia()->autocommit(true);
            if($insertar_novedad){
                //setear valores del objeto    
                $this->setId($id);
                $this->setNombre($datos['nombre']);
                $this->setDescripcion($datos['descripcion']);
                $this->setLugar($datos['lugar']);
                $this->setFechaInicio($datos['fecha_inicio']);
                $this->setFechaFin($datos['fecha_fin']);
                $this->setFechaNotificar($datos['fecha_notificar']);
                $this->setInformacionAdicional($datos['informacion_adicional']);
                $this->setVisible($datos['visible']);
            }
            else {
//                echo "error al guardar en la bd";
                $this->existe = false;
            }
            $this->getObjeto($id);
        }
        else {
            if(isset($id)) {
                $this->getObjeto($id);
            }
            else {
                $this->existe = false;
            }
        }
    }


    function getObjeto($id) {
        $this->id = $id;
        $this->query = "select * from novedad where id = '{$this->id}'";
        $this->datos = BDConexion::getInstancia()->query($this->query);
        $this->datos = $this->datos->fetch_assoc();
        if($this->datos) {
            foreach ($this->datos as $atributo => $valor) {
                $this->{$atributo} = $valor;
            }            
        }
        else {
        $this->existe = false;
        }
        unset($this->query);
        unset($this->datos);
    }
    
    function deleteNovedad() {
        BDConexion::getInstancia()->autocommit(false);
        BDConexion::getInstancia()->begin_transaction();

        $eliminar_novedad = BDObjetoGenerico::deleteTablaBD('novedad','id', $this->id);
        
        if ($eliminar_novedad) {
            BDConexion::getInstancia()->commit();
            BDConexion::getInstancia()->autocommit(true);
            return true;
        }
        BDConexion::getInstancia()->rollback();
        return false;
    }
    
    function getNombre() {
        return $this->nombre;
    }
    function setNombre($nombre) {
        $this->nombre = $nombre;
        $actualizado = BDObjetoGenerico::setTablaBD('novedad','nombre',$nombre,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    
    function getDescripcion() {
        return $this->descripcion;
    }
    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
        $actualizado = BDObjetoGenerico::setTablaBD('novedad','descripcion',$descripcion,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    
    function getLugar() {
        return $this->lugar;
    }
    function setLugar($lugar) {
        $this->lugar = $lugar;
        $actualizado = BDObjetoGenerico::setTablaBD('novedad','lugar',$lugar,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    
    function getFechaInicio() {
        return $this->fecha_inicio;
    }
    function setFechaInicio($fecha_inicio) {
        $this->fecha_inicio = $fecha_inicio;
        $actualizado = BDObjetoGenerico::setTablaBD('novedad','fecha_inicio',$fecha_inicio,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    
    function getFechaFin() {
        return $this->fecha_fin;
    }
    function setFechaFin($fecha_fin) {
        $this->fecha_fin = $fecha_fin;
        $actualizado = BDObjetoGenerico::setTablaBD('novedad','fecha_fin',$fecha_fin,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    
    function getFechaNotificar() {
        return $this->fecha_notificar;
    }
    function setFechaNotificar($fecha_notificar) {
        $this->fecha_notificar = $fecha_notificar;
        $actualizado = BDObjetoGenerico::setTablaBD('novedad','fecha_notificar',$fecha_notificar,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    
    function getVisible() {
        return $this->visible;
    }
    function setVisible($visible) {
        $this->visible = $visible;
        $actualizado = BDObjetoGenerico::setTablaBD('novedad','visible',$visible,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }

    function getFotoRuta() {
        return $this->foto_ruta;
    }
    function setFotoRuta($foto_ruta) {
        $this->foto_ruta = $foto_ruta;
        $actualizado = BDObjetoGenerico::setTablaBD('novedad','foto_ruta',$foto_ruta,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }

    function getInformacionAdicional() {
        return $this->informacion_adicional;
    }
    function setInformacionAdicional($informacion_adicional) {
        $this->informacion_adicional = $informacion_adicional;
        $actualizado = BDObjetoGenerico::setTablaBD('novedad','informacion_adicional',$informacion_adicional,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }

    function getExiste() {
        return $this->existe;
    }
    function getDatos() {
        return $this->datos;
    }
    
}
