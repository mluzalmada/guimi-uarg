<?php
include_once 'BDObjetoGenerico.Class.php';
include_once 'BDConexion.Class.php';

class Contenido extends BDObjetoGenerico {
    
    protected $id;
    protected $nombre;
    protected $descripcion;
    protected $visible;
    protected $duracion_tiempo;
    protected $dimension_pixeles;
    protected $extension_caracteres;
    protected $elemento_id;
    protected $tipo_contenido_id;
    protected $fuente_atribucion;

    protected $datos; 

    function __construct($id = null, $datos = null) {
//        parent::__construct($id, "elemento");
        if(isset($datos)) { // Alta
            // Guardar en la BD -->
            BDConexion::getInstancia()->autocommit(false);
            BDConexion::getInstancia()->begin_transaction();
           
            $query_contenido = "INSERT INTO  ". Constantes::BD_SCHEMA.".contenido "
                ."(nombre, descripcion, visible, duracion_tiempo, dimension_pixeles, extension_caracteres, elemento_id, tipo_contenido_id, fuente_atribucion) ".
                "VALUES('{$datos["nombre"]}', '{$datos["descripcion"]}', {$datos["visible"]}, '{$datos["duracion_tiempo"]}', '{$datos["dimension_pixeles"]}', '{$datos["extension_caracteres"]}', '{$datos["elemento_id"]}', '{$datos["tipo_contenido_id"]}', '{$datos["fuente_atribucion"]}');";

            $insertar_contenido = BDConexion::getInstancia()->query($query_contenido);
            
            $id = (Int) BDConexion::getInstancia()->insert_id;
            BDConexion::getInstancia()->commit();
            BDConexion::getInstancia()->autocommit(true);
            if($insertar_contenido){
                //setear valores del objeto    
                $this->setId($id);
                $this->setNombre($datos['nombre']);
                $this->setDescripcion($datos['descripcion']);
                $this->setVisible($datos['visible']);
                $this->setDuracionTiempo($datos['duracion_tiempo']);
                $this->setDimensionPixeles($datos['dimension_pixeles']);
                $this->setExtensionCaracteres($datos['extension_caracteres']);
                $this->setElementoId($datos['elemento_id']);
                $this->setTipoContenidoId($datos['tipo_contenido_id']);
                $this->setFuenteAtribucion($datos['fuente_atribucion']);
            }
            else {
//                echo "error al guardar en la bd";
                return false;
            }
            return $this->getObjeto($id);
        }
        else {
            if(isset($id)) {
                // modificación --> recuperar datos
                $this->getObjeto($id);
            }
            else {
                return false;
            }
        }
    }
    
    function getObjeto($id) {
        $this->id = $id;
        $this->query = "select * from contenido where id = '{$this->id}'";
        $this->datos = BDConexion::getInstancia()->query($this->query);

        $this->datos = $this->datos->fetch_assoc();

        foreach ($this->datos as $atributo => $valor) {
            $this->{$atributo} = $valor;
        }
        unset($this->query);
        unset($this->datos);
    }
    
    function deleteContenido() {
        BDConexion::getInstancia()->autocommit(false);
        BDConexion::getInstancia()->begin_transaction();

        $eliminar_archivos = BDObjetoGenerico::deleteTablaBD('archivo','contenido_id', $this->id);
        $eliminar_contenido = BDObjetoGenerico::deleteTablaBD('contenido','id', $this->id);
        
        if ($eliminar_archivos +  $eliminar_contenido) {
            BDConexion::getInstancia()->commit();
            BDConexion::getInstancia()->autocommit(true);
            return true;
        }
        BDConexion::getInstancia()->rollback();
        return false;
    }
    
    function getNombre() {
        return $this->nombre;
    }
    function setNombre($nombre) {
        $this->nombre = $nombre;
        $actualizado = BDObjetoGenerico::setTablaBD('contenido','nombre',$nombre,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getDescripcion() {
        return $this->descripcion;
    }
    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
        $actualizado = BDObjetoGenerico::setTablaBD('contenido','nombre',$descripcion,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getVisible() {
        return $this->visible;
    }
    function setVisible($visible) {
        $this->visible = $visible;
        $actualizado = BDObjetoGenerico::setTablaBD('contenido','visible',$visible,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getDuracionTiempo() {
        return $this->duracion_tiempo;
    }
    function setDuracionTiempo($duracion_tiempo) {
        $this->duracion_tiempo = $duracion_tiempo;
        $actualizado = BDObjetoGenerico::setTablaBD('contenido','duracion_tiempo',$duracion_tiempo,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }    
    function getDimensionPixeles() {
        return $this->dimension_pixeles;
    }
    function setDimensionPixeles($dimension_pixeles) {
        $this->dimension_pixeles = $dimension_pixeles;
        $actualizado = BDObjetoGenerico::setTablaBD('contenido','dimension_pixeles',$dimension_pixeles,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getExtensionCaracteres() {
        return $this->extension_caracteres;
    }
    function setExtensionCaracteres($extension_caracteres) {
        $this->extension_caracteres = $extension_caracteres;
        $actualizado = BDObjetoGenerico::setTablaBD('contenido','extension_caracteres',$extension_caracteres,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getElementoId() {
        return $this->elemento_id;
    }
    function setElementoId($elemento_id) {
        $this->elemento_id = $elemento_id;
        $actualizado = BDObjetoGenerico::setTablaBD('contenido','elemento_id',$elemento_id,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getTipoContenidoId() {
        return $this->tipo_contenido_id;
    }
    function setTipoContenidoId($tipo_contenido_id) {
        $this->tipo_contenido_id = $tipo_contenido_id;
        $actualizado = BDObjetoGenerico::setTablaBD('contenido','tipo_contenido_id',$tipo_contenido_id,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getFuenteAtribucion() {
        return $this->fuente_atribucion;
    }
    function setFuenteAtribucion($fuente_atribucion) {
        $this->fuente_atribucion = $fuente_atribucion;
        $actualizado = BDObjetoGenerico::setTablaBD('contenido','fuente_atribucion',$fuente_atribucion,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
}
