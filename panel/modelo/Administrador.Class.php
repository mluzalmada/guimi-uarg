<?php
include_once 'BDObjetoGenerico.Class.php';
include_once 'BDConexion.Class.php';

class Administrador extends BDObjetoGenerico {
    private $id;
    private $correo;

    function __construct($id = null) {
        parent::__construct($id, "administrador");
    }
    function getId() {
        return $this->id;
    }

    function getCorreo() {
        return $this->correo;
    }

}
