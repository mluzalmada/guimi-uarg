<?php
include_once 'BDColeccionGenerica.Class.php';
include_once 'TipoElemento.Class.php';

class ColeccionTiposElemento extends BDColeccionGenerica {

    /**
     *
     * @var Rol[]
     */
    private $tipos_elemento;
   
    function __construct() {
        parent::__construct();
        $this->setColeccion("tipo_elemento","TipoElemento");
        $this->tipos_elemento = $this->coleccion;
    }
    
     /**
     * 
     * @return array()
     */
    function getTiposElemento() {
        return $this->tipos_elemento;
    }
}
