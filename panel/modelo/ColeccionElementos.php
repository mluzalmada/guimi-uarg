<?php
include_once 'BDColeccionGenerica.Class.php';
include_once 'BDObjetoGenerico.Class.php';
include_once 'Elemento.Class.php';

class ColeccionElementos extends BDColeccionGenerica{
    
    /**
     *
     * @var Elemento[]
     */
    private $elementos;
       
    function __construct() {
        parent::__construct();
        $this->setColeccion("elemento","Elemento");
        $this->elementos = $this->coleccion;
    }
    
     /**
     * 
     * @return array()
     */
    function getElementos() {
        return $this->elementos;
    }
    
}


