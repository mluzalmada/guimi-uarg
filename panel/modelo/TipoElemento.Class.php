<?php
include_once 'BDObjetoGenerico.Class.php';
include_once 'BDConexion.Class.php';

class TipoElemento extends BDObjetoGenerico {

    protected $nombre;
    
    function __construct($id = null) {
        parent::__construct($id, "tipo_elemento");
    }
    function deleteTipoElemento() {
        BDConexion::getInstancia()->autocommit(false);
        BDConexion::getInstancia()->begin_transaction();

        $eliminar_tipo_elemento = BDObjetoGenerico::deleteTablaBD('tipo_elemento','id', $this->id);
        
        if ($eliminar_tipo_elemento) {
            BDConexion::getInstancia()->commit();
            BDConexion::getInstancia()->autocommit(true);
            return true;
        }
        BDConexion::getInstancia()->rollback();
        return false;
    }
    function getId() {
        return $this->id;
    }

    function getNombre() {
        return $this->nombre;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    

}
