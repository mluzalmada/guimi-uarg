<?php
include_once 'BDColeccionGenerica.Class.php';
include_once 'BDObjetoGenerico.Class.php';
include_once 'BDConexion.Class.php';
include_once 'Contenido.Class.php';

class ColeccionContenidos extends BDColeccionGenerica{
    
    /**
     *
     * @var Contenido[]
     */
    private $contenidos;
       
    function __construct($elemento_id = null) {
        // Si recibe ID, retorna la colección de contenidos del elemento con ese ID.
        if(isset($elemento_id)) {
            $this->elemento_id = $elemento_id;
            parent::__construct();
            $this->setColeccion("contenido where elemento_id = {$this->elemento_id}","Contenido");
            if($this->coleccion) {
                $this->contenidos = $this->coleccion;
            }
            else $this->contenidos = null;
        }
        // Si no se indica ID, se obtiene la colección completa de contenidos.
        else {
            parent::__construct();
            $this->setColeccion("contenido","Contenido");
            $this->contenidos = $this->coleccion;
        }
    }
    
     /**
     * 
     * @return array()
     */
    function getContenidos() {
        return $this->contenidos;
    }
    
}
