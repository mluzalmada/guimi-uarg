<?php
include_once 'BDColeccionGenerica.Class.php';
include_once 'Administrador.Class.php';

class ColeccionAdministradores extends BDColeccionGenerica{
    
    /**
     *
     * @var Administrador[]
     */
    private $administrador;
       
    function __construct() {
        parent::__construct();
        $this->setColeccion("administrador","Administrador");
        $this->administradores = $this->coleccion;
    }
    
     /**
     * 
     * @return array()
     */
    function getAdministradores() {
        return $this->administradores;
    }
}


