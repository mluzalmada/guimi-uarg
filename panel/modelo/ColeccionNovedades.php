<?php
include_once 'BDColeccionGenerica.Class.php';
include_once 'Novedad.Class.php';

class ColeccionNovedades extends BDColeccionGenerica{
    
    /**
     *
     * @var Novedad[]
     */
    private $novedades;
       
    function __construct() {
        parent::__construct();
        $this->setColeccion("novedad","Novedad");
        $this->novedades = $this->coleccion;
    }
    
     /**
     * 
     * @return array()
     */
    function getNovedades() {
        return $this->novedades;
    }
}