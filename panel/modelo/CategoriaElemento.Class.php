<?php
include_once 'BDObjetoGenerico.Class.php';

class CategoriaElemento extends BDObjetoGenerico {

    function __construct($id = null) {
        parent::__construct($id, "categoria_elemento");
    }
    function deleteCategoriaElemento() {
        BDConexion::getInstancia()->autocommit(false);
        BDConexion::getInstancia()->begin_transaction();

        $eliminar_categoria_elemento = BDObjetoGenerico::deleteTablaBD('categoria_elemento','id', $this->id);
        
        if ($eliminar_categoria_elemento) {
            BDConexion::getInstancia()->commit();
            BDConexion::getInstancia()->autocommit(true);
            return true;
        }
        BDConexion::getInstancia()->rollback();
        return false;
    }
}
