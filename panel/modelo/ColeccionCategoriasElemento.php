<?php
include_once 'BDColeccionGenerica.Class.php';
include_once 'CategoriaElemento.Class.php';

class ColeccionCategoriasElemento extends BDColeccionGenerica {

    /**
     *
     * @var CategoriaElemento[]
     */
    private $categorias_elemento;
   
    function __construct() {
        parent::__construct();
        $this->setColeccion("categoria_elemento","CategoriaElemento");
        $this->categorias_elemento = $this->coleccion;
    }
    
     /**
     * 
     * @return array()
     */
    function getCategoriasElemento() {
        return $this->categorias_elemento;
    }
}
