<?php
include_once 'BDObjetoGenerico.Class.php';
include_once 'BDConexion.Class.php';

class TipoContenido extends BDObjetoGenerico {

    protected $nombre;
    
    function __construct($id = null) {
        parent::__construct($id, "tipo_contenido");
    }
    
    function deleteTipoContenido() {
        BDConexion::getInstancia()->autocommit(false);
        BDConexion::getInstancia()->begin_transaction();

        $eliminar_tipo_contenido = BDObjetoGenerico::deleteTablaBD('tipo_contenido','id', $this->id);
        
        if ($eliminar_tipo_contenido) {
            BDConexion::getInstancia()->commit();
            BDConexion::getInstancia()->autocommit(true);
            return true;
        }
        BDConexion::getInstancia()->rollback();
        return false;
    }
    
    function getId() {
        return $this->id;
    }

    function getNombre() {
        return $this->nombre;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }
    
    function getDescripcion() {
        return $this->descripcion;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

}
