<?php
include_once 'BDColeccionGenerica.Class.php';
include_once 'Marcador.Class.php';

class ColeccionMarcadores extends BDColeccionGenerica{
    
    /**
     *
     * @var Marcador[]
     */
    private $elemento_id = null;
    private $marcadores;
       
    function __construct($elemento_id = null) {
        // Si recibe ID, retorna la colección de marcadores del elemento con ese ID.
        if(isset($elemento_id)) {
            $this->elemento_id = $elemento_id;
            parent::__construct();
            $this->setColeccion("marcador where elemento_id = {$this->elemento_id}","Marcador");
            if($this->coleccion) {
                $this->marcadores = $this->coleccion;
            }
            else $this->marcadores = null;
        }
        // Si no se indica ID, se obtiene la colección completa de marcadores.
        else {
            parent::__construct();
            $this->setColeccion("marcador","Marcador");
            $this->marcadores = $this->coleccion;
        }
    }
    
     /**
     * 
     * @return array()
     */
    function getMarcadores() {
        return $this->marcadores;
    }
}