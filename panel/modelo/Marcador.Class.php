<?php
include_once 'BDObjetoGenerico.Class.php';
include_once 'BDConexion.Class.php';

class Marcador extends BDObjetoGenerico {

    protected $id;
    protected $imagen_ruta;
    protected $patron_ruta;
    protected $elemento_id;
    protected $datos;
    protected $existe = true;
    
    function __construct($id = null, $datos = null) {
//        parent::__construct($id, "marcador");
        if(isset($datos)) {
            $this->datos = $datos;
            BDConexion::getInstancia()->autocommit(false);
            BDConexion::getInstancia()->begin_transaction();

            $query_marcador = "INSERT INTO  ". Constantes::BD_SCHEMA.".marcador ".
                "(elemento_id) ".
                    "VALUES ('{$datos["elemento_id"]}');";
            $insertar_marcador = BDConexion::getInstancia()->query($query_marcador);

            $id = (Int) BDConexion::getInstancia()->insert_id;

            BDConexion::getInstancia()->commit();
            BDConexion::getInstancia()->autocommit(true);

            if($insertar_marcador) {
                $this->setId($id);
                $this->setElementoId($datos['elemento_id']);
            }
            else {
                $this->existe = false;
            }
            $this->getObjeto($id);
        }
        else {
            if(isset($id)) {
                $this->getObjeto($id);
            }
            else {
                $this->existe = false;
            }
        }
    }
    
        function getObjeto($id) {
        $this->id = $id;
        $this->query = "select * from marcador where id = '{$this->id}'";
        $this->datos = BDConexion::getInstancia()->query($this->query);
        $this->datos = $this->datos->fetch_assoc();
        if($this->datos) {
            foreach ($this->datos as $atributo => $valor) {
                $this->{$atributo} = $valor;
            }            
        }
        else {
            $this->existe = false;
        }
        unset($this->query);
        unset($this->datos);
    }
    
    function getMarcadorPorElementoId($elemento_id) {
        $this->elemento_id = $elemento_id;
        $this->query = "select * from marcador where elemento_id = '{$this->elemento_id}'";
        $this->datos = BDConexion::getInstancia()->query($this->query);
        
        $this->datos = $this->datos->fetch_assoc();

        if($this->datos) {
            foreach ($this->datos as $atributo => $valor) {
                $this->{$atributo} = $valor;
            }
        }
        else {
            $this->existe = false;
        }
        unset($this->query);
        unset($this->datos);
    }
        
    function deleteMarcador() {
        BDConexion::getInstancia()->autocommit(false);
        BDConexion::getInstancia()->begin_transaction();

        $eliminar_marcador = BDObjetoGenerico::deleteTablaBD('marcador','id', $this->id);
        
        if ($eliminar_marcador) {
            BDConexion::getInstancia()->commit();
            BDConexion::getInstancia()->autocommit(true);
            return true;
        }
        BDConexion::getInstancia()->rollback();
        return false;
    }
    
    function getImagenRuta() {
        return $this->imagen_ruta;
    }
    function setImagenRuta($imagen_ruta) {
        $this->imagen_ruta = $imagen_ruta;
        $actualizado = BDObjetoGenerico::setTablaBD('marcador','imagen_ruta',$imagen_ruta,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }

    function getPatronRuta() {
    return $this->patron_ruta;
    }
    function setPatronRuta($patron_ruta) {
        $this->patron_ruta = $patron_ruta;
        $actualizado = BDObjetoGenerico::setTablaBD('marcador','patron_ruta',$patron_ruta,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }

    function getElementoId() {
    return $this->elemento_id;
    }
    function setElementoId($elemento_id) {
        $this->elemento_id = $elemento_id;
        $actualizado = BDObjetoGenerico::setTablaBD('marcador','elemento_id',$elemento_id,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    
    function getExiste() {
        return $this->existe;
    }

}
