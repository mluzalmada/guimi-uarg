<?php
include_once 'BDColeccionGenerica.Class.php';
include_once 'Archivo.Class.php';

class ColeccionArchivos extends BDColeccionGenerica {

    /**
     *
     * @var Rol[]
     */
    private $archivos;
   
    function __construct($contenido_id = null) {
        // Si recibe ID, retorna el archivo del conrenido con ese ID.
        if(isset($contenido_id)) {
            $this->contenido_id = $contenido_id;
            parent::__construct();
            $this->setColeccion("arhivo where contenido_id = {$this->contenido_id}","Archivo");
            if($this->coleccion) {
                $this->archivos = $this->coleccion;
            }
            else $this->archivos = null;
        }
        else {
            parent::__construct();
            $this->setColeccion("archivo","Archivo");
            $this->archivos = $this->coleccion;            
        }
    }
    
     /**
     * 
     * @return array()
     */
    function getArchivos() {
        return $this->archivos;
    }
}
