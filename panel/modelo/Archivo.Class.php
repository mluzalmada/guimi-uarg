<?php
include_once 'BDObjetoGenerico.Class.php';
include_once 'BDConexion.Class.php';

class Archivo extends BDObjetoGenerico {
    protected $id;
    protected $nombre;
    protected $extension;
    protected $ruta;
    protected $tamanio_bytes;
    protected $contenido_id;
    protected $tipo_mime; 
    protected $datos; 
    protected $existe = true; 

    function __construct($id = null, $datos = null) {

        if(isset($datos)) {
            // Guardar en la BD -->
            BDConexion::getInstancia()->autocommit(false);
            BDConexion::getInstancia()->begin_transaction();
            
             $query_archivo = "INSERT INTO  ". Constantes::BD_SCHEMA.".archivo ".
                " (nombre, extension, ruta, tamanio_bytes, contenido_id, tipo_mime)".
                " VALUES('{$datos["nombre"]}', '{$datos["extension"]}', '{$datos["ruta"]}', '{$datos["tamanio_bytes"]}', '{$datos["contenido_id"]}', '{$datos["tipo_mime"]}');";
            $insertar_archivo = BDConexion::getInstancia()->query($query_archivo);
            $id = (Int) BDConexion::getInstancia()->insert_id;
            
            BDConexion::getInstancia()->commit();
            BDConexion::getInstancia()->autocommit(true);
            if($insertar_archivo) {
                $this->setId($id);
                $this->setNombre($datos['nombre']);
                $this->setExtension($datos['extension']);
                $this->setRuta($datos['ruta']);
                $this->setTamanioBytes($datos['tamanio_bytes']);
                $this->setContenidoId($datos['contenido_id']);
                $this->setTipoMime($datos['tipo_mime']);
                $this->getObjeto($id);
            }
            else {
//                echo "error al guardar en la bd";
                $this->existe = false;
            }
        }
        else {
            if(isset($id)) {
                // modificación --> recuperar datos
                $this->getObjeto($id);
            }
            else { // No se recibieron parámetros
                $this->existe = false;
            }
        }
    }
    
    function getObjeto($id) {
        $this->id = $id;
        $this->query = "select * from archivo where id = '{$this->id}'";
        $this->datos = BDConexion::getInstancia()->query($this->query);

        $this->datos = $this->datos->fetch_assoc();

        if($this->datos) {
            foreach ($this->datos as $atributo => $valor) {
                $this->{$atributo} = $valor;
            }
        }
        else {
            $this->existe = false;
        }
        unset($this->query);
        unset($this->datos);
    }
    
    function getArchivoPorContenidoId($contenido_id) {
        $this->contenido_id = $contenido_id;
        $this->query = "select * from archivo where contenido_id = '{$this->contenido_id}'";
        $this->datos = BDConexion::getInstancia()->query($this->query);
        
        $this->datos = $this->datos->fetch_assoc();

        if($this->datos) {
            foreach ($this->datos as $atributo => $valor) {
                $this->{$atributo} = $valor;
            }
        }
        else {
            $this->existe = false;
        }
        unset($this->query);
        unset($this->datos);
    }
    
    function deleteArchivo() {
        BDConexion::getInstancia()->autocommit(false);
        BDConexion::getInstancia()->begin_transaction();

        $eliminar_archivos = BDObjetoGenerico::deleteTablaBD('archivo','id', $this->id);
        
        if ($eliminar_archivos) {
            BDConexion::getInstancia()->commit();
            BDConexion::getInstancia()->autocommit(true);
            return true;
        }
        BDConexion::getInstancia()->rollback();
        return false;
    }
    
    function getNombre() {
        return $this->nombre;
    }
    function setNombre($nombre) {
        $this->nombre = $nombre;
        $actualizado = BDObjetoGenerico::setTablaBD('archivo','nombre',$nombre,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getExtension() {
        return $this->extension;
    }
    function setExtension($extension) {
        $this->extension = $extension;
        $actualizado = BDObjetoGenerico::setTablaBD('archivo','extension',$extension,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getRuta() {
        return $this->ruta;
    }
    function setRuta($ruta) {
        $this->ruta = $ruta;
        $actualizado = BDObjetoGenerico::setTablaBD('archivo','ruta',$ruta,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getTamanioBytes() {
        return $this->tamanio_bytes;
    }
    function setTamanioBytes($tamanio_bytes) {
        $this->tamanio_bytes = $tamanio_bytes;
        $actualizado = BDObjetoGenerico::setTablaBD('archivo','tamanio_bytes',$tamanio_bytes,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }    
    function getContenidoId() {
        return $this->dimension_pixeles;
    }
    function setContenidoId($dimension_pixeles) {
        $this->dimension_pixeles = $dimension_pixeles;
        $actualizado = BDObjetoGenerico::setTablaBD('archivo','dimension_pixeles',$dimension_pixeles,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getTipoMime() {
        return $this->tipo_mime;
    }
    function setTipoMime($tipo_mime) {
        $this->tipo_mime = $tipo_mime;
        $actualizado = BDObjetoGenerico::setTablaBD('archivo','tipo_mime',$tipo_mime,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
}
