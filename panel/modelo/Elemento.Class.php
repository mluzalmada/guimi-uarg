<?php
include_once 'BDObjetoGenerico.Class.php';
include_once 'BDConexion.Class.php';

class Elemento extends BDObjetoGenerico {

    protected $id;
    protected $nombre;
    protected $tipo_elemento_id;
    protected $categoria_elemento_id;
    protected $estado_conservacion;
    protected $descripcion;
    protected $fabricante;
    protected $modelo;
    protected $anio_referencia;
    protected $entrega_nombre;
    protected $entrega_caracter;
    protected $ubicacion;
    protected $visible;
    protected $id_omeka;
    protected $tamanio_fisico;
    protected $foto_ruta;
    protected $existe = true;
    
    protected $datos; 

    function __construct($id = null, $datos = null) {
//        parent::__construct($id, "elemento");
        if(isset($datos)) { // Alta
            // Guardar en la BD -->
            $this->datos = $datos;
            BDConexion::getInstancia()->autocommit(false);
            BDConexion::getInstancia()->begin_transaction();

            $query_elemento = "INSERT INTO  ". Constantes::BD_SCHEMA.".elemento "
                ."(id, nombre, tipo_elemento_id, categoria_elemento_id, estado_conservacion, descripcion, fabricante, modelo, anio_referencia, entrega_nombre, entrega_caracter, ubicacion, visible, id_omeka, tamanio_fisico) "
                ."VALUES (null, '{$datos["nombre"]}', '{$datos["tipo_elemento_id"]}', '{$datos["categoria_elemento_id"]}', '{$datos["estado_conservacion"]}', '{$datos["descripcion"]}', '{$datos["fabricante"]}', '{$datos["modelo"]}', '{$datos["anio_referencia"]}', '{$datos["entrega_nombre"]}', '{$datos["entrega_caracter"]}', '{$datos["ubicacion"]}', '{$datos["visible"]}', '{$datos["id_omeka"]}', '{$datos["tamanio_fisico"]}');";
            $insertar_elemento = BDConexion::getInstancia()->query($query_elemento);
            
            $id = (Int) BDConexion::getInstancia()->insert_id;
            
            BDConexion::getInstancia()->commit();
            BDConexion::getInstancia()->autocommit(true);
            if($insertar_elemento){
                //setear valores del objeto    
                $this->setId($id);
                $this->setNombre($datos['nombre']);
                $this->setTipo($datos['tipo_elemento_id']);
                $this->setCategoria($datos['categoria_elemento_id']);
                $this->setEstadoConservacion($datos['estado_conservacion']);
                $this->setDescripcion($datos['descripcion']);
                $this->setFabricante($datos['fabricante']);
                $this->setModelo($datos['modelo']);
                $this->setAnioReferencia($datos['anio_referencia']);
                $this->setEntregaNombre($datos['entrega_nombre']);
                $this->setEntregaCaracter($datos['entrega_caracter']);
                $this->setUbicacion($datos['ubicacion']);
                $this->setVisible($datos['visible']);
                $this->setDescripcion($datos['descripcion']);
                $this->setIdOmeka($datos['id_omeka']);
                $this->setTamanioFisico($datos['tamanio_fisico']);
                $this->setFotoRuta($datos['descripcion']);
            }
            else {
                $this->existe = false;
            }
            $this->getObjeto($id);
        }
        else {
            if(isset($id)) {
                $this->getObjeto($id);
            }
            else {
                $this->existe = false;
            }
        }
    }
    
    function getObjeto($id) {
        $this->id = $id;
        $this->query = "select * from elemento where id = '{$this->id}'";
        $this->datos = BDConexion::getInstancia()->query($this->query);
        $this->datos = $this->datos->fetch_assoc();
        if($this->datos) {
            foreach ($this->datos as $atributo => $valor) {
                $this->{$atributo} = $valor;
            }            
        }
        else {
            $this->existe = false;
        }
        unset($this->query);
        unset($this->datos);
    }
    
    function deleteElemento() {
        BDConexion::getInstancia()->autocommit(false);
        BDConexion::getInstancia()->begin_transaction();
//        TODO: Eliminar archivos asociados a cada recurso
//        $recursos_del_elemento = BDObjetoGenerico::getElementosRelacionados('recurso', 'id','elemento_id', $this->id);
        $eliminar_marcador = BDObjetoGenerico::deleteTablaBD('marcador','elemento_id', $this->id);
        $eliminar_recurso = BDObjetoGenerico::deleteTablaBD('recurso','elemento_id', $this->id);
        $eliminar_elemento = BDObjetoGenerico::deleteTablaBD('elemento','id', $this->id);
        
        if ($eliminar_marcador +  $eliminar_recurso + $eliminar_elemento) {
            BDConexion::getInstancia()->commit();
            BDConexion::getInstancia()->autocommit(true);
            return true;
        }
        BDConexion::getInstancia()->rollback();
        return false;
    }
    
    function getNombre() {
        return $this->nombre;
    }
    function setNombre($nombre) {
        $this->nombre = $nombre;
        $actualizado = BDObjetoGenerico::setTablaBD('elemento','nombre',$nombre,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getTipoId() {
        return $this->tipo_elemento_id;
    }
    function setTipo($tipo_elemento_id) {
        $this->tipo_elemento_id = $tipo_elemento_id;
        $actualizado = BDObjetoGenerico::setTablaBD('elemento','tipo_elemento_id',$tipo_elemento_id,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getCategoria() {
        return $this->categoria_elemento_id;
    }
    function setCategoria($categoria_elemento_id) {
        $this->categoria_elemento_id = $categoria_elemento_id;
        $actualizado = BDObjetoGenerico::setTablaBD('elemento','categoria_elemento_id',$categoria_elemento_id,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getEstadoConservacion() {
        return $this->estado_conservacion;
    }
    function setEstadoConservacion($estado_conservacion) {
        $this->estado_conservacion = $estado_conservacion;
        $actualizado = BDObjetoGenerico::setTablaBD('elemento','estado_conservacion',$estado_conservacion,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getDescripcion() {
        return $this->descripcion;
    }
    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
        $actualizado = BDObjetoGenerico::setTablaBD('elemento','descripcion',$descripcion,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getFabricante() {
        return $this->fabricante;
    }
    function setFabricante($fabricante) {
        $this->fabricante = $fabricante;
        $actualizado = BDObjetoGenerico::setTablaBD('elemento','fabricante',$fabricante,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getModelo() {
        return $this->modelo;
    }
    function setModelo($modelo) {
        $this->modelo = $modelo;
        $actualizado = BDObjetoGenerico::setTablaBD('elemento','modelo', $modelo, $this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getAnioReferencia() {
        return $this->anio_referencia;
    }
    function setAnioReferencia($anio_referencia) {
        $this->anio_referencia = $anio_referencia;
        $actualizado = BDObjetoGenerico::setTablaBD('elemento','anio_referencia',$anio_referencia,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getEntregaNombre() {
        return $this->entrega_nombre;
    }
    function setEntregaNombre($entrega_nombre) {
        $this->entrega_nombre = $entrega_nombre;
        $actualizado = BDObjetoGenerico::setTablaBD('elemento','entrega_nombre',$entrega_nombre,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getEntregaCaracter() {
        return $this->entrega_caracter;
    }
    function setEntregaCaracter($entrega_caracter) {
        $this->entrega_caracter = $entrega_caracter;
        $actualizado = BDObjetoGenerico::setTablaBD('elemento','entrega_caracter',$entrega_caracter,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getUbicacion() {
        return $this->ubicacion;
    }
    function setUbicacion($ubicacion) {
        $this->ubicacion = $ubicacion;
        $actualizado = BDObjetoGenerico::setTablaBD('elemento','ubicacion',$ubicacion,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getVisible() {
        return $this->visible;
    }
    function setVisible($visible) {
        $this->visible = $visible;
        $actualizado = BDObjetoGenerico::setTablaBD('elemento','visible',$visible,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getIdOmeka() {
        return $this->id_omeka;
    }
    function setIdOmeka($id_omeka) {
        $this->id_omeka = $id_omeka;
        $actualizado = BDObjetoGenerico::setTablaBD('elemento','id_omeka',$id_omeka,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getTamanioFisico() {
        return $this->tamanio_fisico;
    }
    function setTamanioFisico($tamanio_fisico) {
        $this->tamanio_fisico = $tamanio_fisico;
        $actualizado = BDObjetoGenerico::setTablaBD('elemento','tamanio_fisico',$tamanio_fisico,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getFotoRuta() {
        return $this->foto_ruta;
    }
    function setFotoRuta($foto_ruta) {
        $this->foto_ruta = $foto_ruta;
        $actualizado = BDObjetoGenerico::setTablaBD('elemento','foto_ruta',$foto_ruta,$this->id);
        if($actualizado) {
            return true;
        }
        return false;
    }
    function getExiste() {
        return $this->existe;
    }
    function getDatos() {
        return $this->datos;
    }

}
