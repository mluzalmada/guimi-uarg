<?php
include_once 'BDColeccionGenerica.Class.php';
include_once 'TipoContenido.Class.php';

class ColeccionTiposContenido extends BDColeccionGenerica {

    /**
     *
     * @var Rol[]
     */
    private $tipos_contenido;
   
    function __construct() {
        parent::__construct();
        $this->setColeccion("tipo_contenido","TipoContenido");
        $this->tipos_contenido = $this->coleccion;
    }
    
     /**
     * 
     * @return array()
     */
    function getTiposContenido() {
        return $this->tipos_contenido;
    }
}