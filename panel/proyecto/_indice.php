<?php include_once '../lib/ControlAcceso.Class.php'; ?>

<html>

<head>
    <meta charset="UTF-8">
    <?php include_once('../lib/headers.php'); ?>
    <link rel="stylesheet" href="css/estilos_proyecto.min.css">

    <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Proceso de Desarrollo</title>
</head>

<body class="sticky-footer">

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container navbar-dark bg-dark">
            <a class="navbar-brand" href="menu_principal.php">
                <img src="../lib/img/Logo-GUIMI-blanco.png" height="30" class="d-inline-block align-top" alt="">
            </a>
            <span class="navbar-text">
                <?php echo Constantes::NOMBRE_SISTEMA; ?> - Proceso de Desarrollo
            </span>
        </div>
    </nav>

    <div class="container mt-4">
     <?php
        if(isset($_GET["ir_a"])) {
            switch($_GET["ir_a"]) {
                case 'descargas': {
                    break;
                }
                default: {
                    include_once('estructura.php');
                }
            }
        }
        else {
            include_once('estructura.php');
        }
        ?>
      
    </div>
    <?php include_once('../gui/footer.php'); ?>
    <script type="application/javascript" src="js/funciones_proyecto.min.js"></script>
</body>

</html>