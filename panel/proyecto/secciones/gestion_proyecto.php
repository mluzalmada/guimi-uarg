<!--SECCIÓN: GESTIÓN DEL PROYECTO-->

<div id="gestion_proyecto" class="tab-pane fade" role="tabpanel">
    <h3>Gestión del proyecto</h3>
    <p class="text-justify">El análisis consiste en obtener una visión del sistema que se preocupa de ver qué hace, de modo que sólo se interesa por los requisitos funcionales. El diseño, por su parte, es un refinamiento del análisis que tiene en cuenta los requisitos no funcionales, es decir, cómo cumple el sistema sus objetivos.</p>
    <p class="text-justify">Con estos objetivos, se desarrollarán a continuación los siguientes documentos:</p>
    <ul>
        <li class="text-uppercase small">Estimación</li>
        <li class="text-uppercase small">Plan de estimación</li>
        <li class="text-uppercase small">Plan de iteración</li>
        <li class="text-uppercase small">Plan de proyecto</li>
        <li class="text-uppercase small">Resumen de reunión</li>
    </ul>

    <div class="observacion observacion-info">
        <p class="text-justify">
            <span class="font-weight-bold mr-1">NOTA:</span>
            <span>Los documentos aquí presentados constituyen la versión final de los mismos, luego de haber sido modificados a lo largo del proyecto.</span>
        </p>
    </div>
</div>

<!-- Estimación-->
<div id="estimacion" class="tab-pane fade" role="tabpanel">
    <h3>Estimación</h3>
    <p class="text-justify">...</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>

<!-- Plan de estimación-->
<div id="pan_estimacion" class="tab-pane fade" role="tabpanel">
    <h3>Plan de estimación</h3>
    <p class="text-justify">...</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>

<!-- Plan de iteración-->
<div id="plan_iteracion" class="tab-pane fade" role="tabpanel">
    <h3>Plan de iteración</h3>
    <p class="text-justify">...</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>

<!-- Plan de proyecto-->
<div id="plan_proyecto" class="tab-pane fade" role="tabpanel">
    <h3>Plan de proyecto</h3>
    <p class="text-justify">...</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>


<!-- Resumen de reunión-->
<div id="resumen_reunion" class="tab-pane fade" role="tabpanel">
    <h3>Resumen de reunión</h3>
    <p class="text-justify">...</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>
