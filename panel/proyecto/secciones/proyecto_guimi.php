<div id="como_surge_guimi" class="tab-pane fade show active" role="tabpanel">
    <h3>¿Cómo surge el proyecto?</h3>
    <p class="text-justify">El proyecto GuIMI surge en el marco de la asignatura <span class="font-weight-bold">Laboratorio de Desarrollo de Software</span>, donde los estudiantes deben participar de manera grupal en el desarrollo de un software que resuelva un problema designado por los docentes. En esta tarea, además de resolver el problema con una solución que implique el desarrollo de software, el enfoque debe centrarse en el proceso, las técnicas y metodologías utilizadas de manera integral en todo el proyecto, <span class="font-weight-bold">aplicando los conocimientos adquiridos en las diferentes asignaturas del plan de estudios</span>.</p>
    <p class="text-justify">Es en este contexto donde el equipo de desarrollo tiene sus primeras reuniones con los docentes, profesionales y estudiantes que trabajan en el museo inaugurado hace algunos años en el campus. Luego de analizar los requerimientos de los mismos, se propone el desarrollo de un sistema software que, implementando realidad aumentada (AR, por sus siglas en inglés), pueda satisfacer las necesidades del Museo de Informática.</p>
</div>

<div id="en_que_consiste_guimi" class="tab-pane fade" role="tabpanel">
    <h3>¿En qué consiste el proyecto?</h3>
    <p class="text-justify">GuIMI es el acrónimo de <span class="font-italic">Guía Interactiva del Museo de Informática</span>. Tal como se intenta presentar con su nombre, el objetivo del proyecto es desarrollar una guía virtual que, utilizando tecnología de Realidad Aumentada, permita a los visitantes del museo una exploración ampliada de los elementos y su historia a través de la interacción con dispositivos móviles.</p>
    <div class="card">
        <blockquote class="card-body mb-0">
            <img src="../lib/img/realidad_aumentada3.jpg" class="img-thumbnail float-right ml-2 mb-1 w-50" alt="Ejemplo de realidad aumentada" onerror="this.src='../lib/img/realidad_aumentada1.jpg'">
            <small>
                <h6 class="text-uppercase">Realidad Aumentada (AR)</h6>
                <p class="text-justify">La realidad aumentada es una herramienta que integra la percepción e interacción con el mundo real y permite al interesado estar en un contexto real aumentado con información adicional sobre un objeto generada por el ordenador u otros dispositivos móviles.</p>
                <p class="text-justify">Supone la incorporación de datos e información digital en un entorno real por medio del reconocimiento de patrones que se realiza mediante un software.</p>
                <p class="text-justify">En la realidad aumentada nuestro propio mundo se convierte en el soporte. Todo se produce en un entorno real, y gracias a la cámara y la pantalla de un dispositivo podremos ver elementos que no están presentes en el mundo real y, también, interactuar con los mismos.</p>
                <p class="text-right text-uppercase font-weight-bold mb-0">FUENTES:</p>
                <p class="text-right text-uppercase">Basogain, X. <a href="http://files.mediaticos.webnode.es/200000016-a645ea73b3/realidad%20A..pdf">Realidad aumentada en la educación: una tecnología emergente</a>. Consultado el 10 de octubre de 2019.</p>
                <p class="text-right text-uppercase">Moreno, Marta (11 de enero de 2019). <a href="https://www.educaciontrespuntocero.com/noticias/realidad-aumentada-virtual-y-mixta/97214.html">¿Qué diferencias hay entre realidad aumentada, virtual y mixta?</a>. Educación 3.0 (Tecno Media Comunicación SL). Consultado el 10 de octubre de 2019.</p>
            </small>
        </blockquote>


    </div>
    <br>
    <p class="text-justify">Además, como el desarrollo está basado en la Metodología PSI, seguimos también sus objetivos didácticos dejando a disposición de usuarios y estudiantes la documentación del proyecto.</p>
</div>

<div id="cual_es_la_importancia" class="tab-pane fade" role="tabpanel">
    <h3>¿Cuál es la importancia de GuIMI?</h3>
    <ul>
        <li>Se ofrece una <span class="font-weight-bold">experiencia interactiva</span> a los visitantes del museo.</li>

        <li>Funciona como <span class="font-weight-bold">aplicación móvil</span> sin ocupar espacio de almacenamiento en el dispositivo (PWA, Progressive Web App).</li>

        <li><span class="font-weight-bold">Preservación de las piezas:</span> Debido a su valor y fragilidad, algunos de los elementos no pueden ser observados de cerca o manipularse. El recorrido del museo con GuIMI permite conocer detalles, como observar la placa madre de una computadora antigua, que una visita tradicional no permitiría.</li>

        <li><span class="font-weight-bold">Ofrece autonomía y recorridos personalizados a los visitantes del museo:</span> Como el personal del museo es limitado, los recorridos suelen hacerse de manera grupal con tiempos limitados. Con GuIMI, el usuario visitante puede enfocar su recorrido especificamente en los elementos o secciones que sean de su interés particular.</li>

        <li><span class="font-weight-bold">Agrega diversos contenidos con realidad aumentada a los elementos históricos del museo:</span> Permite exponer imágenes históricas, audios del sonido al utilizarlos o entrevistas, videos explicativos o spots publicitarios y textos que permitan conocer con mayor detalle cada elemento.</li>

        <li><span class="font-weight-bold">Su propósito es educativo:</span> Poder transmitir conocimiento y educar a través del vínculo lúdico o novedoso con la aplicación.</li>

        <li>La herramienta misma, como una nueva tecnología, <span class="font-weight-bold">se suma a la colección del Museo</span>.</li>
    </ul>
</div>

<div id="como_se_desarrolla" class="tab-pane fade" role="tabpanel">
    <h3>¿Cómo se desarrolla?</h3>

    <p class="text-justify">Como ya se ha mencionado, el desarrollo de GuIMI se ha realizado siguiendo el enfoque del Proceso PSI, que tal como sus autores lo sugieren, fue adaptado para responder a las necesidades del proyecto.</p>
    <p class="text-justify">Se utilizaron a lo largo del proceso de desarrollo diversas herramientas y recursos:</p>
    <ul>
        <li><span class="font-weight-bold mr-1">Diagramas y diseño/edición de imágenes:</span> Día, CorelDraw</li>
        <li><span class="font-weight-bold mr-1">Gestión de configuración:</span> Github, Gitlab</li>
        <li><span class="font-weight-bold mr-1">Capacitación:</span>Documentación, tutoriales, experiencia de otros desarrolladores, listas de correo.</li>
        <li><span class="font-weight-bold mr-1">Mejora continua:</span>Revisión de código, Refactoring, Pruebas.</li>
    </ul>
    <h6 class="small text-uppercase">Software:</h6>
    <ul>
        <li><span class="font-weight-bold mr-1">Editores de código:</span>Brackets</li>
        <li><span class="font-weight-bold mr-1">Gestor de base de datos:</span>DBeaver</li>
        <!--
                                <li><span class="font-weight-bold mr-1"></span></li>
                                <li><span class="font-weight-bold mr-1"></span></li>
-->
        <li><span class="font-weight-bold mr-1">Canales de comunicación:</span>Slack, Mattermost, Signal, WhatsApp, Correos electrónicos y SMS</li>
        <li><span class="font-weight-bold mr-1">Otros:</span>Meld, LibreOffice, Navegadores web (Brave, Firefox, Chrome)</li>
    </ul>

    <div class="observacion observacion-info">
        <p class="text-justify">
            <strong>Más información:</strong>
            <span>En las siguientes secciones se incluye una breve introducción sobre las diferentes etapas y los documentos asociados a cada una de ellas.</span>
            <br>
            <span>El código fuente y la documentación también se encuentra disponible en el repositorio de Gitlab.</span>
        </p>
        <div class="row">
            <div class="col">
                <a href="https://gitlab.com/guimi-unpa/guimi-web/" target="_blank">
                    <button class="btn btn-outline-info btn-sm float-right">
                        <i class="fas fa-arrow-up-right-from-square fa-fw mr-1"></i>
                        Ir al repositorio GuIMI
                        <i class="fab fa-gitlab fa-fw ml-1"></i>
                    </button>
                </a>

            </div>
        </div>
    </div>
</div>

<div id="como_se_implementa" class="tab-pane fade" role="tabpanel">
    <h3>¿Cómo se implementa?</h3>
    <p class="text-justify"></p>
    <p class="text-justify">El sistema se implementa en dos módulos:</p>
    <ul>
        <li><span class="font-weight-bold mr-1">Módulo administrador:</span>El panel de administración de GuIMI requiere la autenticación del usuario mediante UargFlow BS para poder gestionar la carga, edición y eliminación de los elementos, contenidos y novedades del Museo.</li>
        <li><span class="font-weight-bold mr-1">Módulo usuario visitante:</span>La PWA de GuIMI está destinada a usuarios visitantes del museo y utiliza tecnología de realidad aumentada para mostrar contenidos de cada elemento y novedades acerca de muestras y eventos del museo.</li>
    </ul>

    <p class="text-justify mt-2">Se utilizan diferentes lenguajes, frameworks y librerías para la implementación de este sistema software. </p>
    <ul>
        <li>PHP, HTML5, CSS</li>
        <li>Javascript (JQuery, AR.js)</li>
        <li>MySQL (MariaDB)</li>
        <li>Servidor Web Apache</li>
        <li>Metodología PSI, UargFlow BS, Bootstrap, FontAwesome</li>
    </ul>

</div>
