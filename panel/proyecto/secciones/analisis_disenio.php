<!--SECCIÓN: ANÁLISIS Y DISEÑO-->

<div id="analisis_disenio" class="tab-pane fade" role="tabpanel">
    <h3>Análisis y Diseño</h3>
    <p class="text-justify">El análisis consiste en obtener una visión del sistema que se preocupa de ver qué hace, de modo que sólo se interesa por los requisitos funcionales. El diseño, por su parte, es un refinamiento del análisis que tiene en cuenta los requisitos no funcionales, es decir, cómo cumple el sistema sus objetivos.</p>
    <p class="text-justify">Con estos objetivos, se desarrollarán a continuación los siguientes documentos:</p>
    <ul>
        <li class="text-uppercase small">Estudio de factibilidad</li>
        <li class="text-uppercase small">Modelo de casos de uso</li>
        <li class="text-uppercase small">Modelo de datos</li>
        <li class="text-uppercase small">Modelo de diseño</li>
        <li class="text-uppercase small">Modelo de negocio</li>
        <li class="text-uppercase small">Prototipo de interfaz</li>
        <li class="text-uppercase small">Arquitectura del sistema</li>
        <li class="text-uppercase small">Modelo de visión</li>
        <li class="text-uppercase small">Especificación de caso de uso</li>
        <li class="text-uppercase small">Glosario</li>
    </ul>

    <div class="observacion observacion-info">
        <p class="text-justify">
            <span class="font-weight-bold mr-1">NOTA:</span>
            <span>Los documentos aquí presentados constituyen la versión final de los mismos, luego de haber sido modificados a lo largo del proyecto.</span>
        </p>
    </div>
</div>

<!-- Estudio de factibilidad-->
<div id="estudio_factibilidad" class="tab-pane fade" role="tabpanel">
    <h3>Estudio de factibilidad</h3>
    <p class="text-justify">El desarrollo de un estudio de factibilidad consta en ver si la realización del proyecto software en cuestión es realizable y acompaña los objetivos de la organización donde se planea implementarlo, para realizar dicho estudio se debe analizar la situación de la organización como así todos sus recursos disponibles</p>
    <button type="button" class="btn btn-outline-dark my-3" data-toggle="modal" data-target="#modal-estudio_factibilidad">
        <i class="fas fa-file-pdf fa-fw mr-1"></i>Ver documento
    </button>
</div>

<!-- Modelo de casos de uso-->
<div id="modelo_cu" class="tab-pane fade" role="tabpanel">
    <h3>Modelo de Casos de Uso</h3>
    <p class="text-justify">El modelo de casos de uso describe la funcionalidad propuesta del nuevo sistema.</p>
    <p class="text-justify">Este modelo se basa en la descripción de elementos o usuarios externos al sistema (actores) y de la funcionalidad del sistema (casos de uso). Un Modelo de Casos de Uso describe los requerimientos funcionales de un actor en términos de las interacciones, la utilización de este modelo presenta el sistema desde la perspectiva de su uso y esquematiza cómo proporcionará valor a sus usuarios.</p>
    <p class="text-justify">El modelo de casos de uso sirve como acuerdo entre clientes y desarrolladores para limitar las funciones con que dispondrá el sistema.</p>
    
    <p class="font-weight-bold mt-2">Casos de uso de GuIMI:</p>
    <ul>
        <li>CU01: Acceder a opciones de contenido</li>
        <li>CU02: Mostrar texto</li>
        <li>CU03: Mostrar imagen</li>
        <li>CU04: Mostrar video</li>
        <li>CU05: Mostrar audio</li>
        <li>CU06: Compartir en redes sociales</li>
        <li>CU07: Mostrar novedades</li>
        <li>CU08: Enviar mensaje</li>
        <li>CU09: Mostrar información del museo</li>
        <li>CU10: Administrar novedades</li>
        <li>CU11: Administrar colección</li>
        <li>CU12: Iniciar sesión</li>
    </ul>

    <button type="button" class="btn btn-outline-dark my-3" data-toggle="modal" data-target="#modal-modelo_cu">
        <i class="fas fa-file-pdf fa-fw mr-1"></i>Ver documento
    </button>
</div>

<!-- Modelo de datos-->
<div id="modelo_datos" class="tab-pane fade" role="tabpanel">
    <h3>Modelo de datos</h3>
    <p class="text-justify">...</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>

<!--                            Modelo de diseño-->
<div id="modelo_disenio" class="tab-pane fade" role="tabpanel">
    <h3>Modelo de diseño</h3>
    <p class="text-justify">...</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>

<!--                            Modelo de negocio-->
<div id="modelo_negocio" class="tab-pane fade" role="tabpanel">
    <h3>Modelo de negocio</h3>
    <p class="text-justify">...</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>

<!--                            Prototipo de interfaz-->
<div id="prototipo_interfaz" class="tab-pane fade" role="tabpanel">
    <h3>Prototipo de interfaz</h3>
    <p class="text-justify">...</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>

<!--                            Arquitectura del sistema-->
<div id="arquitectura_sistema" class="tab-pane fade" role="tabpanel">
    <h3>Arquitectura del sistema</h3>
    <p class="text-justify">...</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>

<!--                            Modelo de visión-->
<div id="modelo_vision" class="tab-pane fade" role="tabpanel">
    <h3>Modelo de visión</h3>
    <p class="text-justify">...</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>

<!--                            Especificación de caso de uso-->
<div id="especificacion_cu" class="tab-pane fade" role="tabpanel">
    <h3>Especificación de caso de uso</h3>
    <p class="text-justify">...</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>

<!--                            Glosario-->
<div id="glosario" class="tab-pane fade" role="tabpanel">
    <h3>Glosario</h3>
    <p class="text-justify">...</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>




<!-- Modal -->
<div class="modal fade" id="modal-estudio_factibilidad" tabindex="-1" aria-labelledby="label-estudio_factibilidad" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="label-estudio_factibilidad">Estudio de factibilidad</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <embed src='../../documentacion/An%C3%A1lisis%20y%20dise%C3%B1o/GuIMI%20-%20Estudio%20de%20Factibilidad.pdf' 'type=application/pdf' width="100%" height="600px">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-modelo_cu" tabindex="-1" aria-labelledby="label-modelo_cu" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="label-modelo_cu">Modelo de Casos de Uso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <embed src='../../documentacion/An%25C3%25A1lisis%2520y%2520dise%25C3%25B1o/GuIMI%20-%20Modelo%20de%20Casos%20de%20Uso.pdf' 'type=application/pdf' width="100%" height="600px">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
