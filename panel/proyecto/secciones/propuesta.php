<!--SECCIÓN: PROPUESTA-->

<div id="propuesta" class="tab-pane fade" role="tabpanel">
    <h3>Propuesta</h3>
    <p class="text-justify">La propuesta de desarrollo es el primer documento formal que recibe el posible cliente, donde se manifiesta concretamente la intención de desarrollar un nuevo sistema y es donde se especifican qué características tendrá este desarrollo.</p>
    <p class="text-justify">También esta propuesta sirve como acuerdo entre las partes ya que contienen un análisis técnico y una propuesta técnica asociada, dejando en claro qué es lo que se planea desarrollar, como así también a qué costo.</p>
    <p class="text-justify">Con estos objetivos, se desarrollarán a continuación los siguientes documentos:</p>
    <ul>
        <li class="text-uppercase small">Propuesta de desarrollo</li>
    </ul>

    <div class="observacion observacion-info">
        <p class="text-justify text-90">
            <span class="font-weight-bold mr-1">NOTA:</span>
            <span>Los documentos aquí presentados constituyen la versión final de los mismos, luego de haber sido modificados a lo largo del proyecto.</span>
        </p>
    </div>
</div>

<!-- propuesta_desarrollo-->
<div id="propuesta_desarrollo" class="tab-pane fade" role="tabpanel">
    <h3>Propuesta de desarrollo</h3>
    <p class="text-justify">El propósito de este documento es brindar al cliente los resultados del análisis realizado por el equipo, brindando las características actuales del sistema, la definición del problema y las características de la aplicación de software que se propone como solución al mismo.</p>
    <p class="text-justify">Además, se documentan las características generales del equipo de trabajo que desarrollará el sistema propuesto y una planificación estimada de los tiempos y costos asociados al proyecto.</p>

    <button type="button" class="btn btn-outline-dark my-3" data-toggle="modal" data-target="#modal-propuesta_desarrollo">
        <i class="fas fa-file-pdf fa-fw mr-1"></i>Ver documento
    </button>

    <!-- Modal -->
    <div class="modal fade" id="modal-propuesta_desarrollo" tabindex="-1" aria-labelledby="label-propuesta_desarrollo" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="label-propuesta_desarrollo">Propuesta de desarrollo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <embed src='../../documentacion/Propuesta/Propuesta%20de%20Desarrollo.pdf' 'type=application/pdf' width="100%" height="600px">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>



<!--
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
-->
