<!--SECCIÓN: GESTIÓN DE CALIDAD-->

<div id="gestion_calidad" class="tab-pane fade" role="tabpanel">
    <h3>Gestión de Calidad</h3>
    <p class="text-justify">El análisis consiste en obtener una visión del sistema que se preocupa de ver qué hace, de modo que sólo se interesa por los requisitos funcionales. El diseño, por su parte, es un refinamiento del análisis que tiene en cuenta los requisitos no funcionales, es decir, cómo cumple el sistema sus objetivos.</p>
    <p class="text-justify">Con estos objetivos, se desarrollarán a continuación los siguientes documentos:</p>
    <ul>
        <li class="text-uppercase small">Gestión de configuraciones</li>
        <li class="text-uppercase small">Gestión de riesgos</li>
        <li class="text-uppercase small">Estándar de documentación</li>
    </ul>

    <div class="observacion observacion-info">
        <p class="text-justify">
            <span class="font-weight-bold mr-1">NOTA:</span>
            <span>Los documentos aquí presentados constituyen la versión final de los mismos, luego de haber sido modificados a lo largo del proyecto.</span>
        </p>
    </div>
</div>

<!-- Gestión de configuraciones-->
<div id="gestion_configuraciones" class="tab-pane fade" role="tabpanel">
    <h3>Gestión de configuraciones</h3>
    <p class="text-justify">...</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>

<!-- Gestión de riesgos-->
<div id="gestion_riesgos" class="tab-pane fade" role="tabpanel">
    <h3>Gestión de riesgos</h3>
    <p class="text-justify">Este documento incluye una lista de riesgos conocidos y vigentes en el proyecto, con acciones específicas de contingencia o   mitigación.</p>
    <p class="text-justify">También se llevará a cabo un seguimiento de cada riesgo para tener registro de las acciones tomadas para cada uno.</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>

<!-- Estándar de documentación-->
<div id="estandar_documentacion" class="tab-pane fade" role="tabpanel">
    <h3>Estándar de documentación</h3>
    <p class="text-justify">...</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>
