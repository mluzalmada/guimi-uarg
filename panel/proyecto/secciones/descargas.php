<!--SECCIÓN: DESCARGAS-->
<div id="descargas" class="tab-pane fade" role="tabpanel">
    <h3>Descargas</h3>
    <div class="row row-flex">
        <div class="col-xs-12 col-sm-6">
            <p class="text-muted m-b-1">Manual de uso del sistema GuIMI en PDF (0.9 Mb) </p>
            <a download class="btn btn-primary btn-sm" href=""><i class="fa fa-download"></i>&nbsp; Descargar archivo</a>
        </div>
    </div>
</div>
