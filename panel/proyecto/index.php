<?php include_once '../lib/ControlAcceso.Class.php'; ?>

<html>

<head>
    <meta charset="UTF-8">
    <?php include_once('../lib/headers.php'); ?>

    <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Proceso de desarrollo</title>
</head>

<body class="sticky-footer">

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container navbar-dark bg-dark">
            <a class="navbar-brand" href="menu_principal.php">
                <img src="../lib/img/Logo-GUIMI-blanco.png" height="30" class="d-inline-block align-top" alt="">
            </a>
            <span class="navbar-text">
                <?php echo Constantes::NOMBRE_SISTEMA; ?> - Proceso de Desarrollo
            </span>
        </div>
    </nav>

    <div class="container mt-4">
        <section id="main-content">
            <article>
                <div class="card">
                    <div class="card-header">
                        <h3> <?php echo Constantes::NOMBRE_SISTEMA; ?> - Proceso de Desarrollo</h3>
                    </div>
                    <div class="card-body">
                        <div class="row justify-content-between">
                            <div class="col-md-7">
                                <p>Este proyecto surge en la cátedra <span class="font-weight-bold">Laboratorio de Desarrollo de Software</span> de las carreras Analista de Sistemas y Licenciatura de Sistemas en la UARG - UNPA. </p>
                                <p>En el desarrollo de GuIMI se utilizan frameworks desarrollados en esta casa educativa: la <span class="font-weight-bold">metodología PSI</span> para el proceso de desarrollo y <span class="font-weight-bold">UargFlow BS</span> para la autenticación y gestión de roles y permisos de usuarios.</p>

                                <h6 class="font-weight-bold mt-2">Equipo de desarrollo</h6>
                                <ul>
                                    <li>Almada, María Luz</li>
                                    <li>Trinidad, Franco Alejandro</li>
                                </ul>
                                <h6 class="font-weight-bold mt-2">Docentes</h6>
                                <ul>
                                    <li>Mg. Sofía, Albert Osiris</li>
                                    <li>Lic. Gesto, Esteban</li>
                                    <li>Lic. Hallar, Karim</li>
                                </ul>

                                <h6 class="font-weight-bold mt-2">Institución</h6>
                                <span>Escuela de Sistemas e Informática.<br>
                                    Unidad Académica Río Gallegos.<br>
                                    Universidad nacional de la Patagonia Austral</span>

                            </div>
                            <div class="col-md-5">
                                <img class="img-grande w-auto" src="../lib/img/unpa_exterior2.jpg" onerror="this.src='../lib/img/unpa_exterior1.jpg'">
                            </div>

                        </div>
                        <div class="row mt-8">
                            <div class="col-12">
                                <div class="alert alert-info" role="alert">
                                    <h4 class="alert-heading">Proyecto GuIMI</h4>
                                    <p>En las siguientes secciones se describen las actividades del proceso de software utilizado en el proyecto y se incluyen los documentos relacionados con cada una de ellas.</p>
                                    <div class="row justify-content-end">
                                        <div class="col">
                                           <a href="_indice.php">
                                            <button class="btn btn-outline-info float-right">Comenzar 
<!--                                            <i class="fas fa-arrow-right-to-bracket fa-fw ml-1"></i>-->
                                            <i class="fas fa-circle-arrow-right fa-fw ml-1"></i>
                                            </button>
                                            </a>
                                        </div>
                                    </div>
                                    <hr>
                                    <p class="mb-0 text-90">Tal como los autores del marco de trabajo PSI lo sugieren, el proceso de desarrollo fue adaptado para responder a las necesitades y dimensiones del proyecto GuIMI.</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </article>
        </section>
    </div>
    <?php include_once('../gui/footer.php'); ?>
</body>

</html>
