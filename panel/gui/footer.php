<hr class="hr-footer-light mt-8">
<footer class="footer-guimi">
    <div class="container-fluid">
        <div class="row justify-content-center p-2 text-90">
            <div class="col-md-5 center-sm text-md-left text-sm-center">
                <h5 class="font-weight-bold text-uppercase mt-3 mb-2">Contacto</h5>
                <ul class="fa-ul ml-3">
                    <li><span class="fa-li mt-1"><i class="fa fa-envelope"></i></span><strong>Consultas:</strong> <a href="mailto:museo@guimi.com.ar">museo@guimi.com.ar</a> </li>

                    <li><span class="fa-li mt-1"><i class="fa fa-envelope"></i></span><strong>Soporte técnico:</strong> <a href="mailto:soporte@guimi.com.ar">soporte@guimi.com.ar</a> </li>
                </ul>
            </div>

            <div class="col-md-5 center-sm text-md-right text-sm-center">
                <hr class="hr-sm">
                <h5 class="font-weight-bold text-uppercase mt-3 mb-2">ENLACES </h5>
                <ul class="list-unstyled">
                    <li>
                        <a href="https://www.uarg.unpa.edu.ar/" target="_blank">Portal UNPA-UARG</a>
                    </li>
                    <li>
                        <a href="https://www.uarg.unpa.edu.ar/psi/" target="_blank">Metodología PSI</a>
                    </li>
                    <li>
                        <a href="https://gitlab.com/guimi-unpa/guimi-web" target="_blank">Repositorio GuIMI</a>
                    </li>
                    <li>
                        <a href="../proyecto/index.php" target="_blank">Proyecto GuIMI</a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</footer>
<hr class="hr-footer-guimi">
<footer class="footer-guimi bg-dark text-dark">
    <div class="container">
        <div class="row p-2">
            <div class="col-xs-12">
                <img class="pull-left mr-4 my-2" alt="GuIMI" src="../lib/img/logo_qr.png" onerror="this.src='../lib/img/Logo-GUIMI-blanco.png'">
                <h6 class="copyright mb-0 mt-3 text-grey font-weight-bold text-90">
                    &#169; 2022 &middot; GuIMI &middot; UARGFlow BS <i class="fas fa-earth-americas fa-fw mx-1"></i>&middot; UNPA-UARG
                </h6>
                <p class="copyright text-orange-guimi small">
                    Sistema desarrollado por el Equipo GuIMI
                    en la cátedra Laboratorio de Desarrollo de Software utilizando la metodología PSI <span class="font-weight-bold" style="font-size:1rem;">&#968;</span>.</p>
            </div>
        </div>
    </div>
</footer>
