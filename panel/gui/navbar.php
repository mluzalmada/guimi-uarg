<nav class="navbar navbar-expand-lg navbar-guimi navbar-dark bg-dark">

    <a class="navbar-brand" href="../app/menu_principal.php">
        <img src="../lib/img/Logo-GUIMI-blanco.png" height="40" class="d-inline-block align-top" alt="GuIMI">
    </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <!--                TODO: Cambiar la verificación de permisos, según corresponda! -->
            <?php if (ControlAcceso::verificaPermiso(PermisosSistema::PERMISO_PERMISOS)) { ?>
            <li class="nav-item">
                <a class="nav-link" href="../app/elementos.php">
                    <i class="bi-box mx-2"></i>
                    Elementos
                </a>
            </li>
            <?php }

            if (ControlAcceso::verificaPermiso(PermisosSistema::PERMISO_PERMISOS)) { ?>
            <li class="nav-item">
                <a class="nav-link" href="../app/contenidos.php">
                    <i class="fa fa-photo-film fa-fw mx-2"></i>
                    Contenidos
                </a>
            </li>
            <?php }
            if (ControlAcceso::verificaPermiso(PermisosSistema::PERMISO_PERMISOS)) { ?>
            <li class="nav-item">
                <a class="nav-link" href="../app/marcadores.php">
                    <i class="bi-qr-code-scan mx-2"></i>
                    Marcadores
                </a>
            </li>
            <?php }
            if (ControlAcceso::verificaPermiso(PermisosSistema::PERMISO_PERMISOS)) { ?>
            <li class="nav-item">
                <a class="nav-link" href="../app/novedades.php">
                    <i class="bi-newspaper mx-2"></i>
                    Novedades
                </a>
            </li>
            <?php }
            if (ControlAcceso::verificaPermiso(PermisosSistema::PERMISO_PERMISOS)) { ?>
            <li class="nav-item">
                <a class="nav-link" href="../app/configuracion_uargflow.php">
                    <i class="bi-tools mx-2"></i>
                    UARG-Flow
                </a>
            </li>
            <?php }
            if (ControlAcceso::verificaPermiso(PermisosSistema::PERMISO_PERMISOS)) { ?>
            <li class="nav-item">
                <a class="nav-link" href="../app/configuracion_guimi.php">
                    <i class="fa-solid fa-palette mx-2"></i>
                    Configuración
                </a>
            </li>
            <?php } ?>
        </ul>
           <?php
        if (ControlAcceso::verificaPermiso(PermisosSistema::PERMISO_PERMISOS)) { ?>
        <span class="navbar-text">
            <div class="row align-items-center mr-3">
                <div class="col">
                    <i class="fas fa-circle-user fa-fw fa-2x text-orange-guimi mr-1"></i>
                </div>
                <div class="col">
                    <div class="row text-orange-guimi font-weight-bold small">
                        ¡Hola <?= $_SESSION['usuario']->nombre; ?>!
                    </div>
                    <div class="row">
                        <a class="small text-grey" href="../app/salir.php">
                            Cerrar sesión
                        </a>
                    </div>
                </div>
            </div>
        </span>
    <?php } ?>
    </div>
</nav>
<?php include_once '../gui/loader.php'; ?>