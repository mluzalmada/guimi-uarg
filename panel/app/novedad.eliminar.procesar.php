<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_COLECCION);
include_once '../modelo/BDConexion.Class.php';
include_once '../modelo/Novedad.Class.php';
include_once '../lib/guimi/registrar_actualizaciones.php';

$respuesta = array();
$respuesta['eliminado'] = false;

if(isset($_POST)) {
    $DatosFormulario = $_POST;
    $novedad_id = $DatosFormulario["id"];
    $Novedad = new Novedad($novedad_id);
    
    if($Novedad->deleteNovedad()) {
//        TODO: ELIMINAR LOS ARCHIVOS DEL SERVIDOR
        $respuesta['eliminado'] = true; 
    }
}
echo json_encode($respuesta);
?>
