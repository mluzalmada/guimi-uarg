<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_PERMISOS);
include_once '../modelo/ColeccionTiposElemento.php';
include_once '../modelo/ColeccionCategoriasElemento.php';
$TipoElemento = new ColeccionTiposElemento();
$CategoriaElemento = new ColeccionCategoriasElemento();
?>

<html>

<head>
    <?php include_once('../lib/headers.php'); ?>

    <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Crear Elemento</title>
</head>

<body class="sticky-footer">
    <?php include_once '../gui/navbar.php'; ?>
    <div class="container">
        <form enctype="multipart/form-data" id="form-crear_elemento">
            <div class="card">
                <div class="card-header">
                    <h3>Crear Elemento</h3>
                    <p>
                        Complete los campos a continuaci&oacute;n.
                        Luego, presione el bot&oacute;n <b>Confirmar</b>.<br />
                        Si desea cancelar, presione el bot&oacute;n <b>Cancelar</b>.
                    </p>
                </div>
                <div class="card-body">
                    <h4>Propiedades</h4>

                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="id_omeka">ID Omeka</label>
                                <input type="text" name="id_omeka" class="form-control" id="id_omeka" placeholder="Ingrese el nombre del Elemento" required="">
                            </div>
                        </div>
                        <div class="col-md-6 col-12"><br>
                            <button class="btn btn-outline-secondary btn-block mt-2" disabled>Importar datos de Omeka</button>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Ingrese el nombre del Elemento" required="">
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="tipo_elemento_id">Tipo</label>
                                <select class="form-control" name="tipo_elemento_id" id="tipo_elemento_id">
                                    <?php 
                               foreach ($TipoElemento->getTiposElemento() as $Tipo) { ?>
                                    <option value="<?= $Tipo->getId(); ?>" id="tipo_<?= $Tipo->getId(); ?>">
                                        <?= $Tipo->getNombre(); ?>
                                    </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="categoria_elemento_id">Categoría</label>
                                <select class="form-control" name="categoria_elemento_id" id="categoria_elemento_id">
                                    <?php 
                               foreach ($CategoriaElemento->getCategoriasElemento() as $Categoria) { ?>
                                    <option value="<?= $Categoria->getId(); ?>" id="categoria_<?= $Categoria->getId(); ?>">
                                        <?= $Categoria->getNombre(); ?>
                                    </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="estado_conservacion">Estado:</label>
                                <span class="clasificacion-estado">
                                    <input class="input-estrellas" id="estado_5" type="radio" name="estado_conservacion" value="5">
                                    <label class="label-estrellas" for="estado_5">&#9733;</label>
                                    <input class="input-estrellas" id="estado_4" type="radio" name="estado_conservacion" value="4">
                                    <label class="label-estrellas" for="estado_4">&#9733;</label>
                                    <input class="input-estrellas" id="estado_3" type="radio" name="estado_conservacion" value="3" checked>
                                    <label class="label-estrellas" for="estado_3">&#9733;</label>
                                    <input class="input-estrellas" id="estado_2" type="radio" name="estado_conservacion" value="2">
                                    <label class="label-estrellas" for="estado_2">&#9733;</label>
                                    <input class="input-estrellas" id="estado_1" type="radio" name="estado_conservacion" value="1">
                                    <label class="label-estrellas" for="estado_1">&#9733;</label>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <p class="text-muted font-italic small" id="texto_estado">(Indica el estado del elemento con estrellas.)</p>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="descripcion">Descripción</label>
                                <textarea class="form-control" rows="3" id="descripcion" name="descripcion" placeholder="Ingrese la descripción del elemento"></textarea>
                            </div>
                        </div>

                        <div class="col-md-4 col-12">
                            <div class="form-group">
                                <label for="fabricante">Fabricante</label>
                                <input type="text" name="fabricante" class="form-control" id="fabricante" placeholder="Ingrese el nombre del Fabricante">
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="form-group">
                                <label for="modelo">Modelo</label>
                                <input type="text" name="modelo" class="form-control" id="modelo" placeholder="Ingrese el modelo del Elemento">
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="form-group">
                                <label for="anio_referencia">Año de referencia</label>
                                <input type="tel" name="anio_referencia" class="form-control" id="anio_referencia" placeholder="Ingrese el año de referencia del Elemento">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 align-content-center titulo-seccion mb-3">
                            ENTREGA
                        </div>
                        <hr>
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="entrega_caracter" class="control-label">En carácter de:</label>
                                <select class="form-control" id="entrega_caracter" name="entrega_caracter">
                                    <option value="" selected disabled hidden>Elige...</option>
                                    <option value="1">Donación</option>
                                    <option value="2">En comodato</option>
                                    <option value="3">Préstamo</option>
                                    <option value="4">Otro</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="entrega_nombre">Nombre de la persona o entidad</label>
                                <input type="text" name="entrega_nombre" class="form-control" id="entrega_nombre" placeholder="Ingrese el nombre de la persona/entidad que entrega el elemento" required="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 align-content-center titulo-seccion mb-3">
                            EN EL MUSEO
                        </div>
                        <hr>
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="ubicacion">Ubicación</label>
                                <select class="custom-select" id="ubicacion" name="ubicacion">
                                    <option value="" selected disabled hidden>Elige...</option>
                                    <option value="1">En muestra</option>
                                    <option value="2">En depósito</option>
                                    <option value="3">Otro</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="visible">Visible en la App</label>
                                <div class="form-check my-1 mr-sm-2">
                                    <input type="radio" name="visible" class="check-input" id="visible_1" value="1">
                                    <label class="mr-4" for="visible_1">Si</label>
                                    <input type="radio" name="visible" class="check-input" id="visible_0" value="0" checked>
                                    <label class="mr-4" for="visible_0">No</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="tamanio_fisico">Indique el tamaño del Elemento:</label>
                                <input type="range" class="custom-range px-3" min="1" max="6" step="1" name="tamanio_fisico" id="tamanio_fisico">
                                <div class="row">
                                    <div class="col-2 text-center">
                                        <label class="label-tamanio_fisico" data-tamanio="1">
                                            <span class="small">XS</span><br>
                                            <i class="fas fa-microscope fa-fw"></i>
                                        </label>
                                    </div>
                                    <div class="col-2 text-center">
                                        <label class="label-tamanio_fisico" data-tamanio="2">
                                            <span class="small">S</span><br>
                                            <img xmlns="http://www.w3.org/2000/svg" src="../lib/fontawesome/svgs_guimi/fa-hand-holding-box.svg" class="icon-guimi">
                                        </label>
                                    </div>
                                    <div class="col-2 text-center">
                                        <label class="label-tamanio_fisico" data-tamanio="3">
                                            <span class="small">M</span><br>
                                            <img xmlns="http://www.w3.org/2000/svg" src="../lib/fontawesome/svgs_guimi/fa-person-carry-box.svg" class="icon-guimi">
                                        </label>
                                    </div>
                                    <div class="col-2 text-center">
                                        <label class="label-tamanio_fisico" data-tamanio="4">
                                            <span class="small">L</span><br>
                                            <i class="fas fa-people-carry-box fa-fw"></i>
                                        </label>
                                    </div>
                                    <div class="col-2 text-center">
                                        <label class="label-tamanio_fisico" data-tamanio="5">
                                            <span class="small">XL</span><br>
                                            <i class="fas fa-dolly fa-fw"></i>
                                        </label>
                                    </div>
                                    <div class="col-2 text-center">
                                        <label class="label-tamanio_fisico" data-tamanio="6">
                                            <span class="small">XXL</span><br>
                                            <i class="fas fa-truck-ramp-box fa-fw"></i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="foto_ruta" class="col-sm-2 control-label">Foto: </label>
                                <div class="col-sm-10">
                                    <input type="file" class="form-control w-100" id="foto_ruta" name="foto_ruta">
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row justify-content-between">
                            <a href="elementos.php">
                                <button type="button" class="btn btn-outline-danger">
                                    <i class="fas fa-xmark fa-fw mr-1"></i> Cancelar
                                </button>
                            </a>
                            <button type="submit" class="btn btn-outline-success">
                                <i class="fas fa-check fa-fw mr-1"></i> Confirmar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>


    <!-- MODAL RESPUESTA -->
    <div class="modal fade" id="modal-respuesta" tabindex="-1" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div id="modal-contenido">
                        <span id="modal-icon" class="fas fa-check fa-fw my-4 text-success" style="font-size:6rem;"></span>
                        <h5 id="modal-titulo">Creación de Elemento</h5>
                        <h3 id="modal-subtitulo" class="my-2 text-uppercase"></h3>

                        <div id="modal-btn_cerrar" class="row justify-content-around my-3">
                            <div class="col-6">
                                <a href="elementos.php">
                                    <button type="button" class="btn btn-outline-success">Cerrar</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include_once '../gui/footer.php'; ?>
    <script type="application/javascript" src="../lib/js/funciones_ajax/elemento.crear.js"></script>
</body>

</html>
