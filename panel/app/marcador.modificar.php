<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_PERMISOS);
include_once '../modelo/Elemento.Class.php';
include_once '../modelo/Marcador.Class.php';

$id = $_GET["id"];

$Marcador = new Marcador($id);
$elemento_id = $Marcador->getElementoId();
$Elemento = new Elemento($elemento_id);
?>
<html>

<head>
    <?php include_once('../lib/headers.php'); ?>
    <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Actualizar Marcador</title>
</head>

<body class="sticky-footer">
    <?php include_once '../gui/navbar.php'; ?>
    <div class="container">
        <div class="row justify-content-between mb-3">
            <div class="col-6">
                <a href="marcadores.php?id=<?=$elemento_id?>">
                    <button type="button" class="btn btn-primary">
                        <span class="fas fa-arrow-left fa-fw"></span> Volver a Marcadores
                    </button>
                </a>
            </div>
        </div>

        <form id="form-editar_marcador" enctype="multipart/form-data">
            <div class="card">
                <div class="card-header">
                    <div class="row justify-content-between">
                        <div class="col-9">
                            <h3>Actualizar marcador del elemento <span class="text-uppercase"><?= $Elemento->getNombre(); ?></span></h3>
                            <p>
                                Complete los campos a continuaci&oacute;n.
                                Luego, presione el bot&oacute;n <b>Confirmar</b>.<br />
                                Si desea cancelar, presione el bot&oacute;n <b>Cancelar</b>.
                            </p>
                        </div>
                        <div class="col-3">
                            <div class="text-center position-relative">
                                <a href="" data-toggle="modal" data-target="#modal-vista_previa">
                                    <img class="img-tabla" src="../<?= $Elemento->getFotoRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <input type="hidden" id="marcador_id" name="marcador_id" value="<?=$Marcador->getId();?>">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="imagen_ruta">Imagen del marcador <span class="font-italic text-muted">- en formato PNG</span>:</label>
                                <input type="file" class="form-control" id="imagen_ruta" name="imagen_ruta" accept="image/png" required="" value="<?=$Marcador->getImagenRuta();?>">
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="patron_ruta">Patrón del marcador <span class="font-italic text-muted">- en formato PATT</span>:</label>
                                <input type="file" class="form-control" id="patron_ruta" name="patron_ruta" accept=".patt" required="" value="<?=$Marcador->getPatronRuta();?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="row justify-content-around text-center">
                        <div class="col-6">
                            <a href="marcadores.php?id=<?=$Marcador->getElementoId();?>">
                                <button type="button" class="btn btn-outline-danger">
                                    <i class="fas fa-xmark fa-fw mr-1"></i> Cancelar
                                </button>
                            </a>
                        </div>
                        <div class="col-6">
                            <button type="submit" class="btn btn-outline-success">
                                <i class="fas fa-check fa-fw mr-1"></i> Confirmar
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade" tabindex="-1" aria-hidden="true" id="modal-vista_previa">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Foto del elemento: <?= $Elemento->getNombre(); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img src="../<?= $Elemento->getFotoRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'" width="100%">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL RESPUESTA -->
    <div class="modal fade" id="modal-respuesta" tabindex="-1" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div id="modal-contenido">
                        <span id="modal-icon" class="fas fa-check fa-fw my-4 text-success" style="font-size:6rem;"></span>
                        <h5 id="modal-titulo">Actualización de <?= $Elemento->getNombre(); ?></h5>
                        <h3 id="modal-subtitulo" class="my-2 text-uppercase"></h3>

                        <div id="modal-btn_cerrar" class="row justify-content-around my-3">
                            <div class="col-6">
                                <a href="marcadores.php?id=<?=$elemento_id;?>">
                                    <button type="button" class="btn btn-outline-success">Cerrar</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include_once '../gui/footer.php'; ?>
    <script type="application/javascript" src="../lib/js/funciones_ajax/marcador.modificar.js"></script>
    <script type="application/javascript" src="../lib/js/funciones.js"></script>
</body>

</html>
