<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_PERMISOS);
include_once '../modelo/ColeccionElementos.php';
include_once '../modelo/Elemento.Class.php';
include_once '../modelo/ColeccionTiposElemento.php';
include_once '../modelo/ColeccionCategoriasElemento.php';
$ColeccionElementos = new ColeccionElementos();
$TipoElemento = new ColeccionTiposElemento();
$CategoriaElemento = new ColeccionCategoriasElemento();
?>

<html>

<head>
    <meta charset="UTF-8">
    <?php include_once('../lib/headers.php'); ?>

    <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Elementos</title>

</head>

<body class="sticky-footer">

    <?php include_once '../gui/navbar.php'; ?>
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h3>Elementos</h3>
            </div>
            <div class="card-body">
                <p>
                    <a href="elemento.crear.php">
                        <button type="button" class="btn btn-success">
                            <i class="fas fa-plus fa-fw mr-1"></i> Nuevo Elemento
                        </button>
                    </a>
                </p>
                <table class="table table-hover table-sm">
                    <tr class="">
                        <th>Id</th>
                        <th>Foto referencia</th>
                        <th>Nombre</th>
                        <th class="text-center">Visible</th>
                        <th>Opciones</th>
                    </tr>
                        <?php foreach ($ColeccionElementos->getElementos() as $Elemento) { 
                    if($Elemento->getVisible() == '0') { ?>
                    <tr class="table-secondary">
                    <?php }
                        else { ?>
                    <tr>    
                        <?php } ?>
                        <td> <?=$Elemento->getId();?> </td>
                        <td>
                            <div class="text-center position-relative">
                                <a href="" data-toggle="modal" data-target="#modal-vista_previa_<?= $Elemento->getId(); ?>">
                                    <img class="img-tabla" src="../<?= $Elemento->getFotoRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'">
                                </a>
                            </div>
                        </td>
                        <td>
                            <?= $Elemento->getNombre(); ?>
                        </td>
                        <td class="text-center">
                            <?php if ($Elemento->getVisible() == '1') { ?>
                            <i class="fas fa-eye fa-fw text-success"></i>
                            <?php } else { ?>
                            <i class="fas fa-eye-slash fa-fw text-danger"></i>
                            <?php } ?>
                        </td>
                        <td>
                            <button type="button" class="btn btn-outline-info btn-sm w-100 mb-1" data-toggle="modal" data-target="#modal-elemento_ver_<?= $Elemento->getId(); ?>">
                                <i class="fas fa-magnifying-glass-plus fa-fw"></i>
                                <span class="ml-1">Ver detalle</span>
                            </button><br>
                            <a title="Modificar" href="elemento.modificar.php?id=<?= $Elemento->getId(); ?>">
                                <button type="button" class="btn btn-outline-warning btn-sm w-100 mb-1">
                                    <i class="fas fa-pen-to-square fa-fw"></i>
                                    <span class="ml-1">Editar</span>
                                </button><br>
                            </a>
                            <a title="Contenidos" href="contenidos.php?id=<?= $Elemento->getId(); ?>">
                                <button type="button" class="btn btn-outline-secondary btn-sm w-100 mb-1">
                                    <i class="fas fas fa-photo-film fa-fw fa-fw"></i>
                                    <span class="ml-1">Contenidos</span>
                                </button><br>
                            </a>
                            <a title="Marcadores" href="marcadores.php?id=<?= $Elemento->getId(); ?>">
                                <button type="button" class="btn btn-outline-success btn-sm w-100 mb-1">
                                    <i class="bi-qr-code-scan"></i>
                                    <span class="ml-1">Marcadores</span>
                                </button><br>
                            </a>
                            <button type="button" class="btn btn-outline-danger btn-sm w-100 mb-1" data-toggle="modal" data-target="#modal-elemento_eliminar_<?= $Elemento->getId(); ?>">
                                <i class="fas fa-trash-can fa-fw"></i>
                                <span class="ml-1">Eliminar</span>
                            </button><br>
                        </td>
                    </tr>

                    <!-- MODAL ELIMINAR -->
                    <div class="modal fade" id="modal-elemento_eliminar_<?= $Elemento->getId(); ?>" tabindex="-1" aria-labelledby="elemento_eliminar" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body text-center">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div id="modal-contenido_<?= $Elemento->getId(); ?>">
                                        <span id="modal-icon_<?= $Elemento->getId(); ?>" class="fas fa-trash-can fa-fw my-4 text-danger" style="font-size:6rem;"></span>
                                        <h5 id="modal-titulo_<?= $Elemento->getId(); ?>">¿Estás seguro/a de eliminar?</h5>
                                        <h3 id="modal-subtitulo_<?= $Elemento->getId(); ?>" class="my-2 text-uppercase"> <?= $Elemento->getNombre(); ?></h3>
                                        <div id="modal-form_<?= $Elemento->getId(); ?>">
                                            <form data-id="<?= $Elemento->getId(); ?>">
                                                <p class="alert alert-danger my-3">
                                                    <i class="fas fa-triangle-exclamation fa-fw mr-1"></i> ATENCI&Oacute;N: Esta operaci&oacute;n no puede deshacerse.
                                                </p>

                                                <input type="hidden" name="id" class="form-control" id="id" value="<?= $Elemento->getId(); ?>">
                                                <div class="row justify-content-around">
                                                    <div class="col-6">
                                                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><span class="fas fa-xmark fa-fw mr-1"></span> Cancelar</button>
                                                    </div>
                                                    <div class="col-6">
                                                        <button type="submit" class="btn btn-outline-success">
                                                            <span class="fas fa-check fa-fw"></span> Sí, deseo eliminar
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div id="modal-btn_cerrar_<?= $Elemento->getId(); ?>" class="row justify-content-around my-3" style="display:none;">
                                            <div class="col-6">
                                                <a href="elementos.php">
                                                    <button type="button" class="btn btn-outline-success">Cerrar</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- MODAL VER -->
                    <div class="modal fade" id="modal-elemento_ver_<?= $Elemento->getId(); ?>" tabindex="-1" aria-labelledby="elemento_ver" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title text-uppercase" id="label-elemento_ver"><?= $Elemento->getNombre(); ?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                            <span class="text-uppercase font-weight-bold small">Nombre: </span>
                                            <p><?= $Elemento->getNombre(); ?></p>
                                            <span class="text-uppercase font-weight-bold small">Tipo: </span>
                                            <?php 
           foreach ($TipoElemento->getTiposElemento() as $Tipo) {
               if($Elemento->getTipoId() == $Tipo->getId()) {
                   echo "<p>".$Tipo->getNombre()."</p>";
               }
           } ?>
                                            <span class="text-uppercase font-weight-bold small">Categoria: </span><?php 
           foreach ($CategoriaElemento->getCategoriasElemento() as $Categoria) { 
               if($Elemento->getCategoria() == $Categoria->getId()) {
                            echo "<p>".$Categoria->getNombre()."</p>";
                        }
                 } ?>
                                        </div>
                                        <div class="col-md-6 col-12 text-center div-imagen">
                                            <img class="img-tabla mb-1" src="../<?= $Elemento->getFotoRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'">
                                            <a href="../<?= $Elemento->getFotoRuta(); ?>" target="_blank">
                                                <button class="btn btn-outline-secondary btn-sm py-0 mb-2"><small>Ampliar imagen <i class="bi bi-box-arrow-up-right ml-1"></i></small></button>
                                            </a><br>
                                            <span class="text-uppercase font-weight-bold small">Estado de conservación: </span>
                                            <?php
$estrellas = 5;
for ($i = 1; $i <= $Elemento->getEstadoConservacion(); $i++) {
    echo "<span class='text-orange'>&#9733;</span>";
    $estrellas --; 
}
if($estrellas) {
    for ($e = 0; $e < $estrellas; $e++) {
        echo "<span class='label-estrellas'>&#9733;</span>";
    }
}   
?>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-12">
                                            <span class="text-uppercase font-weight-bold small">Descripción: </span>
                                            <p> <?= $Elemento->getDescripcion(); ?></p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-6">
                                            <span class="text-uppercase font-weight-bold small">Fabricante: </span>
                                            <p> <?= $Elemento->getFabricante(); ?></p>
                                        </div>

                                        <div class="col-6">
                                            <span class="text-uppercase font-weight-bold small">Año de referencia: </span>
                                            <p> <?= $Elemento->getAnioReferencia(); ?></p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-6">
                                            <span class="text-uppercase font-weight-bold small">Entregado por: </span>
                                            <p> <?= $Elemento->getEntregaNombre(); ?></p>
                                        </div>
                                        <div class="col-6">
                                            <span class="text-uppercase font-weight-bold small">Entregado en carácter de: </span>
                                            <p> <?= $Elemento->getEntregaCaracter(); ?></p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-6">
                                            <span class="text-uppercase font-weight-bold small">Ubicación: </span>
                                            <p> <?= $Elemento->getUbicacion(); ?></p>
                                        </div>
                                        <div class="col-6">
                                            <span class="text-uppercase font-weight-bold small">Visible: </span>
                                            <p> <?php if($Elemento->getVisible() == "1") {
                    echo 'Si';
                } else {
                    echo 'No';
                } ?></p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-6">
                                            <span class="text-uppercase font-weight-bold small">ID Omeka: </span>
                                            <p> <?= $Elemento->getIdOmeka(); ?></p>
                                        </div>
                                        <div class="col-6">
                                            <span class="text-uppercase font-weight-bold small">Tamaño: </span>
                                            <span class="detalle_tamanio">
                                                <?php
                                                switch($Elemento->getTamanioFisico()){
                                                    case '1': {
                                                        echo "<span class='mx-1'>XS</span><i class='fas fa-microscope fa-fw'></i>";
                                                        break;
                                                    }
                                                    case '2': {
                                                        echo "<span class='mx-1'>S</span><img xmlns='http://www.w3.org/2000/svg' src='../lib/fontawesome/svgs_guimi/fa-hand-holding-box.svg' class='icon-guimi'>";
                                                        break;
                                                    }
                                                    case '3': {
                                                        echo "<span class='mx-1'>M</span><img xmlns='http://www.w3.org/2000/svg' src='../lib/fontawesome/svgs_guimi/fa-person-carry-box.svg' class='icon-guimi'>";
                                                        break;
                                                    }
                                                    case '4': {
                                                        echo "<span class='mx-1'>L</span><i class='fas fa-people-carry-box fa-fw'></i>";
                                                        break;
                                                    }
                                                    case '5': {
                                                        echo "<span class='mx-1'>XL</span><i class='fas fa-dolly fa-fw'></i>";
                                                        break;
                                                    }
                                                    case '6': {
                                                        echo "<span class='mx-1'>XXL</span><i class='fas fa-truck-ramp-box fa-fw'></i>";
                                                        break;
                                                    }
                                                }
                                                ?>
                                            </span>
                                            <input type="range" class="custom-range" min="1" max="6" step="1" name="tamanio_fisico" id="tamanio_fisico" value="<?= $Elemento->getTamanioFisico(); ?>" disabled>
                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- FIN DEL MODAL-->

                    <!-- MODAL VER IMAGEN-->
                    <div class="modal fade" tabindex="-1" aria-hidden="true" id="modal-vista_previa_<?= $Elemento->getId(); ?>">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Foto del elemento: <?= $Elemento->getNombre(); ?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <img src="../<?= $Elemento->getFotoRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'" width="100%">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php } ?>
                </table>
            </div>
        </div>

        <!-- MODAL Operación exitosa -->
        <div class="modal fade" id="modal-exito" tabindex="-1" aria-labelledby="" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body text-center">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="fas fa-trash-can fa-fw my-4 text-success" style="font-size:6rem;"></span>
                        <h5 class="my-3"> Operaci&oacute;n realizada con &eacute;xito.</h5>

                        <div class="row justify-content-center">
                            <div class="col-6">
                                <button type="button" class="btn btn-outline-primary" data-dismiss="modal"><span class="fas fa-check fa-fw mr-1"></span> Cerrar</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include_once '../gui/footer.php'; ?>
    <script type="application/javascript" src="../lib/js/funciones_ajax/elemento.eliminar.js"></script>
</body>

</html>
