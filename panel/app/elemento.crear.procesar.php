<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_ROLES);
include_once '../modelo/BDConexion.Class.php';

include_once '../modelo/ColeccionElementos.php';
//include_once '../modelo/Elemento.Class.php';

include_once '../lib/guimi/funciones.php';
include_once '../lib/guimi/registrar_actualizaciones.php';

$DatosFormulario = $_POST;
$ArchivoFormulario = $_FILES['foto_ruta'];

BDConexion::getInstancia()->autocommit(false);
BDConexion::getInstancia()->begin_transaction();

$Elemento = new Elemento(null, $DatosFormulario);
$ColeccionElementos = new ColeccionElementos(null);
$ColeccionElementos->addElemento($Elemento);

$elemento_id = false;

$resultado['errores'] = 0;
// El string $resultado['errores'] se mostrará luego de "Ha ocurrido un error..." en caso que su valor sea != 0 

if ($Elemento) {
    $elemento_id = $Elemento->getId();
    
//Se crea un directorio para cada tipo de recurso y patrón asociado al elemento:
    $directorio_recurso_imagen = '../media/elementos/'.$elemento_id.'/imagen';
    $directorio_recurso_texto = '../media/elementos/'.$elemento_id.'/texto';
    $directorio_recurso_audio = '../media/elementos/'.$elemento_id.'/audio';
    $directorio_recurso_video = '../media/elementos/'.$elemento_id.'/video';
    $directorio_patron_png = '../media/elementos/'.$elemento_id.'/patron_png';
    $directorio_patron_patt = '../media/elementos/'.$elemento_id.'/patron_patt';

    crearDirectorio($directorio_recurso_imagen);
    crearDirectorio($directorio_recurso_texto);
    crearDirectorio($directorio_recurso_audio);
    crearDirectorio($directorio_recurso_video);
    crearDirectorio($directorio_patron_png);
    crearDirectorio($directorio_patron_patt);

// Se guarda la imagen preview del elemento:
if($ArchivoFormulario && $elemento_id) {
    $extension = getExtensionArchivo($ArchivoFormulario);
    $directorio = $_SERVER['DOCUMENT_ROOT'].'/media/elementos/'.$elemento_id.'/';

    $nombre_archivo = "preview.".$extension;
    $directorio_bd = 'media/'.$elemento_id.'/';

    // Se actualiza la BD con la ubicación del archivo:
    if (guardarArchivo($ArchivoFormulario, $directorio, $nombre_archivo)) {
        $actualizar_preview = $Elemento->setFotoRuta($directorio_bd.$nombre_archivo);
        
        if(!$actualizar_preview) {
            $resultado['errores'] = "al actualizar la ruta de la imagen $nombre_archivo en la base de datos.";
        }
    }
    else {
        $resultado['errores'] = "al guardar archivo en $directorio$nombre_archivo";
    }
}
    
    // Actualizar la tabla actualizacion_elemento:   
   $actualizacion_id = RegistrarActualizacionElemento('1',$elemento_id); 
    if($actualizacion_id) {
        $resultado['transaccion'] = $actualizacion_id;
    }
}
else {
    BDConexion::getInstancia()->rollback();
    //arrojar una excepcion
    die(BDConexion::getInstancia()->errno);
    
}

echo json_encode($resultado);
?>
