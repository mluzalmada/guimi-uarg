<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_PERMISOS);
include_once '../modelo/BDConexion.Class.php';
include_once '../modelo/Contenido.Class.php';
include_once '../lib/guimi/registrar_actualizaciones.php';

$respuesta = array();
$respuesta['eliminado'] = false;
if(isset($_POST)) {
    $DatosFormulario = $_POST;
    $contenido_id = $DatosFormulario["id"];
    $respuesta['id'] = $contenido_id;
    $Contenido = new Contenido($contenido_id);
    
    $respuesta['nombre'] = $Contenido->getNombre();
    $respuesta['tipo'] = $Contenido->getTipoContenidoId();
    $respuesta['elemento_id'] = $Contenido->getElementoId();
    // Actualizar la tabla actualizacion_elemento:
    $actualizacion_id = RegistrarActualizacionElemento('2',$respuesta['elemento_id'], 'Se elimina el contenido ('.$respuesta["tipo"].'):'.$respuesta["nombre"]);

    if($actualizacion_id) {
        $respuesta['transaccion'] = $actualizacion_id;
    }
    if($Contenido->deleteContenido()) {
//        TODO: ELIMINAR LOS ARCHIVOS DEL SERVIDOR
        $respuesta['eliminado'] = true; 
    }
}
echo json_encode($respuesta);
?>
