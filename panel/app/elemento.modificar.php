<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_PERMISOS);
include_once '../modelo/ColeccionElementos.php';
include_once '../modelo/Elemento.Class.php';

$id = $_GET["id"];
include_once '../modelo/ColeccionTiposElemento.php';
include_once '../modelo/ColeccionCategoriasElemento.php';
$TipoElemento = new ColeccionTiposElemento();
$CategoriaElemento = new ColeccionCategoriasElemento();
$Elemento = new Elemento($id);
?>
<html>

<head>
    <?php include_once('../lib/headers.php'); ?>
    <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Actualizar Elemento</title>
</head>

<body class="sticky-footer">
    <?php include_once '../gui/navbar.php'; ?>
    <div class="container">
        <form id="form-editar_elemento" enctype="multipart/form-data">
            <div class="card">
                <div class="card-header">
                     <div class="row justify-content-between">
                    <div class="col-9">
                    <h3>Actualizar elemento <span class="text-uppercase"><?= $Elemento->getNombre(); ?></span></h3>
                    <p>
                        Complete los campos a continuaci&oacute;n.
                        Luego, presione el bot&oacute;n <b>Confirmar</b>.<br />
                        Si desea cancelar, presione el bot&oacute;n <b>Cancelar</b>.
                    </p>
                        </div>
                    <div class="col-3">
                        <div class="text-center position-relative">
                            <a href="" data-toggle="modal" data-target="#modal-vista_previa">
                                <img class="img-tabla" src="../<?= $Elemento->getFotoRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'">
                            </a>
                        </div>
                    </div>
                </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input type="text" name="nombre" class="form-control" id="nombre" value="<?= $Elemento->getNombre(); ?>" placeholder="Ingrese el nombre del Elemento" required="">
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <label for="tipo_elemento_id">Tipo</label>
                                <select class="form-control" name="tipo_elemento_id" id="tipo_elemento_id">
                                    <?php 
                               foreach ($TipoElemento->getTiposElemento() as $Tipo) { ?>
                                    <option value="<?= $Tipo->getId(); ?>" id="tipo_<?= $Tipo->getId(); ?>" <?php
                                            if($Elemento->getTipoId() == $Tipo->getId()) {
                                                echo " selected";
                                            }
                                            ?>>
                                        <?= $Tipo->getNombre(); ?>
                                    </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="categoria_elemento_id">Categoria</label>
                                <select class="form-control" name="categoria_elemento_id" id="categoria_elemento_id">
                                    <?php 
                               foreach ($CategoriaElemento->getCategoriasElemento() as $Categoria) { ?>
                                    <option value="<?= $Categoria->getId(); ?>" id="categoria_<?= $Categoria->getId(); ?>" <?php
                                            if($Elemento->getCategoria() == $Categoria->getId()) {
                                                echo " selected";
                                            }
                                            ?>>
                                        <?= $Categoria->getNombre(); ?>
                                    </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="estado_conservacion">Estado de conservación</label>
                                <span class="clasificacion-estado">
                                    <input class="input-estrellas" id="estado_5" type="radio" name="estado_conservacion" value="5" <?php
                                           if($Elemento->getEstadoConservacion() == '5') { echo "checked"; }
                                           ?>>
                                    <label class="label-estrellas" for="estado_5">&#9733;</label>
                                    <input class="input-estrellas" id="estado_4" type="radio" name="estado_conservacion" value="4" <?php
                                           if($Elemento->getEstadoConservacion() == '4') { echo "checked"; }
                                           ?>>
                                    <label class="label-estrellas" for="estado_4">&#9733;</label>
                                    <input class="input-estrellas" id="estado_3" type="radio" name="estado_conservacion" value="3" <?php
                                           if($Elemento->getEstadoConservacion() == '3') { echo "checked"; }
                                           ?>>
                                    <label class="label-estrellas" for="estado_3">&#9733;</label>
                                    <input class="input-estrellas" id="estado_2" type="radio" name="estado_conservacion" value="2" <?php
                                           if($Elemento->getEstadoConservacion() == '2') { echo "checked"; }
                                           ?>>
                                    <label class="label-estrellas" for="estado_2">&#9733;</label>
                                    <input class="input-estrellas" id="estado_1" type="radio" name="estado_conservacion" value="1" <?php
                                           if($Elemento->getEstadoConservacion() == '1') { echo "checked"; }
                                           ?>>
                                    <label class="label-estrellas" for="estado_1">&#9733;</label>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <p class="text-muted font-italic small" id="texto_estado">(Indica el estado del elemento con estrellas.)</p>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="descripcion">Descripción</label>
                                <textarea rows="3" class="form-control" name="descripcion" id="descripcion" placeholder="Ingrese la descripción del Elemento"><?= $Elemento->getDescripcion(); ?></textarea>
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="form-group">
                                <label for="fabricante">Fabricante</label>
                                <input type="text" name="fabricante" class="form-control" id="fabricante" value="<?= $Elemento->getFabricante(); ?>" placeholder="Ingrese el nombre del fabricante del Elemento">
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="form-group">
                                <label for="modelo">Modelo</label>
                                <input type="text" name="modelo" class="form-control" id="modelo" value="<?= $Elemento->getModelo(); ?>" placeholder="Ingrese el modelo del Elemento">
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="form-group">
                                <label for="anio_referencia">Año de referencia</label>
                                <input type="text" name="anio_referencia" class="form-control" id="anio_referencia" value="<?= $Elemento->getAnioReferencia(); ?>" placeholder="Ingrese el año de referencia del Elemento">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 align-content-center titulo-seccion mb-3">
                            ENTREGA
                        </div>
                        <hr>
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="entrega_caracter">Entrega en carácter de</label>
                                <select class="form-control" id="entrega_caracter" name="entrega_caracter">
                                    <option value="" selected disabled hidden>Elige...</option>
                                    <option value="1" <?php
                                            if($Elemento->getEntregaCaracter() == "Donación") {
                                                echo " selected";
                                            }
                                            ?>>Donación</option>
                                    <option value="2" <?php
                                            if($Elemento->getEntregaCaracter() == "Comodato") {
                                                echo " selected";
                                            }
                                            ?>>En comodato</option>
                                    <option value="3" <?php
                                            if($Elemento->getEntregaCaracter() == "Préstamo") {
                                                echo " selected";
                                            }
                                            ?>>Préstamo</option>
                                    <option value="4" <?php
                                            if($Elemento->getEntregaCaracter() == "Otro") {
                                                echo " selected";
                                            }
                                            ?>>Otro</option>
                                </select>

                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="entrega_nombre">Entrega en nombre de</label>
                                <input type="text" name="entrega_nombre" class="form-control" id="entrega_nombre" value="<?= $Elemento->getEntregaNombre(); ?>" placeholder="Ingrese el nombre de  quien entrega el Elemento">
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-12 align-content-center titulo-seccion mb-3">
                            EN EL MUSEO
                        </div>
                        <hr>
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="ubicacion">Ubicación</label>
                                <select class="custom-select" id="ubicacion" name="ubicacion">
                                    <option value="1" <?php
                                            if($Elemento->getUbicacion() == "En muestra") {
                                                echo " selected";
                                            }
                                            ?>>En muestra</option>
                                    <option value="2" <?php
                                            if($Elemento->getUbicacion() == "Depósito") {
                                                echo " selected";
                                            }
                                            ?>>En depósito</option>
                                    <option value="3" <?php
                                            if($Elemento->getUbicacion() == "Otro") {
                                                echo " selected";
                                            }
                                            ?>>Otro</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="visible">Visible en la App</label>
                                <div class="form-check my-1 mr-sm-2">
                                    <input type="radio" name="visible" class="check-input" id="visible_1" value="1" <?php
                                           if($Elemento->getVisible() == 1) {
                                               echo " checked";
                                           }
                                           ?>>
                                    <label class="mr-4" for="visible_1">Si</label>
                                    <input type="radio" name="visible" class="check-input" id="visible_0" value="0" <?php
                                           if($Elemento->getVisible() == 0) {
                                               echo " checked";
                                           }
                                           ?>>
                                    <label class="mr-4" for="visible_0">No</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="id_omeka">ID Omeka</label>
                                <input type="text" name="id_omeka" class="form-control" id="id_omeka" value="<?= $Elemento->getIdOmeka(); ?>" placeholder="Ingrese el ID Omeka del Elemento">
                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="tamanio_fisico">Indique el tamaño del Elemento:</label>
                                <input type="range" class="custom-range px-3" min="1" max="6" step="1" name="tamanio_fisico" id="tamanio_fisico">
                                <div class="row">
                                    <div class="col-2 text-center">
                                        <label class="label-tamanio_fisico" data-tamanio="1">
                                            <span class="small">XS</span><br>
                                            <i class="fas fa-microscope fa-fw"></i>
                                        </label>
                                    </div>
                                    <div class="col-2 text-center">
                                        <label class="label-tamanio_fisico" data-tamanio="2">
                                            <span class="small">S</span><br>
                                            <img xmlns="http://www.w3.org/2000/svg" src="../lib/fontawesome/svgs_guimi/fa-hand-holding-box.svg" class="icon-guimi">
                                        </label>
                                    </div>
                                    <div class="col-2 text-center">
                                        <label class="label-tamanio_fisico" data-tamanio="3">
                                            <span class="small">M</span><br>
                                            <img xmlns="http://www.w3.org/2000/svg" src="../lib/fontawesome/svgs_guimi/fa-person-carry-box.svg" class="icon-guimi">
                                        </label>
                                    </div>
                                    <div class="col-2 text-center">
                                        <label class="label-tamanio_fisico" data-tamanio="4">
                                            <span class="small">L</span><br>
                                            <i class="fas fa-people-carry-box fa-fw"></i>
                                        </label>
                                    </div>
                                    <div class="col-2 text-center">
                                        <label class="label-tamanio_fisico" data-tamanio="5">
                                            <span class="small">XL</span><br>
                                            <i class="fas fa-dolly fa-fw"></i>
                                        </label>
                                    </div>
                                    <div class="col-2 text-center">
                                        <label class="label-tamanio_fisico" data-tamanio="6">
                                            <span class="small">XXL</span><br>
                                            <i class="fas fa-truck-ramp-box fa-fw"></i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-12 text-center div-imagen">
                            <img class="img-preview img-rounded" src="../<?= $Elemento->getFotoRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'">

                            <div class="div-overlay">
                                <a href="" data-toggle="modal" data-target="#modal-vista_previa">
                                    <p>Hacé clic para ampliar</p>
                                </a>
                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="foto_ruta">Reemplazar foto</label>
                                <input type="file" name="foto_ruta" class="form-control" id="foto_ruta" value="<?= $Elemento->getFotoRuta(); ?>" placeholder="Ingrese la ruta de la foto del Elemento">
                            </div>
                        </div>

                        <div class="col-12">
                            <input type="hidden" name="id" class="form-control" id="id" value="<?= $Elemento->getId(); ?>">
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="row justify-content-around text-center">
                        <div class="col-6">
                            <a href="elementos.php">
                                <button type="button" class="btn btn-outline-danger">
                                    <i class="fas fa-xmark fa-fw mr-1"></i> Cancelar
                                </button>
                            </a>
                        </div>
                        <div class="col-6">
                            <button type="submit" class="btn btn-outline-success">
                                <i class="fas fa-check fa-fw mr-1"></i> Confirmar
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade" tabindex="-1" aria-hidden="true" id="modal-vista_previa">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Foto del elemento: <?= $Elemento->getNombre(); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img src="../<?= $Elemento->getFotoRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'" width="100%">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL RESPUESTA -->
    <div class="modal fade" id="modal-respuesta" tabindex="-1" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div id="modal-contenido">
                        <span id="modal-icon" class="fas fa-check fa-fw my-4 text-success" style="font-size:6rem;"></span>
                        <h5 id="modal-titulo">Actualización de <?= $Elemento->getNombre(); ?></h5>
                        <h3 id="modal-subtitulo" class="my-2 text-uppercase"></h3>
                        
                        <div id="modal-btn_cerrar" class="row justify-content-around my-3">
                            <div class="col-6">
                                <a href="elementos.php">
                                    <button type="button" class="btn btn-outline-success">Cerrar</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include_once '../gui/footer.php'; ?>
    <script type="application/javascript" src="../lib/js/funciones_ajax/elemento.modificar.js"></script>
</body>

</html>
