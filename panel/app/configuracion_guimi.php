<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_USUARIOS);
include_once '../modelo/ColeccionUsuarios.php';
$ColeccionUsuarios = new ColeccionUsuarios();
?>

<html>

<head>
    <?php include_once('../lib/headers.php'); ?>

    <title><?= Constantes::NOMBRE_SISTEMA; ?> - Configuración</title>
</head>

<body class="sticky-footer">

    <?php include_once '../gui/navbar.php'; ?>

    <div class="container">

        <div class="card">
            <div class="card-header">
                <h3>Configuración de GuIMI</h3>
            </div>
            <div class="card-body p-3">
                <div class="row">
                    <?php if (ControlAcceso::verificaPermiso(PermisosSistema::PERMISO_COLECCION)) { ?>
                    <div class="col-md-4">
                        <a href="../app/tipos_elemento.php">
                            <button class="btn btn-outline-success btn-block p-4">
                                <i class="fas fa-compass-drafting fa-fw mx-2" style="font-size: 3em;"></i>
                                <br>
                                Tipo de Elementos
                            </button>
                        </a>
                        <span class="text-success small">
                            Podés visualizar, crear, modificar y eliminar los tipos de elementos de la colección.
                        </span>
                    </div>
                    <?php }
                    if (ControlAcceso::verificaPermiso(PermisosSistema::PERMISO_COLECCION)) { ?>
                    <div class="col-md-4">
                        <a href="../app/categorias_elemento.php">
                            <button class="btn btn-outline-info btn-block p-4">
                                <i class="fas fa-tag fa-fw mx-2" style="font-size: 3em;"></i>
                                <br>
                                Categoría de Elementos
                            </button>
                        </a>
                        <span class="text-info small">
                            Podés visualizar, crear, modificar y eliminar las diferentes categorías de elementos de la colección.
                        </span>
                    </div>
                    <?php } ?>
                    <?php if (ControlAcceso::verificaPermiso(PermisosSistema::PERMISO_COLECCION)) { ?>
                    <div class="col-md-4">
                        <a href="../app/tipos_contenido.php">
                            <button class="btn btn-outline-warning btn-block p-4">
                                <i class="fas fa-hand-sparkles fa-fw mx-2" style="font-size: 3em;"></i>
                                <br>
                                Tipos de Contenido
                            </button>
                        </a>
                        <span class="text-warning small">
                            Podés visualizar, crear, modificar y eliminar los tipos de Contenido que disponen los elementos de la colección.
                        </span>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <?php include_once '../gui/footer.php'; ?>
</body>

</html>
