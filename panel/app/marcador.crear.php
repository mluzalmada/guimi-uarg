<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_PERMISOS);
include_once '../modelo/ColeccionElementos.php';
include_once '../modelo/ColeccionCategoriasElemento.php';

$ColeccionElementos = new ColeccionElementos();
$CategoriaElemento = new ColeccionCategoriasElemento();
if(isset($_GET["id"])) {
    $id = $_GET["id"];
    $Elemento = new Elemento($id);
}
else {
    $Elemento = new Elemento();
}
?>

<html>

<head>
    <meta charset="UTF-8">
    <?php include_once('../lib/headers.php'); ?>

    <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Crear marcador</title>

</head>

<body class="sticky-footer">
    <?php include_once '../gui/navbar.php'; ?>
    <div class="container">
        <div class="row justify-content-between mb-3">
            <div class="col-6">
                <?php if($Elemento->getExiste()) { ?>
                <a href="marcadores.php?id=<?=$Elemento->getId();?>">
                    <button type="button" class="btn btn-primary">
                        <span class="fas fa-arrow-left fa-fw"></span> Volver a Marcadores
                    </button>
                </a>
                <?php } else { ?>
                <a href="menu_principal.php">
                    <button type="button" class="btn btn-primary">
                        <span class="fas fa-arrow-left fa-fw"></span> Volver al menú principal
                    </button>
                </a>
                <?php } ?>
            </div>
        </div>

        <div class="card">
            <!--// Si recibe por GET el id del archivo al que se le asignará un nuevo marcador, se muestra el formulario: -->
            <?php 
if($Elemento->getExiste()) { ?>
            <form enctype="multipart/form-data" id="form-crear_marcador">
                <div class="card-header">
                    <div class="row justify-content-between">
                        <div class="col-9">
                            <h3>Crear Marcador para <?= $Elemento->getNombre();?></h3>
                            <p>
                                Complete los campos a continuaci&oacute;n.
                                Luego, presione el bot&oacute;n <b>Confirmar</b>.<br />
                                Si desea cancelar, presione el bot&oacute;n <b>Cancelar</b>.
                            </p>
                        </div>
                        <div class="col-3">
                            <div class="text-center position-relative">
                                <a href="" data-toggle="modal" data-target="#modal-vista_previa_<?= $Elemento->getId(); ?>">
                                    <img class="img-tabla" src="../<?= $Elemento->getFotoRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <h4>Propiedades</h4>
                    <input type="hidden" id="elemento_id" name="elemento_id" value="<?=$Elemento->getId();?>">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="nombre">Imagen del marcador <span class="font-italic text-muted">- en formato PNG</span>:</label>
                                <input type="file" class="form-control" id="imagen_ruta" name="imagen_ruta" accept="image/png" required="">
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="nombre">Patrón del marcador <span class="font-italic text-muted">- en formato PATT</span>:</label>
                                <input type="file" class="form-control" id="patron_ruta" name="patron_ruta" accept=".patt" required="">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="row justify-content-between">
                        <a href="marcador.crear.php">
                            <button type="button" class="btn btn-outline-danger">
                                <i class="fas fa-xmark fa-fw mr-1"></i> Cancelar
                            </button>
                        </a>
                        <button type="submit" class="btn btn-outline-success">
                            <i class="fas fa-check fa-fw mr-1"></i> Confirmar
                        </button>
                    </div>
                </div>
            </form>
            <?php }
            else { ?>
            <div class="alert alert-warning">Ups! No ha seleccionado ningún elemento. Los marcadores sólo pueden crearse si se relacionan a un elemento de la colección.</div>
            <?php }
            ?>
        </div>

        <!-- MODAL RESPUESTA -->
        <div class="modal fade" id="modal-respuesta" tabindex="-1" aria-labelledby="" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body text-center">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <div id="modal-contenido">
                            <span id="modal-icon" class="fas fa-check fa-fw my-4 text-success" style="font-size:6rem;"></span>
                            <h5 id="modal-titulo">Creación de Elemento</h5>
                            <h3 id="modal-subtitulo" class="my-2 text-uppercase"> transacción 123</h3>

                            <div id="modal-btn_cerrar" class="row justify-content-around my-3">
                                <div class="col-6">
                                    <a href="marcadores.php?id=<?=$Elemento->getId();?>">
                                        <button type="button" class="btn btn-outline-success">Cerrar</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <?php include_once '../gui/footer.php'; ?>
    <script type="application/javascript" src="../lib/js/funciones_ajax/marcador.crear.js"></script>
</body>

</html>
