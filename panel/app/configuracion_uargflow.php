<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_USUARIOS);
include_once '../modelo/ColeccionUsuarios.php';
$ColeccionUsuarios = new ColeccionUsuarios();
?>

<html>

<head>
    <?php include_once('../lib/headers.php'); ?>

    <title><?= Constantes::NOMBRE_SISTEMA; ?> - Configuración de UARG-Flow</title>
</head>

<body class="sticky-footer">

    <?php include_once '../gui/navbar.php'; ?>

    <div class="container">

        <div class="card">
            <div class="card-header">
                <h3>Configuración de UARG-Flow</h3>
            </div>
            <div class="card-body p-3">
                <div class="row">
                    <div class="col-md-4">
                        <a href="../app/usuarios.php">
                            <button class="btn btn-outline-success btn-block p-4">
                                <i class="bi-people-fill mx-2" style="font-size: 3em;"></i>
                                <br>
                                Usuarios
                            </button>
                        </a>
                        <span class="text-success small">
                            Podés visualizar, crear, modificar y eliminar los usuarios del sistema.
                        </span>
                    </div>
                    <?php if (ControlAcceso::verificaPermiso(PermisosSistema::PERMISO_ROLES)) { ?>
                    <div class="col-md-4">
                        <a href="../app/roles.php">
                            <button class="btn btn-outline-info btn-block p-4">
                                <i class="bi-palette2 mx-2" style="font-size: 3em;"></i>
                                <br>
                                Roles
                            </button>
                        </a>
                        <span class="text-info small">
                            Podés visualizar, crear, modificar y eliminar los diferentes roles en el sistema.
                        </span>
                    </div>
                    <?php } ?>
                    <?php if (ControlAcceso::verificaPermiso(PermisosSistema::PERMISO_PERMISOS)) { ?>
                    <div class="col-md-4">
                        <a href="../app/permisos.php">
                            <button class="btn btn-outline-warning btn-block p-4">
                                <i class="bi-sliders mx-2" style="font-size: 3em;"></i>
                                <br>
                                Permisos
                            </button>
                        </a>
                        <span class="text-warning small">
                            Podés visualizar, crear, modificar y eliminar los permisos para cada rol en el sistema.
                        </span>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <?php include_once '../gui/footer.php'; ?>
</body>

</html>
