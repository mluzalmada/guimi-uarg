<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_PERMISOS);
include_once '../modelo/Permiso.Class.php';

$Permiso = new Permiso($_GET["id"]);
?>


<html>
    <head>
        <?php include_once('../lib/headers.php'); ?>

       <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Propiedades del Permiso</title>

    </head>
    <body class="sticky-footer">
        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <p></p>
            <div class="card">
                <div class="card-header">
                    <h3>Propiedades del Permiso</h3>
                </div>
                <div class="card-body">
                    <h4 class="card-text">Nombre</h4>
                    <p> <?= $Permiso->getNombre(); ?></p>
                    <hr />
                    <h5 class="card-text">Opciones</h5>
                     <a href="permisos.php">
                        <button type="button" class="btn btn-primary">
                            <i class="bi-arrow-left-circle-fill mx-2"></i>
                            Volver a lista de permisos
                        </button>
                    </a>
                </div>
            </div>
        </div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>
