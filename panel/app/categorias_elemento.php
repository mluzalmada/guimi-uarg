<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_PERMISOS);
include_once '../modelo/ColeccionCategoriasElemento.php';
$ColeccionCategoriasElemento = new ColeccionCategoriasElemento();
?>

<html>

<head>
    <meta charset="UTF-8">
    <?php include_once('../lib/headers.php'); ?>

    <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Categorías de Elemento</title>

</head>

<body class="sticky-footer">

    <?php include_once '../gui/navbar.php'; ?>

    <div class="container">
       <div class="alert alert-warning my-2"><span class="font-weight-bold text-uppercase">IMPORTANTE:</span> No podrás eliminar categorías de la colección que contengan elementos. </div>
       
        <div class="card">
            <div class="card-header">
                <h3>Categorías de Elemento</h3>
            </div>
            <div class="card-body">
                <p>
                    <a href="categoria_elemento.crear.php">
                        <button type="button" class="btn btn-success">
                            <i class="fas fa-plus fa-fw mr-1"></i> Nueva Categoría de Elemento
                        </button>
                    </a>
                </p>
                <table class="table table-hover table-sm">
                    <tr class="table-info">
                        <th>Nombre</th>
                        <th>Opciones</th>
                    </tr>
                    <tr>
                        <?php foreach ($ColeccionCategoriasElemento->getCategoriasElemento() as $CategoriaElemento) { ?>
                        <td><?= $CategoriaElemento->getNombre(); ?></td>
                        <td>
<!--
                            <a title="Ver detalle" href="categoria_elemento.ver.php?id=<?= $CategoriaElemento->getId(); ?>">
                                <button type="button" class="btn btn-outline-info btn-sm w-100 mb-1">
                                    <i class="fas fa-magnifying-glass-plus fa-fw"></i>
                                    Ver detalle
                                </button>
                            </a>
-->
                            <a title="Modificar" href="categoria_elemento.modificar.php?id=<?= $CategoriaElemento->getId(); ?>">
                                <button type="button" class="btn btn-outline-warning btn-sm w-100 mb-1">
                                    <i class="fas fa-pen-to-square fa-fw"></i>
                                    Editar
                                </button>
                            </a>

                            <button type="button" class="btn btn-outline-danger btn-sm w-100 mb-1" data-toggle="modal" data-target="#modal-categoria_elemento_eliminar_<?= $CategoriaElemento->getId(); ?>">
                                <i class="fas fa-trash-can fa-fw"></i>
                                <span class="ml-1">Eliminar</span>
                            </button><br>

                        </td>
                    </tr>

                    <!-- MODAL ELIMINAR -->
                    <div class="modal fade" id="modal-categoria_elemento_eliminar_<?= $CategoriaElemento->getId(); ?>" tabindex="-1" aria-labelledby="categoria_elemento_eliminar" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body text-center">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div id="modal-contenido_<?= $CategoriaElemento->getId(); ?>">
                                        <span id="modal-icon_<?= $CategoriaElemento->getId(); ?>" class="fas fa-trash-can fa-fw my-4 text-danger" style="font-size:6rem;"></span>
                                        <h5 id="modal-titulo_<?= $CategoriaElemento->getId(); ?>">¿Estás seguro/a de eliminar?</h5>
                                        <h3 id="modal-subtitulo_<?= $CategoriaElemento->getId(); ?>" class="my-2 text-uppercase"> <?= $CategoriaElemento->getNombre(); ?></h3>
                                        <div id="modal-form_<?= $CategoriaElemento->getId(); ?>">
                                            <form data-id="<?= $CategoriaElemento->getId(); ?>">
                                                <p class="alert alert-danger my-3">
                                                    <i class="fas fa-triangle-exclamation fa-fw mr-1"></i> ATENCI&Oacute;N: Esta operaci&oacute;n no puede deshacerse.
                                                </p>

                                                <input type="hidden" name="id" class="form-control" id="id" value="<?= $CategoriaElemento->getId(); ?>">
                                                <div class="row justify-content-around">
                                                    <div class="col-6">
                                                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><span class="fas fa-xmark fa-fw mr-1"></span> Cancelar</button>
                                                    </div>
                                                    <div class="col-6">
                                                        <button type="submit" class="btn btn-outline-success">
                                                            <span class="fas fa-check fa-fw"></span> Sí, deseo eliminar
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div id="modal-btn_cerrar_<?= $CategoriaElemento->getId(); ?>" class="row justify-content-around my-3" style="display:none;">
                                            <div class="col-6">
                                                <a href="categorias_elemento.php">
                                                    <button type="button" class="btn btn-outline-success">Cerrar</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php } ?>
                </table>
            </div>
        </div>
    </div>

    <?php include_once '../gui/footer.php'; ?>
    <script type="application/javascript" src="../lib/js/funciones_ajax/categoria_elemento.eliminar.js"></script>

</body>

</html>
