<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_PERMISOS);
?>

<html>

<head>
    <?php include_once('../lib/headers.php'); ?>

    <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Crear Tipo de Elemento</title>
</head>

<body class="sticky-footer">
    <?php include_once '../gui/navbar.php'; ?>
    <div class="container">
        <form action="tipo_elemento.crear.procesar.php" method="post">
            <div class="card">
                <div class="card-header">
                    <h3>Crear Tipo de Elemento</h3>
                    <p>
                        Complete los campos a continuaci&oacute;n.
                        Luego, presione el bot&oacute;n <b>Confirmar</b>.<br />
                        Si desea cancelar, presione el bot&oacute;n <b>Cancelar</b>.
                    </p>
                </div>
                <div class="card-body">
                    <h4>Propiedades</h4>

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Ingrese el nombre del tipo de elemento" required="">
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="descripcion">Descripción</label>
                                <textarea class="form-control" rows="3" id="descripcion" name="descripcion" placeholder="Agregue una descripción del tipo de elemento"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row justify-content-between">
                            <a href="tipos_elemento.php">
                                <button type="button" class="btn btn-outline-danger">
                                    <i class="fas fa-xmark fa-fw mr-1"></i> Cancelar
                                </button>
                            </a>
                            <button type="submit" class="btn btn-outline-success">
                                <i class="fas fa-check fa-fw mr-1"></i> Confirmar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
    <?php include_once '../gui/footer.php'; ?>
</body>

</html>
