<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_USUARIOS);
include_once '../modelo/ColeccionUsuarios.php';
$ColeccionUsuarios = new ColeccionUsuarios();
?>

<html>
    <head>
        <?php include_once('../lib/headers.php'); ?>

        <title><?= Constantes::NOMBRE_SISTEMA; ?> - Usuarios</title>
    </head>
    <body class="sticky-footer">

        <?php include_once '../gui/navbar.php'; ?>

        <div class="container">

            <div class="card">
                <div class="card-header">
                    <h3>Usuarios</h3>
                </div>
                <div class="card-body">
                    <p>
                        <a href="usuario.crear.php">
                        <button type="button" class="btn btn-success">
                            <i class="fas fa-plus fa-fw mr-1"></i> Nuevo Usuario
                        </button>
                    </a>
                    </p>
                    <table class="table table-hover table-sm">
                        <tr class="table-info">
                            <th>Usuario</th>
                            <th>Opciones</th>
                        </tr>
                        <tr>
                            <?php foreach ($ColeccionUsuarios->getUsuarios() as $Usuario) {
                                ?>
                                <td><?= $Usuario->getNombre(); ?><br /><?= $Usuario->getEmail(); ?></td>
                                <td>
                                    <a title="Ver detalle" href="usuario.ver.php?id=<?= $Usuario->getId(); ?>">
                                        <button type="button" class="btn btn-outline-info">
                                            <i class="fas fa-magnifying-glass-plus fa-fw"></i>
                                        </button></a>
                                    <a title="Modificar" href="usuario.modificar.php?id=<?= $Usuario->getId(); ?>">
                                        <button type="button" class="btn btn-outline-warning">
                                            <i class="fas fa-pen-to-square fa-fw"></i>
                                        </button></a>
                                    <a title="Eliminar" href="usuario.eliminar.php?id=<?= $Usuario->getId(); ?>">
                                        <button type="button" class="btn btn-outline-danger">
                                            <span class="fas fa-trash-can fa-fw"></span>
                                        </button></a>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>

