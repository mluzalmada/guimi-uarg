<?php include_once '../lib/ControlAcceso.Class.php'; ?>

<html>
    <head>
        <meta charset="UTF-8">
        <?php include_once('../lib/headers.php'); ?>

<!--       Desarrollo: -->
       <meta name="google-signin-client_id" content="356408280239-7airslbg59lt2nped9l4dtqm2rf25aii.apps.googleusercontent.com" />
<!--       Producción:-->
<!--        <meta name="google-signin-client_id" content="440436914034-0peaq7i234lao9ojqn5i0cjahoca41hc.apps.googleusercontent.com" />-->
        <script type="text/javascript" src="https://apis.google.com/js/platform.js" async defer></script>
        <script type="text/javascript" src="../lib/login.js"></script>
        <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Login</title>
    </head>
    <body class="sticky-footer mb-0">

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container navbar-dark bg-dark">
                <a class="navbar-brand" href="menu_principal.php">
                    <img src="../lib/img/Logo-GUIMI-blanco.png" height="30" class="d-inline-block align-top" alt="GuIMI">
                </a>
            </div>
        </nav>

        <div class="container">
            <section id="main-content">
                <article>
                    <div class="card" style="margin:2em;">
                        <div class="card-header">
                            <h3> <?php echo Constantes::NOMBRE_SISTEMA; ?> - Login</h3>
                        </div>
                        <div class="card-body">

                            <h5>¡Hola!</h5>
                            <p>Le damos la bienvenida a la aplicaci&oacute;n <?php echo Constantes::NOMBRE_SISTEMA; ?>, una aplicaci&oacute;n desarrollada en la UARG - UNPA.</p>

                            <div class="row">
                                <div class="col-12">
                                    <div class="alert alert-danger" role="alert">
                                        <div class="row vertical-align">
                                            <div class="col-1 text-center">
                                                <i class="fas fa-circle-info fa-fw"></i> 
                                            </div>
                                            <div class="col-11">
                                                <strong>Importante:</strong> Para acceder al sistema es necesario disponer de un correo de <a href="http://www.gmail.com" target="_blank">GMail</a>.
                                            </div>
                                        </div>
                                    </div>      
                                </div>
                            </div>                              


                            <hr />
                            <h5>Ingreso al Sistema</h5>
                            <p>Podés ingresar el sistema si est&aacute;s conectado a tu e-mail. Por favor hacé click en el bot&oacute;n a continuaci&oacute;n y elegí tu cuenta o realizá el login.</p>
                            <div id="okgoogle" class="g-signin2" data-onsuccess="onSignIn" title="Acceder a GuIMI Admin"></div>
                        </div>
                    </div>
                </article>
            </section>
        </div>
        <?php include_once('../gui/footer.php'); ?>    
        </body>
</html>

