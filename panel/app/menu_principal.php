<?php
include_once '../lib/ControlAcceso.Class.php';
include_once '../modelo/ColeccionElementos.php';
$ColeccionElementos = new ColeccionElementos();
?>

<html>

<head>
    <meta charset="UTF-8">
    <?php include_once('../lib/headers.php'); ?>

    <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Menú principal</title>

</head>

<body class="sticky-footer">

    <?php include_once '../gui/navbar.php'; ?>

    <div class="container mt-4">
        <div class="card">
            <div class="card-header">
                <h3>Menú principal</h3>
            </div>
            <div class="card-body p-3">
                <?php if (!ControlAcceso::verificaPermiso(PermisosSistema::PERMISO_COLECCION)) { ?>
                <div class="alert alert-warning">
                    <h5>¡Hola!</h5>
                    <p>Lamentamos informarte que por el momento, esta sección del sistema GuIMI sólo está disponible para personal autorizado del Museo de Informática.</p>
                    <p>Si deseas ingresar con otra cuenta, <a href="https://accounts.google.com/Logout" target="_blank">cierra la sesión en Google</a> e intenta volver a ingresar.</p>
                    <p>Si crees que esto es un error, por favor comunicate con el administrador del sistema y solicitale que brinde los accesos necesarios para tu cuenta.</p>
                    <hr>
                    <span>Podés utilizar la aplicación de realidad aumentada en tus visitas <a href="#" class="btn btn-warning ml-2">Ir a la App de realidad aumentada</a>
                    </span>
                </div>
                <?PHP } ?>
                <div class="row">
                    <?php if (ControlAcceso::verificaPermiso(PermisosSistema::PERMISO_COLECCION)) { ?>
                    <div class="col-md-4 p-4">
                        <a href="../app/elementos.php">
                            <button class="btn btn-outline-primary btn-block p-4">
                                <i class="bi-box mx-2" style="font-size: 3em;"></i>
                                <br>
                                Elementos
                            </button>
                        </a>
                        <span class="text-primary small">
                            Podés visualizar, crear, modificar y eliminar los elementos de la Colección.
                        </span>
                    </div>
                    <?php }
                    if (ControlAcceso::verificaPermiso(PermisosSistema::PERMISO_COLECCION)) { ?>
                    <div class="col-md-4 p-4">
                        <a href="../app/contenidos.php">
                            <button class="btn btn-outline-warning btn-block p-4">
                                <i class="fa fa-photo-film fa-fw mx-2" style="font-size: 3em;"></i>
                                <br>
                                Contenidos
                            </button>
                        </a>
                        <span class="text-warning small">
                            Podés visualizar los contenidos de los elementos de toda la colección.
                        </span>
                    </div>
                    <?php } 
                    if (ControlAcceso::verificaPermiso(PermisosSistema::PERMISO_COLECCION)) { ?>
                    <div class="col-md-4 p-4">
                        <a href="../app/marcadores.php">
                            <button class="btn btn-outline-success btn-block p-4">
                                <i class="bi-qr-code-scan mx-2" style="font-size: 3em;"></i>
                                <br>
                                Marcadores
                            </button>
                        </a>
                        <span class="text-success small">
                            Podés visualizar, crear o imprimir los marcadores de los elementos.
                        </span>
                    </div>
                    <?php } ?>
                    <?php if (ControlAcceso::verificaPermiso(PermisosSistema::PERMISO_COLECCION)) { ?>
                    <div class="col-md-4 p-4">
                        <a href="../app/novedades.php">
                            <button class="btn btn-outline-info btn-block p-4">
                                <i class="bi-newspaper mx-2" style="font-size: 3em;"></i>
                                <br>
                                Novedades
                            </button>
                        </a>
                        <span class="text-info small">
                            Podés visualizar, crear, modificar y eliminar las novedades acerca del Museo.
                        </span>
                    </div>
                    <?php }
                    if (ControlAcceso::verificaPermiso(PermisosSistema::PERMISO_PERMISOS)) { ?>
                    <div class="col-md-4 p-4">
                        <a href="../app/configuracion_uargflow.php">
                            <button class="btn btn-outline-dark btn-block p-4">
                                <i class="bi-tools mx-2" style="font-size: 3em;"></i>
                                <br>
                                Configuración UARG-Flow
                            </button>
                        </a>
                        <span class="text-dark small">
                            Podés visualizar, crear, modificar y eliminar los usuarios, roles y permisos relacionados con el framework UARG-Flow.
                        </span>
                    </div>
                    <?php }
                    if (ControlAcceso::verificaPermiso(PermisosSistema::PERMISO_PERMISOS)) { ?>
                    <div class="col-md-4 p-4">
                        <a href="../app/configuracion_guimi.php">
                            <button class="btn btn-outline-secondary btn-block p-4">
                                <i class="fa-solid fa-palette" style="font-size: 3em;"></i>
                                <br>
                                Configuración GuIMI
                            </button>
                        </a>
                        <span class="text-dark small">
                            Podés visualizar, crear, modificar y eliminar los usuarios, roles y permisos relacionados con el framework UARG-Flow.
                        </span>
                    </div>
                    <?php }
                    if (ControlAcceso::verificaPermiso(PermisosSistema::PERMISO_SALIR)) { ?>
                    <div class="col-md-4 p-4">
                        <a href="../app/salir.php">
                            <button class="btn btn-outline-danger btn-block p-4">
                                <i class="fas fa-door-open fa-fw mx-2" style="font-size: 3em;"></i>
                                <br>
                                Salir
                            </button>
                        </a>
                        <span class="text-danger small">
                            Cerrá sesión para salir del sistema cuando termines de usar el panel de administración de GuIMI.
                        </span>
                    </div>
                    <?php } ?>
                </div>
            </div>

        </div>
    </div>
    <?php include_once '../gui/footer.php'; ?>
</body>

</html>
