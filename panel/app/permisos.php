<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_PERMISOS);
include_once '../modelo/ColeccionPermisos.php';
$ColeccionPermisos = new ColeccionPermisos();
?>

<html>
    <head>
        <?php include_once('../lib/headers.php'); ?>

        <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Permisos</title>

    </head>
    <body class="sticky-footer">

        <?php include_once '../gui/navbar.php'; ?>

        <div class="container">
            <div class="card">
                <div class="card-header">

                    <h3>Permisos</h3>
                </div>
                <div class="card-body">
                    <p>
                        <a href="permiso.crear.php">
                            <button type="button" class="btn btn-success">
                                <i class="fas fa-plus fa-fw mr-1"></i> Nuevo Permiso
                            </button>
                        </a>
                    </p>
                    <table class="table table-hover table-sm">
                        <tr class="table-info">
                            <th>Nombre</th>
                            <th>Opciones</th>
                        </tr>
                        <tr>
                            <?php foreach ($ColeccionPermisos->getPermisos() as $Permiso) { ?>
                                <td><?= $Permiso->getNombre(); ?></td>
                                <td>
                                    <a title="Ver detalle" href="permiso.ver.php?id=<?= $Permiso->getId(); ?>">
                                        <button type="button" class="btn btn-outline-info">
                                            <i class="fas fa-magnifying-glass-plus fa-fw"></i>
                                        </button>
                                    </a>
                                    <a title="Modificar" href="permiso.modificar.php?id=<?= $Permiso->getId(); ?>">
                                        <button type="button" class="btn btn-outline-warning">
                                            <i class="fas fa-pen-to-square fa-fw"></i>
                                        </button>
                                    </a>
                                    <a title="Eliminar" href="permiso.eliminar.php?id=<?= $Permiso->getId(); ?>">
                                        <button type="button" class="btn btn-outline-danger">
                                            <span class="fas fa-trash-can fa-fw"></span>
                                        </button>
                                    </a>  
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>

