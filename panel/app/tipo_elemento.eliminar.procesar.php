<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_COLECCION);
include_once '../modelo/BDConexion.Class.php';
include_once '../modelo/TipoElemento.Class.php';

$respuesta = array();
$respuesta['eliminado'] = false;

if(isset($_POST)) {
    $DatosFormulario = $_POST;
    $tipo_elemento_id = $DatosFormulario["id"];
    $TipoElemento = new TipoElemento($tipo_elemento_id);
    
    if($TipoElemento->deleteTipoElemento()) {
        $respuesta['eliminado'] = true; 
    }
}
echo json_encode($respuesta);
?>
