<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_PERMISOS);
include_once '../modelo/Elemento.Class.php';
include_once '../modelo/ColeccionContenidos.php';
include_once '../modelo/ColeccionTiposContenido.php';
include_once '../modelo/Archivo.Class.php';

$id = $_GET["id"];
$TipoContenido = new ColeccionTiposContenido();

$Contenido = new Contenido($id);
$elemento_id = $Contenido->getElementoId();
$Elemento = new Elemento($elemento_id);

$Archivo = new Archivo();
$Archivo->getArchivoPorContenidoId($id);
?>
<html>

<head>
    <?php include_once('../lib/headers.php'); ?>
    <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Actualizar Contenido</title>
</head>

<body class="sticky-footer">
    <?php include_once '../gui/navbar.php'; ?>
    <div class="container">
        <div class="row justify-content-between mb-3">
            <div class="col-6">
                <a href="elementos.php">
                    <button type="button" class="btn btn-primary">
                        <span class="fas fa-arrow-left fa-fw"></span> Volver a la colección
                    </button>
                </a>
            </div>
        </div>

        <form id="form-editar_contenido" enctype="multipart/form-data">
            <div class="card">
                <div class="card-header">
                    <div class="row justify-content-between">
                        <div class="col-9">
                            <h3>Actualizar contenido del elemento <span class="text-uppercase"><?= $Elemento->getNombre(); ?></span></h3>
                            <p>
                                Complete los campos a continuaci&oacute;n.
                                Luego, presione el bot&oacute;n <b>Confirmar</b>.<br />
                                Si desea cancelar, presione el bot&oacute;n <b>Cancelar</b>.
                            </p>
                        </div>
                        <div class="col-3">
                            <div class="text-center position-relative">
                                <a href="" data-toggle="modal" data-target="#modal-vista_previa">
                                    <img class="img-tabla" src="../<?= $Elemento->getFotoRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input type="text" name="nombre" class="form-control" id="nombre" value="<?= $Contenido->getNombre(); ?>" placeholder="Ingrese el nombre del contenido" required="">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="tipo_contenido_id">Tipo</label>
                                <select class="form-control" name="tipo_contenido_id" id="tipo_contenido_id">
                                    <?php 
                               foreach ($TipoContenido->getTiposContenido() as $Tipo) { ?>
                                    <option value="<?= $Tipo->getId(); ?>" id="tipo_<?= $Tipo->getId(); ?>" <?php
                                            if($Contenido->getTipoContenidoId() == $Tipo->getId()) {
                                                $tipo_contenido = $Tipo->getNombre();                                               
                                                echo " selected";
                                            }
                                            ?>>
                                        <?= $Tipo->getNombre(); ?>
                                    </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="descripcion">Descripción <span class="text-muted font-italic">(Opcional)</span>:</label>
                                <textarea rows="3" class="form-control" name="descripcion" id="descripcion" placeholder="Ingrese la descripción del Elemento"><?= $Contenido->getDescripcion(); ?></textarea>
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="form-group">
                                <label for="duracion_tiempo">Duración <span class="text-muted font-italic">(Opcional)</span>:</label>
                                <input type="tel" name="duracion_tiempo" class="form-control" id="duracion_tiempo" placeholder="Ingrese la duración del contenido (si corresponde)" value="<?= $Contenido->getDuracionTiempo();?>">
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="form-group">
                                <label for="dimension_pixeles">Dimensión <span class="text-muted font-italic">(Opcional)</span>:</label>
                                <input type="tel" name="dimension_pixeles" class="form-control" id="dimension_pixeles" placeholder="Ingrese la dimensión en píxeles (si corresponde)" value="<?=$Contenido->getDimensionPixeles();?>">
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="form-group">
                                <label for="extension_caracteres">Extensión del texto (en caracteres) <span class="text-muted font-italic">(Opcional)</span>:</label>
                                <input type="tel" name="extension_caracteres" class="form-control" id="extension_caracteres" placeholder="Ingrese la extensión de caracteres (si corresponde)" value="<?=$Contenido->getExtensionCaracteres();?>">
                            </div>
                        </div>

                        <div class="col-md-4 col-12">
                            <div class="form-group">
                                <label for="visible">Visible en la App</label>
                                <div class="form-check my-1 mr-sm-2">
                                    <input type="radio" name="visible" class="check-input" id="visible_1" value="1" <?php
                                           if($Elemento->getVisible() == 1) {
                                               echo " checked";
                                           }
                                           ?>>
                                    <label class="mr-4" for="visible_1">Si</label>
                                    <input type="radio" name="visible" class="check-input" id="visible_0" value="0" <?php
                                           if($Elemento->getVisible() == 0) {
                                               echo " checked";
                                           }
                                           ?>>
                                    <label class="mr-4" for="visible_0">No</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <!--                        VISTA PREVIA DEL CONTENIDO ACTUAL-->
                        <?php 
                         switch($Contenido->getTipoContenidoId()) {
                             case '1': { ?>
                        <!--                            CONTENIDO IMAGEN-->
                        <div class="col-md-6 col-12 text-center div-imagen" id="div-imagen">
                            <img class="img-preview img-rounded" src="<?php echo "../".$Archivo->getRuta().$Archivo->getNombre().".".$Archivo->getExtension(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'">

                            <div class="div-overlay">
                                <a href="" data-toggle="modal" data-target="#modal-vista_previa_contenido">
                                    <p>Hacé clic para ampliar</p>
                                </a>
                            </div>
                        </div>
                        <?php break;
                             }
                             case '2': { ?>
                        <!--CONTENIDO TEXTO                            -->
                        <div class="col-md-6 col-12 text-center" id="div-texto">
                            <a href="" data-toggle="modal" data-target="#modal-vista_previa_contenido_texto">
                                <button class="btn btn-sm btn-outline-primary btn-block">Ver contenido del texto</button>
                            </a>
                        </div>
                        <?php break;
                             }
                             case '3': { ?>
                        <!--CONTENIDO AUDIO                            -->
                        <div class="col-md-6 col-12 text-center" id="div-audio">
                            <audio src="<?php echo "..".$Archivo->getRuta().$Archivo->getNombre().".".$Archivo->getExtension(); ?>" preload="none" controls></audio>
                        </div>
                        <?php break;
                             }
                             case '4': { ?>
                        <!--CONTENIDO VIDEO                            -->
                        <div class="col-md-6 col-12 text-center" id="div-video">
                            <video src="<?php echo "..".$Archivo->getRuta().$Archivo->getNombre().".".$Archivo->getExtension(); ?>" preload="none" controls></video>
                        </div>

                        <?php break;
                             }
                         }
                        ?>

                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="archivo">Reemplazar archivo:</label>
                                <input type="file" name="archivo" class="form-control" id="archivo" value="<?php echo $Archivo->getRuta().$Archivo->getNombre().".".$Archivo->getExtension(); ?>">
                            </div>
                        </div>

                        <div class="col-12">
                            <input type="hidden" name="id" class="form-control" id="id" value="<?= $Contenido->getId(); ?>">
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="descripcion">Fuente o atribución por el contenido <span class="text-muted font-italic">(Opcional)</span>:</label>
                                <input type="text" class="form-control" name="fuente_atribucion" id="fuente_atribucion" placeholder="Indica el link de atribución o nombre de autor/propietario del contenido" value="<?= $Contenido->getFuenteAtribucion();?>">
                            </div>
                        </div>

                    </div>
                </div>

                <div class="card-footer">
                    <div class="row justify-content-around text-center">
                        <div class="col-6">
                            <a href="elementos.php">
                                <button type="button" class="btn btn-outline-danger">
                                    <i class="fas fa-xmark fa-fw mr-1"></i> Cancelar
                                </button>
                            </a>
                        </div>
                        <div class="col-6">
                            <button type="submit" class="btn btn-outline-success">
                                <i class="fas fa-check fa-fw mr-1"></i> Confirmar
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade" tabindex="-1" aria-hidden="true" id="modal-vista_previa">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Foto del elemento: <?= $Elemento->getNombre(); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img src="../<?= $Elemento->getFotoRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'" width="100%">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" aria-hidden="true" id="modal-vista_previa_contenido">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-uppercase small">Contenido tipo <?= $tipo_contenido; ?> de <?=$Elemento->getNombre();?> (archivo <?= $Archivo->getNombre(); ?>)</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img src="<?php echo "../".$Archivo->getRuta().$Archivo->getNombre().".".$Archivo->getExtension(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'" width="100%">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL RESPUESTA -->
    <div class="modal fade" id="modal-respuesta" tabindex="-1" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div id="modal-contenido">
                        <span id="modal-icon" class="fas fa-check fa-fw my-4 text-success" style="font-size:6rem;"></span>
                        <h5 id="modal-titulo">Actualización de <?= $Elemento->getNombre(); ?></h5>
                        <h3 id="modal-subtitulo" class="my-2 text-uppercase"></h3>

                        <div id="modal-btn_cerrar" class="row justify-content-around my-3">
                            <div class="col-6">
                                <a href="contenidos.php?id=<?=$elemento_id;?>">
                                    <button type="button" class="btn btn-outline-success">Cerrar</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include_once '../gui/footer.php'; ?>
    <script type="application/javascript" src="../lib/js/funciones_ajax/contenido.modificar.js"></script>
    <script type="application/javascript" src="../lib/js/funciones.js"></script>
</body>

</html>
