<?php
$ColeccionMarcadores = new ColeccionMarcadores();
$ColeccionElementos = new ColeccionElementos();
?>

<html>

<head>
    <meta charset="UTF-8">
    <?php include_once('../lib/headers.php'); ?>

    <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Marcadores</title>

</head>

<body class="sticky-footer">

    <?php include_once '../gui/navbar.php'; ?>

    <div class="container">
        <div class="card">
            <div class="card-header">
                <h3>Marcadores</h3>
            </div>
            <div class="card-body">
                <p>
                    <a href="../app/marcador.seleccionar.elemento.php">
                        <button type="button" class="btn btn-success">
                            <i class="fas fa-plus fa-fw mr-1"></i> Agregar marcador
                        </button>
                    </a>
                </p>
                <?php if($ColeccionMarcadores->getMarcadores()) { ?>
                <table class="table table-hover table-sm">
                    <tr class="table-info">
                        <th>Id</th>
                        <th>Imagen</th>
                        <th>Elemento</th>
                        <th>Opciones</th>
                    </tr>
                    <tr>
                        <?php
                               foreach ($ColeccionMarcadores->getMarcadores() as $Marcador) { ?>
                        <td><?= $Marcador->getId(); ?></td>
                        <td>
                            <div class="text-center position-relative">
                                <a href="" data-toggle="modal" data-target="#modal-vista_previa_<?= $Marcador->getId(); ?>">
                                    <img class="img-tabla w-auto" src="../<?= $Marcador->getImagenRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'">
                                </a>
                            </div>
                        </td>
                        <td>
                            <?php
$Elemento = new Elemento($Marcador->getElementoId()); 
if($Elemento->getExiste()) {
    echo $Elemento->getNombre();
}
?>
                        </td>
                        <td class="text-right">
                            <!--
                                    <a title="Ver detalle" href="marcador.ver.php?id=<?= $Marcador->getId(); ?>">
                                        <button type="button" class="btn btn-outline-info">
                                            <i class="fas fa-magnifying-glass-plus fa-fw"></i>
                                        </button>
                                    </a>
-->
                            <a title="Descargar" href="../<?= $Marcador->getImagenRuta(); ?>" download="GuIMI-elemento_<?=$Elemento->getId();?>[<?=$Elemento->getNombre();?>]-marcador_<?= $Marcador->getId(); ?>">
                                <button type="button" class="btn btn-outline-info  btn-sm w-50 mb-1">
                                    <span class="fas fa-download fa-fw"></span>
                                     <span class="ml-1">Descargar</span>
                                    
                                </button>
                            </a>
                            <a title="Modificar" href="marcador.modificar.php?id=<?= $Marcador->getId(); ?>">
                                <button type="button" class="btn btn-outline-warning  btn-sm w-50 mb-1">
                                    <i class="fas fa-pen-to-square fa-fw"></i>
                                     <span class="ml-1">Modificar</span>
                                    
                                </button>
                            </a>
                            <button type="button" class="btn btn-outline-danger btn-sm w-50 mb-1" data-toggle="modal" data-target="#modal-marcador_eliminar_<?= $Marcador->getId(); ?>">
                                <i class="fas fa-trash-can fa-fw"></i>
                                <span class="ml-1">Eliminar</span>
                            </button><br>
                        </td>
                    </tr>
                    
   <!-- MODAL ELIMINAR -->
                    <div class="modal fade" id="modal-marcador_eliminar_<?= $Marcador->getId(); ?>" tabindex="-1" aria-labelledby="marcador_eliminar" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body text-center">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div id="modal-contenido_<?= $Marcador->getId(); ?>">
                                        <span id="modal-icon_<?= $Marcador->getId(); ?>" class="fas fa-trash-can fa-fw my-4 text-danger" style="font-size:6rem;"></span>
                                        <h5 id="modal-titulo_<?= $Marcador->getId(); ?>">¿Estás seguro/a de eliminar?</h5>
                                        <h3 id="modal-subtitulo_<?= $Marcador->getId(); ?>" class="my-2 text-uppercase">Marcador <?= $Marcador->getId(); ?></h3>
                                        <div id="modal-form_<?= $Marcador->getId(); ?>">
                                            <form data-id="<?= $Marcador->getId(); ?>">
                                                <p class="alert alert-danger my-3">
                                                    <i class="fas fa-triangle-exclamation fa-fw mr-1"></i> ATENCI&Oacute;N: Esta operaci&oacute;n no puede deshacerse.
                                                </p>

                                                <input type="hidden" name="id" class="form-control" id="id" value="<?= $Marcador->getId(); ?>">
                                                <div class="row justify-content-around">
                                                    <div class="col-6">
                                                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><span class="fas fa-xmark fa-fw mr-1"></span> Cancelar</button>
                                                    </div>
                                                    <div class="col-6">
                                                        <button type="submit" class="btn btn-outline-success">
                                                            <span class="fas fa-check fa-fw"></span> Sí, deseo eliminar
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div id="modal-btn_cerrar_<?= $Marcador->getId(); ?>" class="row justify-content-around my-3" style="display:none;">
                                            <div class="col-6">
                                                <a href="marcadores.php">
                                                    <button type="button" class="btn btn-outline-success">Cerrar</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer text-uppercase text-muted small justify-content-center">
                                MARCADOR DEL ELEMENTO <?=$Elemento->getNombre();?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- FIN DEL MODAL-->
                    <?php } ?>
                </table>
                <?php }
                    else { ?>
                <div class="alert alert-warning">No hay marcadores en la colección. Hacé clic en el el botón <span class="font-weight-bold">Agregar Marcardor</span> para comenzar a crearlos. </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php include_once '../gui/footer.php'; ?>
    <script type="application/javascript" src="../lib/js/funciones_ajax/marcador.eliminar.js"></script>

</body>

</html>
