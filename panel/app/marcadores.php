<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_PERMISOS);
include_once '../modelo/ColeccionMarcadores.php';
include_once '../modelo/ColeccionElementos.php';

if(!isset($_GET["id"])) {
    include_once('marcadores.coleccion.php');
}
else {
    $id = $_GET["id"];
    $ColeccionMarcadores = new ColeccionMarcadores($id);
    $ColeccionElementos = new ColeccionElementos();
    $Elemento = new Elemento($id, null);

?>

<html>

<head>
    <meta charset="UTF-8">
    <?php include_once('../lib/headers.php'); ?>

    <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Marcadores del elemento <?=$Elemento->getNombre();?></title>

</head>

<body class="sticky-footer">

    <?php include_once '../gui/navbar.php'; ?>

    <div class="container">
        <div class="row justify-content-between mb-3">
            <div class="col-6">
                <a href="elementos.php">
                    <button type="button" class="btn btn-primary">
                        <span class="fas fa-arrow-left fa-fw"></span> Volver a la colección
                    </button>
                </a>
            </div>

            <div class="col-6 text-right">
                <?php if($Elemento->getExiste()) { ?>
                <a href="marcador.crear.php?id=<?=$Elemento->getId();?>">
                    <button type="button" class="btn btn-success">
                        <i class="fas fa-plus fa-fw mr-1"></i> Agregar marcador
                    </button>
                </a>
                <?php } ?>
            </div>
        </div>

        <div class="card">
            <?php 
            if($Elemento->getExiste()) {
                if(!$ColeccionMarcadores->getMarcadores()) { ?>

            <div class='alert alert-info'>La colección de marcadores de <span class='text-uppercase'><?=$Elemento->getNombre()?></span> se encuentra vacía! Agrega un marcador para verlo acá.</div>
            <?php }

                else { ?>
            <div class="card-header">                
                 <div class="row justify-content-between">
                    <div class="col-9">
                        <h3>Marcadores del elemento <span class='text-uppercase'><?=$Elemento->getNombre();?></span></h3>
                    </div>
                    <div class="col-3">
                        <div class="text-center position-relative">
                            <a href="" data-toggle="modal" data-target="#modal-vista_previa">
                                <img class="img-tabla" src="../<?= $Elemento->getFotoRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">

                <table class="table table-hover table-sm">
                    <tr class="table-info">
                        <th>Id</th>
                        <th>Imagen</th>
                        <th>Elemento</th>
                        <th>Opciones</th>
                    </tr>
                    <tr>
                        <?php foreach ($ColeccionMarcadores->getMarcadores() as $Marcador) { ?>
                        <td><?= $Marcador->getId(); ?></td>
                        <td>
                            <div class="text-center position-relative">
                                <a href="" data-toggle="modal" data-target="#modal-vista_previa_<?= $Marcador->getId(); ?>">
                                    <img class="img-tabla w-auto" src="../<?= $Marcador->getImagenRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'">
                                </a>
                            </div>
                        </td>
                        <td><?=$Elemento->getNombre();?></td>
                        <td class="text-right">
<!--
                            <a title="Ver detalle" href="marcador.ver.php?id=<?= $Marcador->getId(); ?>">
                                <button type="button" class="btn btn-outline-info">
                                    <i class="fas fa-magnifying-glass-plus fa-fw"></i>
                                </button>
                            </a>
-->
                            <a title="Descargar" href="../<?= $Marcador->getImagenRuta(); ?>" download="GuIMI-elemento_<?=$Elemento->getId();?>[<?=$Elemento->getNombre();?>]-marcador_<?= $Marcador->getId(); ?>">
                                <button type="button" class="btn btn-outline-info  btn-sm w-50 mb-1">
                                    <span class="fas fa-download fa-fw"></span>
                                     <span class="ml-1">Descargar</span>
                                    
                                </button>
                            </a>
                            <a title="Modificar" href="marcador.modificar.php?id=<?= $Marcador->getId(); ?>">
                                <button type="button" class="btn btn-outline-warning  btn-sm w-50 mb-1">
                                    <i class="fas fa-pen-to-square fa-fw"></i>
                                     <span class="ml-1">Modificar</span>
                                    
                                </button>
                            </a>
                            <button type="button" class="btn btn-outline-danger btn-sm w-50 mb-1" data-toggle="modal" data-target="#modal-marcador_eliminar_<?= $Marcador->getId(); ?>">
                                <i class="fas fa-trash-can fa-fw"></i>
                                <span class="ml-1">Eliminar</span>
                            </button><br>
                        </td>
                    </tr>


                    <!-- MODAL ELIMINAR -->
                    <div class="modal fade" id="modal-marcador_eliminar_<?= $Marcador->getId(); ?>" tabindex="-1" aria-labelledby="marcador_eliminar" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body text-center">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div id="modal-contenido_<?= $Marcador->getId(); ?>">
                                        <span id="modal-icon_<?= $Marcador->getId(); ?>" class="fas fa-trash-can fa-fw my-4 text-danger" style="font-size:6rem;"></span>
                                        <h5 id="modal-titulo_<?= $Marcador->getId(); ?>">¿Estás seguro/a de eliminar?</h5>
                                        <h3 id="modal-subtitulo_<?= $Marcador->getId(); ?>" class="my-2 text-uppercase">Marcador <?= $Marcador->getId(); ?></h3>
                                        <div id="modal-form_<?= $Marcador->getId(); ?>">
                                            <form data-id="<?= $Marcador->getId(); ?>">
                                                <p class="alert alert-danger my-3">
                                                    <i class="fas fa-triangle-exclamation fa-fw mr-1"></i> ATENCI&Oacute;N: Esta operaci&oacute;n no puede deshacerse.
                                                </p>

                                                <input type="hidden" name="id" class="form-control" id="id" value="<?= $Marcador->getId(); ?>">
                                                <div class="row justify-content-around">
                                                    <div class="col-6">
                                                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><span class="fas fa-xmark fa-fw mr-1"></span> Cancelar</button>
                                                    </div>
                                                    <div class="col-6">
                                                        <button type="submit" class="btn btn-outline-success">
                                                            <span class="fas fa-check fa-fw"></span> Sí, deseo eliminar
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div id="modal-btn_cerrar_<?= $Marcador->getId(); ?>" class="row justify-content-around my-3" style="display:none;">
                                            <div class="col-6">
                                                <a href="marcadores.php?id=<?= $Elemento->getId(); ?>">
                                                    <button type="button" class="btn btn-outline-success">Cerrar</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer text-uppercase text-muted small justify-content-center">
                                MARCADOR DEL ELEMENTO <?=$Elemento->getNombre();?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- FIN DEL MODAL-->

                    <!-- MODAL VER PATRON-->
                    <div class="modal fade" tabindex="-1" aria-hidden="true" id="modal-vista_previa_<?= $Marcador->getId(); ?>">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Imagen del marcador: <?= $Marcador->getId(); ?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <img src="../<?= $Marcador->getImagenRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'" width="100%">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php } ?>
                </table>
            </div>
                <?php }
            }
            
            else {
            
            }
            ?>
            </div>
        <!-- MODAL VER IMAGEN-->
        <div class="modal fade" tabindex="-1" aria-hidden="true" id="modal-vista_previa">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Foto del elemento: <?= $Elemento->getNombre(); ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img src="../<?= $Elemento->getFotoRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'" width="100%">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- MODAL Operación exitosa -->
        <div class="modal fade" id="modal-exito" tabindex="-1" aria-labelledby="" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body text-center">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="fas fa-trash-can fa-fw my-4 text-success" style="font-size:6rem;"></span>
                        <h5 class="my-3"> Operaci&oacute;n realizada con &eacute;xito.</h5>

                        <div class="row justify-content-center">
                            <div class="col-6">
                                <button type="button" class="btn btn-outline-primary" data-dismiss="modal"><span class="fas fa-check fa-fw mr-1"></span> Cerrar</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include_once '../gui/footer.php'; ?>
    <script type="application/javascript" src="../lib/js/funciones_ajax/marcador.eliminar.js"></script>

</body>

</html>

<?php } ?>