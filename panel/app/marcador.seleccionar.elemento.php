<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_PERMISOS);
include_once '../modelo/ColeccionElementos.php';
include_once '../modelo/ColeccionCategoriasElemento.php';

$ColeccionElementos = new ColeccionElementos();
$CategoriaElemento = new ColeccionCategoriasElemento();
?>

<html>

<head>
    <meta charset="UTF-8">
    <?php include_once('../lib/headers.php'); ?>

    <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Seleccionar elemento para crear marcador</title>

</head>

<body class="sticky-footer">

    <?php include_once '../gui/navbar.php'; ?>
    <div class="container">
        <div class="row justify-content-between mb-3">
            <div class="col-6">
                <a href="marcadores.php">
                    <button type="button" class="btn btn-primary">
                        <span class="fas fa-arrow-left fa-fw"></span> Volver a la colección de marcadores
                    </button>
                </a>
            </div>
        </div>

        <div class="card">
<div class="card-header">
    <h3>Seleccioná un elemento para crear su marcador</h3>
</div>
<div class="card-body">
    <div class="row">

        <?php foreach ($ColeccionElementos->getElementos() as $Elemento) { ?>
        <div class="col-xl-4 col-md-6 col-sm-12 mb-2">
            <label class="tarjeta tarjeta-radio-button">
                <a class="btn btn-light w-100" href="marcador.crear.php?id=<?=$Elemento->getId();?>">
                    <span class="tarjeta-header contenido-radio-button">
                        <h5><?=$Elemento->getNombre();?></h5>
                        <img class="img-btn my-3" src="../<?= $Elemento->getFotoRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'">

                        <div class="row justify-content-around">
                            <div class="col-7">
                                <div class="badge-detalle">
                                    <span class="font-weight-bold mr-1">Categoría:</span>
                                    <?php 
foreach ($CategoriaElemento->getCategoriasElemento() as $Categoria) {
    if($Elemento->getCategoria() == $Categoria->getId()) {
        echo $Categoria->getNombre();
    }
} ?>
                                </div>
                            </div>
                            <div class="col-5 align-self-center">
                                <div class="badge-detalle">
                                    <span class="font-weight-bold mr-1">Id:</span>
                                    <?=$Elemento->getId();?>
                                </div>
                            </div>
                        </div>
                    </span>
                </a>
            </label>
        </div>
        <?php } ?>

    </div>

</div>
