<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_PERMISOS);
include_once '../modelo/BDConexion.Class.php';
include_once '../modelo/Elemento.Class.php';
include_once '../lib/guimi/registrar_actualizaciones.php';

$respuesta = array();
$respuesta['eliminado'] = false;
if(isset($_POST)) {
    $DatosFormulario = $_POST;
    $elemento_id = $DatosFormulario["id"];
    $respuesta['datos'] = $elemento_id;
    $Elemento = new Elemento($elemento_id);
    // Actualizar la tabla actualizacion_elemento:
    $actualizacion_id = RegistrarActualizacionElemento('3',$elemento_id);
    if($actualizacion_id) {
        $respuesta['transaccion'] = $actualizacion_id;
    }
    if($Elemento->deleteElemento()) {
//        TODO: ELIMINAR LOS ARCHIVOS DEL SERVIDOR
        $respuesta['eliminado'] = true; 
    }
}
echo json_encode($respuesta);
?>
