<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_ROLES);
include_once '../modelo/ColeccionRoles.php';
$ColeccionRoles = new ColeccionRoles();
?>

<html>
    <head>
        <?php include_once('../lib/headers.php'); ?>

        <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Roles</title>
    </head>
    <body class="sticky-footer">

        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h3>Roles</h3>
                </div>
                <div class="card-body">
                <table class="table table-hover table-sm">
                    <p>
                        <a href="rol.crear.php">
                            <button type="button" class="btn btn-success">
                                <i class="fas fa-plus fa-fw mr-1"></i> Nuevo Rol
                            </button>
                        </a>
                    </p>
                    <tr class="table-info">
                        <th>Nombre</th>
                        <th>Opciones</th>
                    </tr>

                    <?php foreach ($ColeccionRoles->getRoles() as $Rol) {
                        ?>
                        <tr>
                            <td><?= $Rol->getNombre(); ?></td>
                            <td>
                                <a title="Ver detalle" href="rol.ver.php?id=<?= $Rol->getId(); ?>">
                                    <button type="button" class="btn btn-outline-info">
                                        <i class="fas fa-magnifying-glass-plus fa-fw"></i>
                                    </button>
                                </a>
                                <a title="Modificar" href="rol.modificar.php?id=<?= $Rol->getId(); ?>">
                                    <button type="button" class="btn btn-outline-warning">
                                        <i class="fas fa-pen-to-square fa-fw"></i>
                                    </button>
                                </a>
                                <a title="Eliminar" href="rol.eliminar.php?id=<?= $Rol->getId(); ?>">
                                    <button type="button" class="btn btn-outline-danger">
                                        <span class="fas fa-trash-can fa-fw"></span>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
    <?php include_once '../gui/footer.php'; ?>
</body>
</html>

