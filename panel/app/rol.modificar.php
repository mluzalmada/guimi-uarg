<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_ROLES);
include_once '../modelo/Rol.Class.php';
include_once '../modelo/ColeccionPermisos.php';

$id = $_GET["id"];
$Rol = new Rol($id);
$PermisosSistema = new ColeccionPermisos();
?>
<html>
    <head>
        <?php include_once('../lib/headers.php'); ?>

        <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Actualizar Rol</title>

    </head>
    <body class="sticky-footer">
        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <form action="rol.modificar.procesar.php" method="post">
                <div class="card">
                    <div class="card-header">
                        <h3>Actualizar Rol</h3>
                        <p>
                            Complete los campos a continuaci&oacute;n. 
                            Luego, presione el bot&oacute;n <b>Confirmar</b>.<br />
                            Si desea cancelar, presione el bot&oacute;n <b>Cancelar</b>.
                        </p>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="inputNombre">Nombre</label>
                            <input type="text" name="nombre" class="form-control" id="inputNombre" value="<?= $Rol->getNombre(); ?>" placeholder="Ingrese el nombre del Rol" required="">
                        </div>
                        <input type="hidden" name="id" class="form-control" id="id" value="<?= $Rol->getId(); ?>" >
                        <hr />
                        <h3>Permisos</h3>
                        <?php foreach ($PermisosSistema->getPermisos() as $PermisoSistema) {
                            ?>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" 
                                       id="permiso[<?= $PermisoSistema->getId(); ?>]" name="permiso[<?= $PermisoSistema->getId(); ?>]"
                                       value="<?= $PermisoSistema->getId(); ?>" 
                                       <?php echo $Rol->buscarPermisoPorId($PermisoSistema->getId()) ? "checked" : ""; ?> 
                                       />
                                <label class="form-check-label" for="rol">

                                    <?= $PermisoSistema->getNombre(); ?>

                                </label>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-outline-success">
                            <i class="fas fa-check fa-fw mr-1"></i> Confirmar
                        </button>
                        <a href="roles.php">
                            <button type="button" class="btn btn-outline-danger">
                                <i class="fas fa-xmark fa-fw mr-1"></i> Cancelar
                            </button>
                        </a>
                    </div>
                </div>
            </form>
        </div>

        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>