<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_PERMISOS);
include_once '../modelo/ColeccionNovedades.php';
$Novedad = new ColeccionNovedades();
?>

<html>

<head>
    <?php include_once('../lib/headers.php'); ?>

    <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Crear Novedad</title>
</head>

<body class="sticky-footer">
    <?php include_once '../gui/navbar.php'; ?>
    <div class="container">
        <form enctype="multipart/form-data" id="form-crear_novedad">
            <div class="card">
                <div class="card-header">
                    <h3>Crear Novedad</h3>
                    <p>
                        Complete los campos a continuaci&oacute;n.
                        Luego, presione el bot&oacute;n <b>Confirmar</b>.<br />
                        Si desea cancelar, presione el bot&oacute;n <b>Cancelar</b>.
                    </p>
                </div>
                <div class="card-body">
                    <h4>Propiedades</h4>

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="nombre">Nombre:</label>
                                <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Ingrese el nombre de la novedad" required="">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="lugar">Lugar <span class="font-italic text-muted">(Opcional)</span>:</label>
                                <input type="text" name="lugar" class="form-control" id="lugar" placeholder="Ej. Sector a del campus UNPA-UARG">
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label for="fecha_inicio">Fecha de inicio:</label>
                                <input type="date" name="fecha_inicio" class="form-control" id="fecha_inicio">
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label for="fecha_fin">Fecha de fin:</label>
                                <input type="date" name="fecha_fin" class="form-control" id="fecha_fin">
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label for="fecha_notificar">Fecha para notificar:</label>
                                <input type="date" name="fecha_notificar" class="form-control" id="fecha_notificar">
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="descripcion">Descripción <span class="font-italic text-muted">(Opcional)</span>:</label>
                                <textarea class="form-control" rows="3" id="descripcion" name="descripcion" placeholder="Ingrese la descripción de la novedad"></textarea>
                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="visible">Visible en la App:</label>
                                <div class="form-check my-1 mr-sm-2">
                                    <input type="radio" name="visible" class="check-input" id="visible_1" value="1">
                                    <label class="mr-4" for="visible_1">Si</label>
                                    <input type="radio" name="visible" class="check-input" id="visible_0" value="0" checked>
                                    <label class="mr-4" for="visible_0">No</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <label for="foto_ruta" class="control-label">Foto <span class="font-italic text-muted">(Opcional)</span>:</label>
                                <input type="file" class="form-control" id="foto_ruta" name="foto_ruta">
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="tipo_info_adicional">Información adicional</label>
                                <div class="row">
                                    <div class="form-check my-1 mr-sm-2">
                                        <input type="radio" name="tipo_info_adicional" class="check-input" id="info_adicional_texto" value="texto" checked>
                                        <label class="mr-4" for="info_adicional_texto">Mostrar como texto</label>
                                        <input type="radio" name="tipo_info_adicional" class="check-input" id="info_adicional_link" value="link">
                                        <label class="mr-4" for="info_adicional_link">Mostrar como link</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <input type="text" class="form-control" id="informacion_adicional_link" placeholder="Indique la dirección (sin https://)">

                                    </div>
                                    <div class="col-6">
                                        <input type="text" class="form-control" id="informacion_adicional_texto" placeholder="Indique el texto para mostrar">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <input type="text" name="informacion_adicional" class="form-control" id="informacion_adicional" placeholder="Ej. Comunicarse a museo@uarg.unpa.edu.ar para consultas">
                                    </div>
                                </div>

                                <div class="row mt-2 small">
                                    <div class="col-12 hidden" id="informacion_adicional_vista_previa"></div>
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="card-footer">
                        <div class="row justify-content-between">
                            <a href="novedades.php">
                                <button type="button" class="btn btn-outline-danger">
                                    <i class="fas fa-xmark fa-fw mr-1"></i> Cancelar
                                </button>
                            </a>
                            <button type="submit" class="btn btn-outline-success">
                                <i class="fas fa-check fa-fw mr-1"></i> Confirmar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>


    <!-- MODAL RESPUESTA -->
    <div class="modal fade" id="modal-respuesta" tabindex="-1" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div id="modal-contenido">
                        <span id="modal-icon" class="fas fa-check fa-fw my-4 text-success" style="font-size:6rem;"></span>
                        <h5 id="modal-titulo">Creación de Novedad</h5>
                        <h3 id="modal-subtitulo" class="my-2 text-uppercase"> transacción 123</h3>

                        <div id="modal-btn_cerrar" class="row justify-content-around my-3">
                            <div class="col-6">
                                <a href="novedades.php">
                                    <button type="button" class="btn btn-outline-success">Cerrar</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include_once '../gui/footer.php'; ?>
    <script type="application/javascript" src="../lib/js/funciones_ajax/novedad.crear.js"></script>
</body>

</html>
