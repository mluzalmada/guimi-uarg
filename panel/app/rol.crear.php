<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_ROLES);
include_once '../modelo/ColeccionPermisos.php';
$Permiso = new ColeccionPermisos();
?>
<html>
    <head>
        <?php include_once('../lib/headers.php'); ?>

        <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Crear Rol</title>

    </head>
    <body class="sticky-footer">
        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <form action="rol.crear.procesar.php" method="post">
                <div class="card">
                    <div class="card-header">
                        <h3>Crear Rol</h3>
                        <p>
                            Complete los campos a continuaci&oacute;n. 
                            Luego, presione el bot&oacute;n <b>Confirmar</b>.<br />
                            Si desea cancelar, presione el bot&oacute;n <b>Cancelar</b>.
                        </p>
                    </div>
                    <div class="card-body">
                        <h4>Propiedades</h4>
                        <div class="form-group">
                            <label for="inputNombre">Nombre</label>
                            <input type="text" name="nombre" class="form-control" id="inputNombre" placeholder="Ingrese el nombre del Rol" required="">
                        </div>
                        <hr />
                        <h4>Permisos</h4>
                        <?php foreach ($Permiso->getPermisos() as $Permiso) { ?>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="<?= $Permiso->getId(); ?>" id="rol[<?= $Permiso->getId(); ?>]" name="permiso[<?= $Permiso->getId(); ?>]" />
                                <label class="form-check-label" for="permiso">
                                    <?= $Permiso->getNombre(); ?>
                                </label>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-outline-success">
                            <i class="fas fa-check fa-fw mr-1"></i> Confirmar
                        </button>
                        <a href="roles.php">
                            <button type="button" class="btn btn-outline-danger">
                                <i class="fas fa-xmark fa-fw mr-1"></i> Cancelar
                            </button>
                        </a>
                    </div>
                </div>
            </form>
        </div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>
