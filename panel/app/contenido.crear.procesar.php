<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_ROLES);
include_once '../modelo/BDConexion.Class.php';
include_once '../modelo/ColeccionElementos.php';
include_once '../modelo/ColeccionContenidos.php';
include_once '../modelo/TipoContenido.Class.php';
include_once '../modelo/ColeccionArchivos.php';

include_once '../lib/guimi/funciones.php';
include_once '../lib/guimi/registrar_actualizaciones.php';

BDConexion::getInstancia()->autocommit(false);
BDConexion::getInstancia()->begin_transaction();

// Primero crear el objeto Contenido
// Luego, crear el objeto Archivo con el dato {contenido_id}

$DatosFormulario = $_POST;
$ArchivoFormulario = $_FILES['archivo'];

$resultado = array();
$resultado['errores'] = 0;
    
$elemento_id = false;

if(isset($DatosFormulario)) {
    $elemento_id = $DatosFormulario['elemento_id'];
    $Contenido = new Contenido(null, $DatosFormulario);
    
    $resultado['datos'] = $Contenido->getNombre();
    $contenido_id = $Contenido->getId();
    
    $resultado["tamanio"] = getTamanioArchivo($ArchivoFormulario);
    $resultado["archivo"] = $ArchivoFormulario;
    
    // crear Archivo:
    if(isset($ArchivoFormulario) && getTamanioArchivo($ArchivoFormulario) > 0) {
        $tipo_contenido_id = $DatosFormulario['tipo_contenido_id'];
        $TipoContenido = new TipoContenido($tipo_contenido_id);
        $tipo_contenido = strtolower($TipoContenido->getNombre());
        
//        $resultado['archivo'] = $ArchivoFormulario;
        
        $nombre_archivo = $Contenido->getId();
        $extension_archivo = getExtensionArchivo($ArchivoFormulario);
        $directorio_bd = 'media/elementos/'.$elemento_id.'/'.$tipo_contenido.'/';
        $directorio = $_SERVER['DOCUMENT_ROOT'].'/media/elementos/'.$elemento_id.'/'.$tipo_contenido.'/'; 
        $tamanio_archivo = getTamanioArchivo($ArchivoFormulario);
        $tipo_mime = getTipoMime($ArchivoFormulario);
        
        $datos = array(
            "nombre" => $nombre_archivo,
            "extension" => $extension_archivo,
            "ruta" => $directorio_bd,
            "tamanio_bytes" => $tamanio_archivo,
            "contenido_id" => $contenido_id,
            "tipo_mime" => $tipo_mime
        );
        $Archivo = new Archivo(null, $datos);
        
        if($Archivo->getId()) {
            $resultado['guardado'] = guardarArchivo($ArchivoFormulario, $directorio, $nombre_archivo.".".$extension_archivo);
            if($resultado['guardado']) {
                $resultado['transaccion'] = RegistrarActualizacionElemento('1', $elemento_id, "Se agrega contenido [id=".$contenido_id."] al elemento.");
            }
        }
    }
    else {
        $resultado['errores'] = "al crear el archivo para el contenido.";
    }
}
else {
    $resultado['errores'] = "al recibir los datos del formulario";
}

echo json_encode($resultado);
?>
