<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_PERMISOS);
include_once '../modelo/ColeccionNovedades.php';

$id = $_GET["id"];
$Novedad = new Novedad($id);
?>
<html>

<head>
    <?php include_once('../lib/headers.php'); ?>
    <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Actualizar Novedad</title>
</head>

<body class="sticky-footer">
    <?php include_once '../gui/navbar.php'; ?>
    <div class="container">
        <div class="row justify-content-between mb-3">
            <div class="col-6">
                <a href="novedades.php">
                    <button type="button" class="btn btn-primary">
                        <span class="fas fa-arrow-left fa-fw"></span> Volver a las novedades
                    </button>
                </a>
            </div>
        </div>

        <form id="form-editar_novedad" enctype="multipart/form-data">
            <div class="card">
                <div class="card-header">
                    <div class="row justify-content-between">
                        <div class="col-9">
                            <h3>Actualizar datos de la novedad <span class="text-uppercase"><?= $Novedad->getNombre(); ?></span></h3>
                            <p>
                                Complete los campos a continuaci&oacute;n.
                                Luego, presione el bot&oacute;n <b>Confirmar</b>.<br />
                                Si desea cancelar, presione el bot&oacute;n <b>Cancelar</b>.
                            </p>
                        </div>
                        <div class="col-3">
                            <div class="text-center position-relative">
                                <a href="" data-toggle="modal" data-target="#modal-vista_previa">
                                    <img class="img-tabla" src="../<?= $Novedad->getFotoRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input type="text" name="nombre" class="form-control" id="nombre" value="<?= $Novedad->getNombre(); ?>" placeholder="Ingrese el nombre del contenido" required="">
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="lugar">Lugar <span class="font-italic text-muted">(Opcional)</span>:</label>
                                <input type="text" name="lugar" class="form-control" id="lugar" value="<?= $Novedad->getLugar(); ?>" placeholder="Ej. Sector a del campus UNPA-UARG">
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label for="fecha_inicio">Fecha de inicio:</label>
                                <input type="date" name="fecha_inicio" class="form-control" id="fecha_inicio" value="<?= $Novedad->getFechaInicio(); ?>">
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label for="fecha_fin">Fecha de fin:</label>
                                <input type="date" name="fecha_fin" class="form-control" id="fecha_fin" value="<?= $Novedad->getFechafin(); ?>">
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label for="fecha_notificar">Fecha para notificar:</label>
                                <input type="date" name="fecha_notificar" class="form-control" id="fecha_notificar" value="<?= $Novedad->getFechaNotificar(); ?>">
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="descripcion">Descripción <span class="text-muted font-italic">(Opcional)</span>:</label>
                                <textarea rows="3" class="form-control" name="descripcion" id="descripcion" placeholder="Ingrese la descripción del Elemento"><?= $Novedad->getDescripcion(); ?></textarea>
                            </div>
                        </div>

                        <div class="col-md-4 col-12">
                            <div class="form-group">
                                <label for="visible">Visible en la App</label>
                                <div class="form-check my-1 mr-sm-2">
                                    <input type="radio" name="visible" class="check-input" id="visible_1" value="1" <?php
                                           if($Novedad->getVisible() == 1) {
                                               echo " checked";
                                           }
                                           ?>>
                                    <label class="mr-4" for="visible_1">Si</label>
                                    <input type="radio" name="visible" class="check-input" id="visible_0" value="0" <?php
                                           if($Novedad->getVisible() == 0) {
                                               echo " checked";
                                           }
                                           ?>>
                                    <label class="mr-4" for="visible_0">No</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <!--                        VISTA PREVIA DEL CONTENIDO ACTUAL-->
                        <div class="col-md-6 col-12 text-center div-imagen" id="div-imagen">
                            <img class="img-preview" src="../<?=$Novedad->getFotoRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'">

                            <div class="div-overlay">
                                <a href="" data-toggle="modal" data-target="#modal-vista_previa">
                                    <p>Hacé clic para ampliar</p>
                                </a>
                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label for="foto_ruta">Reemplazar foto</label>
                                <input type="file" name="foto_ruta" class="form-control" id="foto_ruta" value="<?= $Novedad->getFotoRuta(); ?>" placeholder="Ingrese la ruta de la foto de la novedad">
                            </div>
                        </div>

                        <div class="col-12 mt-2">
                            <div class="form-group">
                                <label for="tipo_info_adicional">Información adicional</label>
                                <div class="row">
                                    <div class="form-check my-1 mr-sm-2">
                                        <input type="radio" name="tipo_info_adicional" class="check-input" id="info_adicional_texto" value="texto" checked>
                                        <label class="mr-4" for="info_adicional_texto">Mostrar como texto</label>
                                        <input type="radio" name="tipo_info_adicional" class="check-input" id="info_adicional_link" value="link">
                                        <label class="mr-4" for="info_adicional_link">Mostrar como link</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <input type="text" class="form-control" id="informacion_adicional_link" placeholder="Indique la dirección (sin https://)">

                                    </div>
                                    <div class="col-6">
                                        <input type="text" class="form-control" id="informacion_adicional_texto" placeholder="Indique el texto para mostrar">
                                        <input type="hidden" id="informacion_adicional_guardada" value="<?php echo $Novedad->getInformacionAdicional();?>">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <input type="text" name="informacion_adicional" class="form-control" id="informacion_adicional" placeholder="Ej. Comunicarse a museo@uarg.unpa.edu.ar para consultas">
                                    </div>
                                </div>

                                <div class="row mt-2 small">
                                    <div class="col-12 hidden" id="informacion_adicional_vista_previa"></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <input type="hidden" name="id" class="form-control" id="id" value="<?= $Novedad->getId(); ?>">
                        </div>
                    </div>

                </div>

                <div class="card-footer">
                    <div class="row justify-content-around text-center">
                        <div class="col-6">
                            <a href="novedades.php">
                                <button type="button" class="btn btn-outline-danger">
                                    <i class="fas fa-xmark fa-fw mr-1"></i> Cancelar
                                </button>
                            </a>
                        </div>
                        <div class="col-6">
                            <button type="submit" class="btn btn-outline-success">
                                <i class="fas fa-check fa-fw mr-1"></i> Confirmar
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>

    <!-- MODAL VISTA PREVIA -->
    <div class="modal fade" tabindex="-1" aria-hidden="true" id="modal-vista_previa">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Foto de la novedad: <?= $Novedad->getNombre(); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img src="../<?= $Novedad->getFotoRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'" width="100%">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL RESPUESTA -->
    <div class="modal fade" id="modal-respuesta" tabindex="-1" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div id="modal-contenido">
                        <span id="modal-icon" class="fas fa-check fa-fw my-4 text-success" style="font-size:6rem;"></span>
                        <h5 id="modal-titulo">Actualización de <?= $Novedad->getNombre(); ?></h5>
                        <h3 id="modal-subtitulo" class="my-2 text-uppercase"></h3>

                        <div id="modal-btn_cerrar" class="row justify-content-around my-3">
                            <div class="col-6">
                                <a href="novedades.php">
                                    <button type="button" class="btn btn-outline-success">Cerrar</button>
                                </a>
                            </div>
                        </div>
                        <div id="modal-errores" class="row hidden">
                            <hr>
                            <span id="modal-detalle" class="text-uppercase text-muted small"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include_once '../gui/footer.php'; ?>
    <script type="application/javascript" src="../lib/js/funciones_ajax/novedad.modificar.js"></script>
</body>

</html>
