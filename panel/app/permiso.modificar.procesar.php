<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_PERMISOS);
include_once '../modelo/BDConexion.Class.php';
$DatosFormulario = $_POST;
$query = "UPDATE permiso "
        . "SET nombre = '{$DatosFormulario["nombre"]}' "
        . "WHERE id = {$DatosFormulario["id"]}";
$consulta = BDConexion::getInstancia()->query($query);
?>
<html>
    <head>
        <?php include_once('../lib/headers.php'); ?>
        <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Actualizar Permiso</title>
    </head>
    <body class="sticky-footer">
        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <p></p>
            <div class="card">
                <div class="card-header">
                    <h3>Actualizar Permiso</h3>
                </div>
                <div class="card-body">
                    <?php if ($consulta) { ?>
                        <div class="alert alert-success" role="alert">
                            Operaci&oacute;n realizada con &eacute;xito.
                        </div>
                    <?php } ?>   
                    <?php if (!$consulta) { ?>
                        <div class="alert alert-danger" role="alert">
                            Ha ocurrido un error.
                        </div>
                    <?php } ?>
                    <hr />
                    <h5 class="card-text">Opciones</h5>
                    <a href="permisos.php">
                        <button type="button" class="btn btn-primary">
                           <i class="bi-arrow-left-circle-fill mx-2"></i>
                           Volver a lista de Permisos
                        </button>
                    </a>
                </div>
            </div>
        </div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>
