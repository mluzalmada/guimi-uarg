<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_PERMISOS);
include_once '../modelo/BDConexion.Class.php';

include_once '../modelo/Elemento.Class.php';
include_once '../modelo/ColeccionContenidos.php';
include_once '../modelo/ColeccionTiposContenido.php';
include_once '../modelo/Archivo.Class.php';

include_once '../lib/guimi/funciones.php';
include_once '../lib/guimi/registrar_actualizaciones.php';

//BDConexion::getInstancia()->autocommit(false);
//BDConexion::getInstancia()->begin_transaction();

// Primero crear el objeto Contenido
// Luego, crear el objeto Archivo con el dato {contenido_id}

$DatosFormulario = $_POST;
$ArchivoFormulario = $_FILES['archivo'];

$resultado = array();
$resultado['errores'] = 0;

//$resultado['datos_error'] = array();
//$resultado['detalles'] = false;
//$resultado['actualizado'] = false;

$elemento_id = false;

if(isset($DatosFormulario)) {
    $contenido_id = $DatosFormulario["id"];
    $Contenido = new Contenido($contenido_id, null);

    $elemento_id = $Contenido->getElementoId();
    $Elemento = new Elemento($elemento_id);

    // Si también se edita el archivo: 
    if($Contenido && isset($ArchivoFormulario) && !getErroresArchivo($ArchivoFormulario)) {
        $Archivo = new Archivo();
        $Archivo->getArchivoPorContenidoId($contenido_id);

        $TipoContenido = new TipoContenido($Contenido->getTipoContenidoId());
        $tipo_contenido = strtolower($TipoContenido->getNombre());
        
        $nombre_archivo = $Contenido->getId();
        $extension_archivo = getExtensionArchivo($ArchivoFormulario);
        $directorio_bd = 'media/elementos/'.$elemento_id.'/'.$tipo_contenido.'/';
        $directorio = $_SERVER['DOCUMENT_ROOT'].'/media/elementos/'.$elemento_id.'/'.$tipo_contenido.'/'; 
        $tamanio_archivo = getTamanioArchivo($ArchivoFormulario);
        $tipo_mime = getTipoMime($ArchivoFormulario);
        
        $Archivo->setNombre($nombre_archivo);
        $Archivo->setExtension($extension_archivo);
        $Archivo->setTamanioBytes($tamanio_archivo);
        $Archivo->setTipoMime($tipo_mime);
        
        $resultado['guardado'] = guardarArchivo($ArchivoFormulario, $directorio, $nombre_archivo.".".$extension_archivo);
        
        if ($resultado['guardado']) {
            $Archivo->setRuta($directorio_bd.$nombre_archivo);
            $resultado['transaccion'] = RegistrarActualizacionElemento('2', $elemento_id, "Se actualiza el archivo del contenido ".$contenido_id);
        }
        else {
            $resultado['errores'] = "al guardar archivo en el servidor.";
        }
    }
    // Si no se eita el archivo / contiene errores:
else { 
    $resultado['detalles'] = "No se ha cargado un archivo o el mismo contenía errores";
}

    // Actualizar datos del objeto Contenido con los datos obtenidos del formulario:
    if ($Contenido && $DatosFormulario) {
        if(!$Contenido->setNombre($DatosFormulario['nombre'])) {
            $resultado['errores'] ++;
            $resultado['datos_error'] [] = 'nombre';
        }
        if(!$Contenido->setDescripcion($DatosFormulario['descripcion'])) {
            $resultado['errores'] ++;
            $resultado['datos_error'] [] = 'descripcion';
        }
        if(!$Contenido->setVisible($DatosFormulario['visible'])) {
            $resultado['errores'] ++;
            $resultado['datos_error'] [] = 'visible';
        }
        if(!$Contenido->setDuracionTiempo($DatosFormulario['duracion_tiempo'])) {
            $resultado['errores'] ++;
            $resultado['datos_error'] [] = 'duracion_tiempo';
        }
        if(!$Contenido->setDimensionPixeles($DatosFormulario['dimension_pixeles'])) {
            $resultado['errores'] ++;
            $resultado['datos_error'] [] = 'dimension_pixeles';
        }
        if(!$Contenido->setExtensionCaracteres($DatosFormulario['extension_caracteres'])) {
            $resultado['errores'] ++;
            $resultado['datos_error'] [] = 'extension_caracteres';
        }
        if(!$Contenido->setElementoId($elemento_id)) {
            $resultado['errores'] ++;
            $resultado['datos_error'] [] = 'elemento_id';
        }
        if(!$Contenido->setTipoContenidoId($DatosFormulario['tipo_contenido_id'])) {
            $resultado['errores'] ++;
            $resultado['datos_error'] [] = 'tipo_contenido_id';
        }
        if(!$Contenido->setFuenteAtribucion($DatosFormulario['fuente_atribucion'])) {
            $resultado['errores'] ++;
            $resultado['datos_error'] [] = 'fuente_atribucion';
        }
        if( $resultado['errores'] == 0) {
            $resultado['actualizado'] = true;
        }
        else {
            // $resultado['errores'] = "ERROR AL ACTUALIZAR DATOS EN LA BASE DE DATOS";
        }
    }
    else {
        $resultado['errores'] = " AL BUSCAR EL ELEMENTO SELECCIONADO EN LA COLECCIÓN.";
    }
}
else {
    $resultado['errores'] = " AL RECIBIR LOS DATOS DEL FORMULARIO";
}

// Actualizar la tabla actualizacion_elemento:
if(!$resultado['errores']) {
    $actualizacion_id = RegistrarActualizacionElemento('2',$elemento_id, "Se actualizan los datos del contenido ".$contenido_id);
    if($actualizacion_id) {
        $resultado['transaccion'] = $actualizacion_id;
    }    
}

echo json_encode($resultado);
?>
