<?php
include_once '../lib/ControlAcceso.Class.php';
session_destroy();
?>

<html>

<head>
    <?php include_once('../lib/headers.php'); ?>
    <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Salir del Sistema</title>
</head>

<body class="sticky-footer mb-0">


    <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-4">
        <div class="container navbar-dark bg-dark">
            <a class="navbar-brand" href="menu_principal.php">
                <img src="../lib/img/Logo-GUIMI-blanco.png" height="30" class="d-inline-block align-top" alt="GuIMI">
            </a>
        </div>
    </nav>

    <div class="container">
        <div class="card">
            <div class="card-header">
                <h3>Salir del Sistema</h3>
            </div>
            <div class="card-body">
                <div class="alert alert-success" role="alert">
                    Ha salido del sistema <?= Constantes::NOMBRE_SISTEMA; ?>.
                    Ud. seguir&aacute; conectado a su correo electr&oacute;nico.
                </div>
                <p>Elija una de las opciones a continuaci&oacute;n:</p>
                <ul>
                    <li><b>Volver a ingresar</b>: Regresar al sistema.</li>
                    <li><b>Ir a e-mail</b>: Abre el correo electr&oacute;nico.</li>
                    <li><b>Ir a Portal UARG</b>: Abre el Portal de la UARG en otra pesta&ntilde;a.</li>
                </ul>
                <hr />
                <h5 class="card-text">Opciones</h5>
                <a href="index.php">
                    <button type="button" class="btn btn-primary">
                        <i class="fas fa-right-to-bracket fa-fw mr-1"></i> Volver a Ingresar
                    </button></a>
                <a href="http://www.gmail.com">
                    <button type="button" class="btn btn-primary">
                        <i class="fas fa-envelope fa-fw mr-1"></i> Ir a e-mail
                    </button></a>
                <a href="http://www.uarg.unpa.edu.ar" target="_blank">
                    <button type="button" class="btn btn-primary">
                        <span class="fa-earth-americas fa-fw mr-1"></span> Ir a Portal UARG
                    </button></a>
            </div>
        </div>
    </div>
    <?php include_once '../gui/footer.php'; ?>
</body>

</html>

