<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_PERMISOS);
include_once '../modelo/BDConexion.Class.php';
include_once '../modelo/Marcador.Class.php';
include_once '../lib/guimi/registrar_actualizaciones.php';

$respuesta = array();
$respuesta['eliminado'] = false;
if(isset($_POST)) {
    $DatosFormulario = $_POST;
    $marcador_id = $DatosFormulario["id"];
    $respuesta['id'] = $marcador_id;
    $Marcador = new Marcador($marcador_id);
    
    $respuesta['elemento_id'] = $Marcador->getElementoId();
    // Actualizar la tabla actualizacion_elemento:
    $actualizacion_id = RegistrarActualizacionElemento('2',$respuesta['elemento_id'], 'Se elimina el marcador ('.$respuesta["id"].')');

    if($actualizacion_id) {
        $respuesta['transaccion'] = $actualizacion_id;
    }
    if($Marcador->deleteMarcador()) {
//        TODO: ELIMINAR LOS ARCHIVOS DEL SERVIDOR
        $respuesta['eliminado'] = true; 
    }
}
echo json_encode($respuesta);
?>
