<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_USUARIOS);
include_once '../modelo/Usuario.Class.php';

$Usuario = new Usuario($_GET["id"]);
?>
<html>
    <head>
        <?php include_once('../lib/headers.php'); ?>

        <title><?= Constantes::NOMBRE_SISTEMA; ?> - Propiedades del Usuario</title>
    </head>
    <body class="sticky-footer">
        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <p></p>
            <div class="card">
                <div class="card-header">
                    <h3>Propiedades del Usuario</h3>
                </div>
                <div class="card-body">
                    <h4 class="card-text">Nombre</h4>
                    <p> <?= $Usuario->getNombre(); ?></p>
                    <hr>
                    <h4 class="card-text">Email</h4>
                    <p> <?= $Usuario->getEmail(); ?></p>
                    <hr>
                    <h4 class="card-text">Roles</h4>
                    <?php foreach ($Usuario->getRoles() as $Rol) { ?>
                        <p> <?= $Rol->getNombre(); ?> </p>
                    <?php } ?> 
                    <hr>
                    <h5 class="card-text">Opciones</h5>
                    <a href="usuarios.php">
                        <button type="button" class="btn btn-primary">
                            <i class="fa fa-arrow-left fa-fw mr-1"></i> Volver
                        </button>
                    </a>
                </div>
            </div>
        </div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>
