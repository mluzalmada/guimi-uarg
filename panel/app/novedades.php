<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_PERMISOS);
include_once '../modelo/ColeccionNovedades.php';
$ColeccionNovedades = new ColeccionNovedades();
?>

<html>

<head>
    <meta charset="UTF-8">
    <?php include_once('../lib/headers.php'); ?>

    <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Novedades</title>

</head>

<body class="sticky-footer">

    <?php include_once '../gui/navbar.php'; ?>

    <div class="container">
        <div class="card">
            <div class="card-header">
                <h3>Novedades</h3>
            </div>
            <div class="card-body">
                <p>
                    <a href="novedad.crear.php">
                        <button type="button" class="btn btn-success">
                            <i class="fas fa-plus fa-fw mr-1"></i> Agregar novedad
                        </button>
                    </a>
                </p>
                <table class="table table-hover table-sm">
                    <tr class="table-info">
                        <th>Id</th>
                        <th>Foto referencia</th>
                        <th>Nombre</th>
                        <th class="text-center">Visible</th>
                        <th>Opciones</th>
                    </tr>
                    <tr>
                        <?php foreach ($ColeccionNovedades->getNovedades() as $Novedad) { 
                    if($Novedad->getVisible() == '0') { ?>
                    <tr class="table-secondary">
                        <?php }
                        else { ?>
                    <tr>
                        <?php } ?>
                        <td><?= $Novedad->getId(); ?></td>
                        <td>
                            <div class="text-center position-relative">
                                <a href="" data-toggle="modal" data-target="#modal-vista_previa_<?= $Novedad->getId(); ?>">
                                    <img class="img-tabla" src="../<?= $Novedad->getFotoRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'">
                                </a>
                            </div>
                        </td>
                        <td><?= $Novedad->getNombre(); ?></td>
                        <td class="text-center">
                            <?php if ($Novedad->getVisible() == '1') { ?>
                            <i class="fas fa-eye fa-fw text-success"></i>
                            <?php } else { ?>
                            <i class="fas fa-eye-slash fa-fw text-danger"></i>
                            <?php } ?>
                        </td>
                        <td>
                            <button type="button" class="btn btn-outline-info btn-sm w-100 mb-1" data-toggle="modal" data-target="#modal-novedad_ver_<?= $Novedad->getId(); ?>">
                                <i class="fas fa-magnifying-glass-plus fa-fw"></i>
                                <span class="ml-1">Ver detalle</span>
                            </button><br>

                            <a title="Modificar" href="novedad.modificar.php?id=<?= $Novedad->getId(); ?>">
                                <button type="button" class="btn btn-outline-warning btn-sm w-100 mb-1">
                                    <i class="fas fa-pen-to-square fa-fw"></i>
                                    <span class="ml-1">Editar</span>
                                </button>
                            </a>
                            <button type="button" class="btn btn-outline-danger btn-sm w-100 mb-1" data-toggle="modal" data-target="#modal-novedad_eliminar_<?= $Novedad->getId(); ?>">
                                <i class="fas fa-trash-can fa-fw"></i>
                                <span class="ml-1">Eliminar</span>
                            </button><br>
                        </td>
                    </tr>

                    <!-- MODAL ELIMINAR -->
                    <div class="modal fade" id="modal-novedad_eliminar_<?= $Novedad->getId(); ?>" tabindex="-1" aria-labelledby="novedad_eliminar" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body text-center">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div id="modal-contenido_<?= $Novedad->getId(); ?>">
                                        <span id="modal-icon_<?= $Novedad->getId(); ?>" class="fas fa-trash-can fa-fw my-4 text-danger" style="font-size:6rem;"></span>
                                        <h5 id="modal-titulo_<?= $Novedad->getId(); ?>">¿Estás seguro/a de eliminar?</h5>
                                        <h3 id="modal-subtitulo_<?= $Novedad->getId(); ?>" class="my-2 text-uppercase"> <?= $Novedad->getNombre(); ?></h3>
                                        <div id="modal-form_<?= $Novedad->getId(); ?>">
                                            <form data-id="<?= $Novedad->getId(); ?>">
                                                <p class="alert alert-danger my-3">
                                                    <i class="fas fa-triangle-exclamation fa-fw mr-1"></i> ATENCI&Oacute;N: Esta operaci&oacute;n no puede deshacerse.
                                                </p>

                                                <input type="hidden" name="id" class="form-control" id="id" value="<?= $Novedad->getId(); ?>">
                                                <div class="row justify-content-around">
                                                    <div class="col-6">
                                                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><span class="fas fa-xmark fa-fw mr-1"></span> Cancelar</button>
                                                    </div>
                                                    <div class="col-6">
                                                        <button type="submit" class="btn btn-outline-success">
                                                            <span class="fas fa-check fa-fw"></span> Sí, deseo eliminar
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div id="modal-btn_cerrar_<?= $Novedad->getId(); ?>" class="row justify-content-around my-3" style="display:none;">
                                            <div class="col-6">
                                                <a href="novedades.php">
                                                    <button type="button" class="btn btn-outline-success">Cerrar</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- MODAL VER -->
                    <div class="modal fade" id="modal-novedad_ver_<?= $Novedad->getId(); ?>" tabindex="-1" aria-labelledby="novedad_ver" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title text-uppercase" id="label-novedad_ver"><?= $Novedad->getNombre(); ?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                            <span class="text-uppercase font-weight-bold small">Nombre: </span>
                                            <p><?= $Novedad->getNombre(); ?></p>
                                            <span class="text-uppercase font-weight-bold small">Lugar: </span>
                                            <p><?= $Novedad->getLugar(); ?></p>
                                            <span class="text-uppercase font-weight-bold small">Fecha: </span>
                                            <p>
                                                <?php 
    echo date("d-m-Y ", strtotime($Novedad->getFechaInicio())).
        " al ".
        date("d-m-Y ", strtotime($Novedad->getFechaFin()));
    ?>
                                            </p>
                                        </div>

                                        <div class="col-md-6 col-12 text-center div-imagen">
                                            <img class="img-tabla mb-1" src="../<?= $Novedad->getFotoRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'">
                                            <a href="../<?= $Novedad->getFotoRuta(); ?>" target="_blank">
                                                <button class="btn btn-outline-secondary btn-sm py-0 mb-2"><small>Ampliar imagen <i class="bi bi-box-arrow-up-right ml-1"></i></small></button>
                                            </a><br>

                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-12">
                                            <span class="text-uppercase font-weight-bold small">Descripción: </span>
                                            <p> <?= $Novedad->getDescripcion(); ?></p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-6">
                                            <span class="text-uppercase font-weight-bold small">Visible: </span>
                                            <p>
                                                <?php 
    if($Novedad->getVisible() == "1") {
        echo 'Si';
    } else {
        echo 'No';
    } ?>
                                            </p>
                                        </div>
                                        <div class="col-6">
                                            <span class="text-uppercase font-weight-bold small">Notificar: </span>
                                            <p>
                                                <?php 
    if($Novedad->getFechaNotificar()) {
        echo date("d-m-Y ", strtotime($Novedad->getFechaNotificar()));
    }
    else {
        echo "Sin fecha de notificación";
    }
?>
                                            </p>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <span class="text-uppercase font-weight-bold small">Más información: </span>
                                            <p>
                                                <?php 
    if($Novedad->getInformacionAdicional()) {
        echo $Novedad->getInformacionAdicional();
    }
    else {
        echo "Sin información adicional.";
    }
?>
                                            </p>
                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- FIN DEL MODAL-->

                    <!-- MODAL VER IMAGEN-->
                    <div class="modal fade" tabindex="-1" aria-hidden="true" id="modal-vista_previa_<?= $Novedad->getId(); ?>">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Foto de la novedad: <?= $Novedad->getNombre(); ?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <img src="../<?= $Novedad->getFotoRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'" width="100%">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
    <?php include_once '../gui/footer.php'; ?>
    <script type="application/javascript" src="../lib/js/funciones_ajax/novedad.eliminar.js"></script>
</body>

</html>
