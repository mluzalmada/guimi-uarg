<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_ROLES);
include_once '../modelo/Rol.Class.php';
$Rol = new Rol($_GET["id"]);
?>

<html>
    <head>
        <?php include_once('../lib/headers.php'); ?>

        <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Propiedades del Rol</title>
    </head>
    <body class="sticky-footer">
        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <p></p>
            <div class="card">
                <div class="card-header">
                    <h3>Propiedades del Rol</h3>
                </div>
                <div class="card-body">
                    <h4 class="card-text">Nombre</h4>
                    <p> <?= $Rol->getNombre(); ?></p>
                    <hr />

                    <h4 class="card-text">Permisos</h4>
                    <?php foreach ($Rol->getPermisos() as $Permiso) { ?>
                        <p> <?= $Permiso->getNombre(); ?> </p>
                    <?php } ?> 

                    <hr />
                    <h5 class="card-text">Opciones</h5>
                    <a href="roles.php">
                        <button type="button" class="btn btn-primary">
                            <i class="fa fa-arrow-left fa-fw mr-1"></i> Volver
                        </button>
                    </a>
                </div>
            </div>
        </div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>
