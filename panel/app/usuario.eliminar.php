<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_USUARIOS);
include_once '../modelo/Usuario.Class.php';
$id = $_GET["id"];
$Usuario = new Usuario($id);
?>
<html>
    <head>
        <?php include_once('../lib/headers.php'); ?>

        <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Eliminar Usuario</title>
    </head>
    <body class="sticky-footer">
        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <form action="usuario.eliminar.procesar.php" method="post">
                <div class="card">
                    <div class="card-header">
                        <h3>Eliminar Usuario</h3>
                    </div>
                    <div class="card-body">
                        <p class="alert alert-warning ">
                            <i class="fas fa-triangle-exclamation fa-fw mr-1"></i> ATENCI&Oacute;N. Esta operaci&oacute;n no puede deshacerse.
                        </p>
                        <p>¿Est&aacute; seguro que desea eliminar el usuario <b><?= $Usuario->getNombre(); ?></b>?
                    </div>
                    <div class="card-footer">
                        <input type="hidden" name="id" class="form-control" id="id" value="<?= $Usuario->getId(); ?>" >
                        <button type="submit" class="btn btn-outline-success">
                            <span class="fas fa-check fa-fw"></span> Sí, deseo eliminar
                        </button>
                        <a href="usuarios.php">
                            <button type="button" class="btn btn-outline-danger">
                                <span class="fas fa-xmark fa-fw"></span> NO (Salir de esta pantalla)
                            </button>
                        </a>
                    </div>
                </div>
            </form>
        </div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>