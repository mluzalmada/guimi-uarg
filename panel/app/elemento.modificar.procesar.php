<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_PERMISOS);
include_once '../modelo/BDConexion.Class.php';

include_once '../modelo/ColeccionElementos.php';
include_once '../lib/guimi/funciones.php';
include_once '../lib/guimi/registrar_actualizaciones.php';

$respuesta = array();
$respuesta['errores'] = 0;
$respuesta['datos_error'] = array();
$respuesta['detalles'] = false;
$respuesta['actualizado'] = false;

$DatosFormulario = $_POST;
$ArchivoFormulario = $_FILES['foto_ruta'];
$elemento_id = $DatosFormulario["id"];
$Elemento = new Elemento($elemento_id, null);

// Actualizar archivo:
if($Elemento && !getErroresArchivo($ArchivoFormulario) && validarFormatoImagen($ArchivoFormulario)) {
    $directorio = $_SERVER['DOCUMENT_ROOT'].'/media/elementos/'.$DatosFormulario["id"].'/';
    $respuesta['dir'] = $directorio;
    $directorio_bd = 'media/elementos/'.$DatosFormulario["id"].'/';

    $nombre_archivo = 'preview.'.getExtensionArchivo($ArchivoFormulario);

    if (guardarArchivo($ArchivoFormulario, $directorio, $nombre_archivo)) {
        $Elemento->setFotoRuta($directorio_bd.$nombre_archivo);
        $respuesta['detalles'] = "Se ha guardado la imagen con éxito.";
    }
    else {
        $respuesta['datos_error'][] = "al guardar archivo en ".$directorio;
    }
}
else {
    $respuesta['detalles'] = "No se ha cargado una imagen o la misma contiene errores";
}

// Actualizar datos del elemento:
if ($Elemento && $DatosFormulario) {
    if(!$Elemento->setDescripcion($DatosFormulario['descripcion'])) {
        $respuesta['errores'] ++;
        $respuesta['datos_error'] [] = 'descripción';
    }
    if(!$Elemento->setNombre($DatosFormulario['nombre'])) {
        $respuesta['errores'] ++;
        $respuesta['datos_error'] [] = 'nombre';
    }
    if(!$Elemento->setTipo($DatosFormulario['tipo_elemento_id'])) {
        $respuesta['errores'] ++;
        $respuesta['datos_error'] [] = 'tipo_elemento_id';
    }
    if(!$Elemento->setCategoria($DatosFormulario['categoria_elemento_id'])) {
        $respuesta['errores'] ++;
        $respuesta['datos_error'] [] = 'categoria_elemento_id';
    }
    if(!$Elemento->setEstadoConservacion($DatosFormulario['estado_conservacion'])) {
        $respuesta['errores'] ++;
        $respuesta['datos_error'] [] = 'estado_conservacion';
    }
    if(!$Elemento->setFabricante($DatosFormulario['fabricante'])) {
        $respuesta['errores'] ++;
        $respuesta['datos_error'] [] = 'fabricante';
    }
    if(!$Elemento->setModelo($DatosFormulario['modelo'])) {
        $respuesta['errores'] ++;
        $respuesta['datos_error'] [] = 'modelo';
    }
    if(!$Elemento->setAnioReferencia($DatosFormulario['anio_referencia'])) {
        $respuesta['errores'] ++;
        $respuesta['datos_error'] [] = 'anio_referencia';
    }
    if(!$Elemento->setEntregaNombre($DatosFormulario['entrega_nombre'])) {
        $respuesta['errores'] ++;
        $respuesta['datos_error'] [] = 'entrega_nombre';
    }
    if(!$Elemento->setEntregaCaracter($DatosFormulario['entrega_caracter'])) {
        $respuesta['errores'] ++;
        $respuesta['datos_error'] [] = 'entrega_caracter';
    }
    if(!$Elemento->setUbicacion($DatosFormulario['ubicacion'])) {
        $respuesta['errores'] ++;
        $respuesta['datos_error'] [] = 'ubicacion';
    }
    if(!$Elemento->setVisible($DatosFormulario['visible'])) {
        $respuesta['errores'] ++;
        $respuesta['datos_error'] [] = 'visible';
    }
    if(!$Elemento->setIdOmeka($DatosFormulario['id_omeka'])) {
        $respuesta['errores'] ++;
        $respuesta['datos_error'] [] = 'id_omeka';
    }
    if(!$Elemento->setTamanioFisico($DatosFormulario['tamanio_fisico'])) {
        $respuesta['errores'] ++;
        $respuesta['datos_error'] [] = 'tamanio_fisico';
    }
    if($DatosFormulario) {
        if( $respuesta['errores'] == 0) {
            $respuesta['actualizado'] = true;
        }
        else {
            $respuesta['errores'] ++;
//            $respuesta['errores'] = "ERROR AL ACTUALIZAR DATOS EN LA BASE DE DATOS";
        }
    }
    else {
        $respuesta['errores'] ++;
       $respuesta['datos_error'][] = "ERROR AL RECIBIR LOS DATOS DEL FORMULARIO";
    }
}
else {
    $respuesta['errores'] ++;
    $respuesta['datos_error'][] = "ERROR AL BUSCAR EL ELEMENTO SELECCIONADO EN LA COLECCIÓN.";
}

// Actualizar la tabla actualizacion_elemento:
if(!$respuesta['errores']) {
    $actualizacion_id = RegistrarActualizacionElemento('2',$elemento_id);
    if($actualizacion_id) {
        $respuesta['transaccion'] = $actualizacion_id;
    }    
}

echo json_encode($respuesta);
?>
