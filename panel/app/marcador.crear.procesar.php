<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_ROLES);
include_once '../modelo/BDConexion.Class.php';
include_once '../modelo/ColeccionElementos.php';
include_once '../modelo/ColeccionMarcadores.php';

include_once '../lib/guimi/funciones.php';
include_once '../lib/guimi/registrar_actualizaciones.php';

BDConexion::getInstancia()->autocommit(false);
BDConexion::getInstancia()->begin_transaction();

// 1. crear el objeto Marcador
// 2. guardar los archivos en el servidor con el ID
// 3. Actualizar la ruta en el objeto y la BD

$DatosFormulario = $_POST;
$ArchivoFormularioImagen = $_FILES['imagen_ruta'];
$ArchivoFormularioPatron = $_FILES['patron_ruta'];

$resultado = array();
$resultado['errores'] = 0;
$resultado['datos_error'] = array();
    
$elemento_id = false;

if(isset($DatosFormulario) && isset($ArchivoFormularioImagen) && isset($ArchivoFormularioPatron)) {
    $elemento_id = $DatosFormulario['elemento_id'];
    $Marcador = new Marcador(null, $DatosFormulario);
        
    if($Marcador->getExiste()) {
        $marcador_id = $Marcador->getId();
        // Archivo imagen [patron_png]
        if(getTamanioArchivo($ArchivoFormularioImagen) > 0 && getExtensionArchivo($ArchivoFormularioImagen) == "png") {
            $extension_archivo = getExtensionArchivo($ArchivoFormularioImagen);
            $directorio_bd = 'media/elementos/'.$elemento_id.'/patron_png/';
            $directorio = $_SERVER['DOCUMENT_ROOT'].'/media/elementos/'.$elemento_id.'/patron_png/'; 
            $resultado['guardado_imagen'] = guardarArchivo($ArchivoFormularioImagen, $directorio, $marcador_id.".".$extension_archivo);
            if($resultado['guardado_imagen']) {
                if(!$Marcador->setImagenRuta($directorio_bd.$marcador_id.".".$extension_archivo)) {
                    $resultado['datos_error'][] = "error al actualizar el archivo de la imagen en la base de datos";
                }
            }
            else {
                $resultado['datos_error'][] = "error al guardar la imagen en el servidor";
            }
        }
        else {
            $resultado['datos_error'][] = "error en el formato o tamaño de la imagen";
            $resultado['errores'] ++ ;
        }

        // Archivo patrón [patron_patt]
        if(getTamanioArchivo($ArchivoFormularioPatron) > 0 && getExtensionArchivo($ArchivoFormularioPatron) == "patt") {
            $extension_archivo = getExtensionArchivo($ArchivoFormularioPatron);
            $directorio_bd = 'media/elementos/'.$elemento_id.'/patron_patt/';
            $directorio = $_SERVER['DOCUMENT_ROOT'].'/media/elementos/'.$elemento_id.'/patron_patt/'; 
            $resultado['guardado_patron'] = guardarArchivo($ArchivoFormularioPatron, $directorio, $marcador_id.".".$extension_archivo);
            if($resultado['guardado_patron']) {
                if(!$Marcador->setPatronRuta($directorio_bd.$marcador_id.".".$extension_archivo)) {
                    $resultado['datos_error'][] = "error al actualizar el archivo del patrón en la base de datos";
                }
            }
            else {
                $resultado['datos_error'][] = "error al guardar el patrón en ".$directorio;
            }
        }
        else {
            $resultado['datos_error'][] = "error en el formato o tamaño del patrón";
            $resultado['errores'] ++ ;
        }

        // Registrar actualización del elemento
        if($resultado['guardado_imagen'] && $resultado['guardado_patron']) {
            $resultado['transaccion'] = RegistrarActualizacionElemento('2', $elemento_id, "Se guarda imagen/patrón del marcador [id=".$marcador_id."] del elemento.");
        }
    }
    else {
        $resultado['datos_error'] = "al crear el objeto Marcador";
        $resultado['errores'] ++ ;
    }
}
else {
    $resultado['errores'] = "al recibir los datos del formulario";
    $resultado['errores'] ++ ;
}

echo json_encode($resultado);
?>
