<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_ROLES);
include_once '../modelo/BDConexion.Class.php';

include_once '../modelo/ColeccionNovedades.php';

include_once '../lib/guimi/funciones.php';
include_once '../lib/guimi/registrar_actualizaciones.php';

$DatosFormulario = $_POST;
$ArchivoFormulario = $_FILES['foto_ruta'];

BDConexion::getInstancia()->autocommit(false);
BDConexion::getInstancia()->begin_transaction();

$Novedad = new Novedad(null, $DatosFormulario);
$ColeccionNovedades = new ColeccionNovedades(null);
$ColeccionNovedades->addElemento($Novedad);

$novedad_id = false;

$resultado['errores'] = 0;
// El string $resultado['errores'] se mostrará luego de "Ha ocurrido un error..." en caso que su valor sea != 0 

if ($Novedad) {
    $novedad_id = $Novedad->getId();
    
    // Actualizar la tabla actualizacion_novedad:
    $actualizacion_id = RegistrarActualizacionNovedad('1',$novedad_id, "Creación de la novedad: ".$Novedad->getNombre()); 
    if($actualizacion_id) {
        $resultado['transaccion'] = $actualizacion_id;
    }
    
    // Se guarda la imagen preview de la novedad:
    if(!getErroresArchivo($ArchivoFormulario) && $novedad_id) {
        $extension = getExtensionArchivo($ArchivoFormulario);
        $directorio = $_SERVER['DOCUMENT_ROOT'].'/admin/panel/media/novedades/';

        $nombre_archivo = $novedad_id.".".$extension;
        $directorio_bd = 'media/novedades/';

        // Se actualiza la BD con la ubicación del archivo:
        if (guardarArchivo($ArchivoFormulario, $directorio, $nombre_archivo)) {
            $actualizar_preview = $Novedad->setFotoRuta($directorio_bd.$nombre_archivo);

            if(!$actualizar_preview) {
                $resultado['errores'] = "al actualizar la ruta de la imagen $nombre_archivo en la base de datos.";
            }
            else {
                // Actualizar la tabla actualizacion_novedad:
                $actualizacion_id = RegistrarActualizacionNovedad('2',$novedad_id, "Se guarda la imagen de la novedad: ".$Novedad->getNombre());
                if($actualizacion_id) {
                    $resultado['transaccion'] = $actualizacion_id;
                }
    
            }
        }
        else {
            $resultado['errores'] = "al guardar archivo en $directorio$nombre_archivo";
        }
    }   
}
else {
    BDConexion::getInstancia()->rollback();
    die(BDConexion::getInstancia()->errno);
}

echo json_encode($resultado);
?>
