<?php
$ColeccionContenidos = new ColeccionContenidos();
$TipoContenido = new ColeccionTiposContenido(); 
?>

<html>

<head>
    <meta charset="UTF-8">
    <?php include_once('../lib/headers.php'); ?>

    <title>
        <?php echo Constantes::NOMBRE_SISTEMA." - Contenidos de todos los elementos de la colección"; ?>
    </title>

</head>

<body class="sticky-footer">

    <?php include_once '../gui/navbar.php'; ?>
    <div class="container">
        <div class="row justify-content-between mb-3">
            <div class="col-6">
                <a href="elementos.php">
                    <button type="button" class="btn btn-primary">
                        <span class="fas fa-arrow-left fa-fw"></span> Volver a la colección
                    </button>
                </a>
            </div>
        </div>

        <div class="card">
            <?php 
            if(!$ColeccionContenidos->getContenidos()) { ?>
            <div class='alert alert-info'>La colección de contenidos de GuIMI está vacía! Agrega contenido para verlo acá.</div>
            <?php }
            else {
            ?>
            <div class="card-header">
                <div class="row justify-content-between">
                    <div class="col-9">
                        <h3>Contenidos de todos los elementos de la colección</h3>
                    </div>
                    <div class="col-3">
                        <div class="text-center position-relative">
                            <a href="" data-toggle="modal" data-target="#modal-vista_previa">
                                <img class="img-tabla" src="../lib/img/museo_unpa_uarg.jpg" onerror="this.src='../media/imagen_no_encontrada.png'">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">

                <table class="table table-hover table-sm">
                    <thead>
                        <tr>
                            <th rowspan="2">Elemento</th>
                            <th colspan="4" class="text-center">Contenido</th>

                        </tr>
                        <tr>
                            <th>Tipo</th>
                            <th>Nombre</th>
                            <th class="text-center">Visible</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <?php 
                foreach ($ColeccionContenidos->getContenidos() as $Contenido) {
                    $Elemento = new Elemento($Contenido->getElementoId());
                    if($Contenido->getVisible() == '0') { ?>
                    <tr class="table-secondary">
                        <?php }
                        else { ?>
                    <tr>
                        <?php } ?>
                        <td>
                            <?=$Elemento->getNombre();?>
                        </td>
                        <td>
                            <?php
                                 foreach ($TipoContenido->getTiposContenido() as $Tipo) { 
                                    if($Tipo->getId() == $Contenido->getTipoContenidoID()) {
                                        $tipo_contenido = $Tipo->getNombre();
                                        echo $tipo_contenido;
                                    }
                            } ?>
                        </td>
                        <td>
                            <?= $Contenido->getNombre(); ?>
                        </td>
                        <td class="text-center">
                            <?php if ($Contenido->getVisible() == '1') { ?>
                            <i class="fas fa-eye fa-fw text-success"></i>
                            <?php } else { ?>
                            <i class="fas fa-eye-slash fa-fw text-danger"></i>
                            <?php } ?>
                        </td>
                        <td>
                            <button type="button" class="btn btn-outline-info btn-sm w-100 mb-1" data-toggle="modal" data-target="#modal-contenido_ver_<?= $Contenido->getId(); ?>">
                                <i class="fas fa-magnifying-glass-plus fa-fw"></i>
                                <span class="ml-1">Ver detalle</span>
                            </button><br>
                            <a title="Modificar" href="contenido.modificar.php?id=<?= $Contenido->getId(); ?>">
                                <button type="button" class="btn btn-outline-warning btn-sm w-100 mb-1">
                                    <i class="fas fa-pen-to-square fa-fw"></i>
                                    <span class="ml-1">Editar</span>
                                </button><br>
                            </a>
                            <button type="button" class="btn btn-outline-danger btn-sm w-100 mb-1" data-toggle="modal" data-target="#modal-contenido_eliminar_<?= $Contenido->getId(); ?>">
                                <i class="fas fa-trash-can fa-fw"></i>
                                <span class="ml-1">Eliminar</span>
                            </button><br>
                        </td>
                    </tr>

                    <!-- MODAL ELIMINAR -->
                    <div class="modal fade" id="modal-contenido_eliminar_<?= $Contenido->getId(); ?>" tabindex="-1" aria-labelledby="contenido_eliminar" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body text-center">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div id="modal-contenido_<?= $Contenido->getId(); ?>">
                                        <span id="modal-icon_<?= $Contenido->getId(); ?>" class="fas fa-trash-can fa-fw my-4 text-danger" style="font-size:6rem;"></span>
                                        <h5 id="modal-titulo_<?= $Contenido->getId(); ?>">¿Estás seguro/a de eliminar?</h5>
                                        <h3 id="modal-subtitulo_<?= $Contenido->getId(); ?>" class="my-2 text-uppercase"> <?= $Contenido->getNombre(); ?></h3>
                                        <div id="modal-form_<?= $Contenido->getId(); ?>">
                                            <form data-id="<?= $Contenido->getId(); ?>">
                                                <p class="alert alert-danger my-3">
                                                    <i class="fas fa-triangle-exclamation fa-fw mr-1"></i> ATENCI&Oacute;N: Esta operaci&oacute;n no puede deshacerse.
                                                </p>

                                                <input type="hidden" name="id" class="form-control" id="id" value="<?= $Contenido->getId(); ?>">
                                                <div class="row justify-content-around">
                                                    <div class="col-6">
                                                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><span class="fas fa-xmark fa-fw mr-1"></span> Cancelar</button>
                                                    </div>
                                                    <div class="col-6">
                                                        <button type="submit" class="btn btn-outline-success">
                                                            <span class="fas fa-check fa-fw"></span> Sí, deseo eliminar
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div id="modal-btn_cerrar_<?= $Contenido->getId(); ?>" class="row justify-content-around my-3" style="display:none;">
                                            <div class="col-6">
                                                <a href="contenidos.php?id=<?= $Contenido->getId(); ?>">
                                                    <button type="button" class="btn btn-outline-success">Cerrar</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- MODAL VER -->
                    <div class="modal fade" id="modal-contenido_ver_<?= $Contenido->getId(); ?>" tabindex="-1" aria-labelledby="contenido_ver" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title text-uppercase" id="label-contenido_ver"><?= $Contenido->getNombre(); ?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                            <span class="text-uppercase font-weight-bold small">Nombre: </span>
                                            <p><?= $Contenido->getNombre(); ?></p>
                                            <span class="text-uppercase font-weight-bold small">Tipo: </span>
                                            <p>
                                                <?php 
           foreach ($TipoContenido->getTiposContenido() as $Tipo) {
               if($Contenido->getTipoContenidoId() == $Tipo->getId()) {
                   echo $Tipo->getNombre();
               }
           } ?>
                                            </p>
                                            <span class="text-uppercase font-weight-bold small">Visible: </span>
                                            <p>
                                                <?php 
    if($Contenido->getVisible() == "1") {
        echo 'Si';
    } else {
        echo 'No';
    } ?>
                                            </p>

                                        </div>
                                        <div class="col-md-6 col-12 text-center div-imagen">
                                            <img class="img-tabla mb-1" src="../<?= $Elemento->getFotoRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'">
                                            <a href="../<?= $Elemento->getFotoRuta(); ?>" target="_blank">
                                                <button class="btn btn-outline-secondary btn-sm py-0 mb-2"><small>Ampliar imagen <i class="bi bi-box-arrow-up-right ml-1"></i></small></button>
                                            </a>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-12">
                                            <span class="text-uppercase font-weight-bold small">Descripción: </span>
                                            <p> <?= $Contenido->getDescripcion(); ?></p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-6">
                                            <span class="text-uppercase font-weight-bold small">Duración: </span>
                                            <p>
                                                <?php 
    if($Contenido->getDuracionTiempo()) {
        echo $Contenido->getDuracionTiempo().".";
    }
    else {
        echo "SIN DATO";
    }
                                                ?>
                                            </p>
                                        </div>
                                        <div class="col-6">
                                            <span class="text-uppercase font-weight-bold small">Dimensión: </span>
                                            <p>
                                                <?php
    if($Contenido->getDimensionPixeles()) {
        echo $Contenido->getDimensionPixeles()." píxeles.";
    }
    else {
        echo "SIN DATO";
    }
                                                ?>
                                            </p>
                                        </div>
                                        <div class="col-6">
                                            <span class="text-uppercase font-weight-bold small">Extensión: </span>
                                            <p>
                                                <?php
    if($Contenido->getExtensionCaracteres()) {
        echo $Contenido->getExtensionCaracteres()." caracteres.";
    }
    else {
        echo "SIN DATO";
    }
    ?>
                                            </p>
                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <div class="row justify-content-between w-100">
                                        <div class="col-8">
                                            <span class="text-uppercase text-muted small">CONTENIDO (<?=$tipo_contenido;?>) DEL ELEMENTO <?=$Elemento->getNombre();?></span>
                                        </div>
                                        <div class="col-4 text-right">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- FIN DEL MODAL-->

                    <?php } ?>
                </table>
            </div>
            <?php } ?>
        </div>
        <!-- MODAL VER IMAGEN-->
        <div class="modal fade" tabindex="-1" aria-hidden="true" id="modal-vista_previa">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Foto del Museo</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img src="../lib/img/museo_unpa_uarg.jpg" onerror="this.src='../media/imagen_no_encontrada.png'" width="100%">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <?php include_once '../gui/footer.php'; ?>
    <script type="application/javascript" src="../lib/js/funciones_ajax/contenido.eliminar.js"></script>

</body>

</html>
