<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_PERMISOS);
include_once '../modelo/BDConexion.Class.php';

include_once '../modelo/ColeccionNovedades.php';

include_once '../lib/guimi/funciones.php';
include_once '../lib/guimi/registrar_actualizaciones.php';

$resultado = array();
$resultado["errores"] = 0;
$resultado['datos_error'] = array();
$resultado['detalles'] = false;
$resultado['actualizado'] = false;

$DatosFormulario = $_POST;
$ArchivoFormulario = $_FILES['foto_ruta'];
$novedad_id = $DatosFormulario["id"];
$Novedad = new Novedad($novedad_id, null);

// Actualizar archivo:
if($Novedad && !getErroresArchivo($ArchivoFormulario) && validarFormatoImagen($ArchivoFormulario)) {
    $directorio = $_SERVER['DOCUMENT_ROOT'].'/media/novedades/';
    $directorio_bd = 'media/novedades/';

    $nombre_archivo = $DatosFormulario["id"].'.'.getExtensionArchivo($ArchivoFormulario);
    
    if (guardarArchivo($ArchivoFormulario, $directorio, $nombre_archivo)) {
        $Novedad->setFotoRuta($directorio_bd.$nombre_archivo);
        $resultado['detalles'] = "Se ha guardado la imagen con éxito.";
        $actualizacion_id = RegistrarActualizacionNovedad('2', $novedad_id, "Se actualiza la foto de la novedad: ".$Novedad->getNombre()); 
        if($actualizacion_id) {
            $resultado['transaccion'] = $actualizacion_id;
        }
    }
    else {
        $resultado['errores'] = " al guardar archivo en el servidor.";
    }
}
else { 
    $resultado['detalles'] = "No se ha cargado una imagen o la misma contiene errores";
}

// Actualizar datos de la novedad:
if ($Novedad && $DatosFormulario) {
    if(!$Novedad->setNombre($DatosFormulario['nombre'])) {
        $resultado['errores'] ++;
        $resultado['datos_error'] [] = 'error al actualizar el nombre. ';
    }
    if(!$Novedad->setDescripcion($DatosFormulario['descripcion'])) {
        $resultado['errores'] ++;
        $resultado['datos_error'] [] = 'error al actualizar la descripción. ';
    }
    if(!$Novedad->setLugar($DatosFormulario['lugar'])) {
        $resultado['errores'] ++;
        $resultado['datos_error'] [] = 'error al actualizar el lugar. ';
    }
    if(!$Novedad->setFechaInicio($DatosFormulario['fecha_inicio'])) {
        $resultado['errores'] ++;
        $resultado['datos_error'] [] = 'error al actualizar la fecha de inicio. ';
    }
    if(!$Novedad->setFechaFin($DatosFormulario['fecha_fin'])) {
        $resultado['errores'] ++;
        $resultado['datos_error'] [] = 'error al actualizar la fecha de fin. ';
    }
    if(!$Novedad->setFechaNotificar($DatosFormulario['fecha_notificar'])) {
        $resultado['errores'] ++;
        $resultado['datos_error'] [] = 'error al actualizar la fecha para notificar. ';
    }
    if(!$Novedad->setVisible($DatosFormulario['visible'])) {
        $resultado['errores'] ++;
        $resultado['datos_error'] [] = 'error al actualizar la visibilidad. ';
    }
    if(!$Novedad->setInformacionAdicional($DatosFormulario['informacion_adicional'])) {
        $resultado['errores'] ++;
        $resultado['datos_error'] [] = 'error al actualizar la informacion adicional. ';
    }    
    if(!$resultado['errores']) {
        $resultado['actualizado'] = true;
        $actualizacion_id = RegistrarActualizacionNovedad('2', $novedad_id, "Se actualizan los datos de la novedad: ".$Novedad->getNombre());
        if($actualizacion_id) {
            $resultado['transaccion'] = $actualizacion_id;
        }
    }
}
else {
    $resultado['datos_error'][] = "error al recibir los datos del formulario";
}

echo json_encode($resultado);
?>
