<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_COLECCION);
include_once '../modelo/BDConexion.Class.php';
include_once '../modelo/CategoriaElemento.Class.php';

$respuesta = array();
$respuesta['eliminado'] = false;

if(isset($_POST)) {
    $DatosFormulario = $_POST;
    $categoria_elemento_id = $DatosFormulario["id"];
    $CategoriaElemento = new CategoriaElemento($categoria_elemento_id);
    
    if($CategoriaElemento->deleteCategoriaElemento()) {
        $respuesta['eliminado'] = true; 
    }
}
echo json_encode($respuesta);
?>
