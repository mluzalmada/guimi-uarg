<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_PERMISOS);
include_once '../modelo/Elemento.Class.php';
include_once '../modelo/ColeccionTiposContenido.php';
$TipoContenido = new ColeccionTiposContenido();

//pruebas
include_once '../modelo/ColeccionContenidos.php';
//pruebas
?>

<html>

<head>
    <?php include_once('../lib/headers.php'); ?>

    <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Crear Contenido</title>
</head>

<body class="sticky-footer">
    <?php include_once '../gui/navbar.php'; ?>
    <div class="container">
        <div class="row justify-content-between mb-3">
            <div class="col-6">
                <a href="elementos.php">
                    <button type="button" class="btn btn-primary">
                        <span class="fas fa-arrow-left fa-fw"></span> Volver a la colección
                    </button>
                </a>
            </div>
        </div>
        
        <form enctype="multipart/form-data" id="form-crear_contenido">
            <div class="card">
                <?php 
if(isset($_GET["id"])) {
    $id = $_GET["id"];
    $Elemento = new Elemento($id);
?>
                <div class="card-header">
                    <div class="row justify-content-between">
                        <div class="col-9">
                            <h3>Crear Contenido para <?= $Elemento->getNombre();?></h3>
                            <p>
                                Complete los campos a continuaci&oacute;n.
                                Luego, presione el bot&oacute;n <b>Confirmar</b>.<br />
                                Si desea cancelar, presione el bot&oacute;n <b>Cancelar</b>.
                            </p>

                        </div>
                        <div class="col-3">
                            <div class="text-center position-relative">
                                <a href="" data-toggle="modal" data-target="#modal-vista_previa_<?= $Elemento->getId(); ?>">
                                    <img class="img-tabla" src="../<?= $Elemento->getFotoRuta(); ?>" onerror="this.src='../media/imagen_no_encontrada.png'">
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h4>Propiedades</h4>
                    <input type="hidden" id="elemento_id" name="elemento_id" value="<?=$Elemento->getId();?>">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="nombre">Nombre:</label>
                                <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Ingrese el nombre o título del contenido" required="">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="tipo_contenido_id">Tipo:</label>
                                <select class="form-control" name="tipo_contenido_id" id="tipo_contenido_id">
                                    <?php 
                               foreach ($TipoContenido->getTiposContenido() as $Tipo) { ?>
                                    <option value="<?= $Tipo->getId(); ?>" id="tipo_<?= $Tipo->getId(); ?>">
                                        <?= $Tipo->getNombre(); ?>
                                    </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="descripcion">Descripción <span class="text-muted font-italic">(Opcional)</span>:</label>
                                <textarea class="form-control" rows="3" id="descripcion" name="descripcion" placeholder="Ingrese la descripción del contenido"></textarea>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="descripcion">Fuente o atribución por el contenido <span class="text-muted font-italic">(Opcional)</span>:</label>
                                <input type="text" class="form-control" name="fuente_atribucion" id="fuente_atribucion" placeholder="Indica el link de atribución o nombre de autor/propietario del contenido">
                            </div>
                        </div>

                        <div class="col-md-4 col-12">
                            <div class="form-group">
                                <label for="duracion_tiempo">Duración <span class="text-muted font-italic">(Opcional)</span>:</label>
                                <input type="tel" name="duracion_tiempo" class="form-control" id="duracion_tiempo" placeholder="Ej. 12:34">
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="form-group">
                                <label for="dimension_pixeles">Dimensión (en píxeles) <span class="text-muted font-italic">(Opcional)</span>:</label>
                                <input type="tel" name="dimension_pixeles" class="form-control" id="dimension_pixeles" placeholder="Ej. 720x1200">
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="form-group">
                                <label for="extension_caracteres">Extensión del texto (en caracteres) <span class="text-muted font-italic">(Opcional)</span>:</label>
                                <input type="tel" name="extension_caracteres" class="form-control" id="extension_caracteres" placeholder="Ej. 140">
                            </div>
                        </div>

                        <div class="col-md-4 col-12">
                            <div class="form-group">
                                <label for="visible">Visible en la App</label>
                                <div class="form-check my-1 mr-sm-2">
                                    <input type="radio" name="visible" class="check-input" id="visible_1" value="1">
                                    <label class="mr-4" for="visible_1">Si</label>
                                    <input type="radio" name="visible" class="check-input" id="visible_0" value="0" checked>
                                    <label class="mr-4" for="visible_0">No</label>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-8 col-12">
                            <div class="form-group">
                                <label for="archivo" class="control-label">Archivo: </label>
<!--                                <input type="hidden" name="MAX_FILE_SIZE" value="30000">-->
                                <input type="file" class="form-control" id="archivo" name="archivo">
                            </div>

                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row justify-content-between">
                        <a href="contenidos.php?id=<?=$Elemento->getId();?>">
                            <button type="button" class="btn btn-outline-danger">
                                <i class="fas fa-xmark fa-fw mr-1"></i> Cancelar
                            </button>
                        </a>
                        <button type="submit" class="btn btn-outline-success">
                            <i class="fas fa-check fa-fw mr-1"></i> Confirmar
                        </button>
                    </div>
                </div>
                <?php }
                else {?>
                <div class="alert alert-warning">Ups! No ha seleccionado ningún elemento. Los contenidos sólo pueden crearse si se relacionan a un elemento de la colección.</div>
                <?php }
                ?>
                
            </div>
        </form>

    </div>

    <!-- MODAL RESPUESTA -->
    <div class="modal fade" id="modal-respuesta" tabindex="-1" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div id="modal-contenido">
                        <span id="modal-icon" class="fas fa-check fa-fw my-4 text-success" style="font-size:6rem;"></span>
                        <h5 id="modal-titulo">Creación de Elemento</h5>
                        <h3 id="modal-subtitulo" class="my-2 text-uppercase"> transacción 123</h3>

                        <div id="modal-btn_cerrar" class="row justify-content-around my-3">
                            <div class="col-6">
                                <a href="contenidos.php?id=<?=$Elemento->getId();?>">
                                    <button type="button" class="btn btn-outline-success">Cerrar</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include_once '../gui/footer.php'; ?>
    <script type="application/javascript" src="../lib/js/funciones.js"></script>
    <script type="application/javascript" src="../lib/js/funciones_ajax/contenido.crear.js"></script>
</body>

</html>