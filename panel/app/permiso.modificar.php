<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_PERMISOS);
include_once '../modelo/Permiso.Class.php';
$id = $_GET["id"];
$Permiso = new Permiso($id);
?>
<html>
    <head>
        <?php include_once('../lib/headers.php'); ?>

        <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Actualizar Permiso</title>

    </head>
    <body class="sticky-footer">
        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <form action="permiso.modificar.procesar.php" method="post">
                <div class="card">
                    <div class="card-header">
                        <h3>Actualizar Permiso</h3>
                        <p>
                            Complete los campos a continuaci&oacute;n. 
                            Luego, presione el bot&oacute;n <b>Confirmar</b>.<br />
                            Si desea cancelar, presione el bot&oacute;n <b>Cancelar</b>.
                        </p>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="inputNombre">Nombre</label>
                            <input type="text" name="nombre" class="form-control" id="inputNombre" value="<?= $Permiso->getNombre(); ?>" placeholder="Ingrese el nombre del Permiso" required="">
                        </div>

                        <input type="hidden" name="id" class="form-control" id="id" value="<?= $Permiso->getId(); ?>" >
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-outline-success">
                            <i class="fas fa-check fa-fw mr-1"></i> Confirmar
                        </button>
                        <a href="permisos.php">
                            <button type="button" class="btn btn-outline-danger">
                                <i class="fas fa-xmark fa-fw mr-1"></i> Cancelar
                            </button>
                        </a>
                    </div>
                </div>
            </form>
        </div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>