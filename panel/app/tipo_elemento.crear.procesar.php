<?php
include_once '../lib/ControlAcceso.Class.php';
ControlAcceso::requierePermiso(PermisosSistema::PERMISO_ROLES);
include_once '../modelo/BDConexion.Class.php';
$DatosFormulario = $_POST;
BDConexion::getInstancia()->autocommit(false);
BDConexion::getInstancia()->begin_transaction();

$query = "INSERT INTO ". Constantes::BD_SCHEMA.".tipo_elemento "
. "(id, nombre, descripcion)"
. "VALUES (null, '{$DatosFormulario["nombre"]}', '{$DatosFormulario["descripcion"]}');";

$consulta = BDConexion::getInstancia()->query($query);

if (!$consulta) {
    BDConexion::getInstancia()->rollback();
    //arrojar una excepcion
    die(BDConexion::getInstancia()->errno);
}

BDConexion::getInstancia()->commit();
BDConexion::getInstancia()->autocommit(true);
?>
<html>
    <head>
       <?php
        include_once('../lib/headers.php'); 
        include_once '../gui/navbar.php'; 
        ?>
                <title><?php echo Constantes::NOMBRE_SISTEMA; ?> - Crear Tipo de Elemento</title>

    </head>
    <body class="sticky-footer">
        <?php include_once '../gui/navbar.php'; ?>
        <div class="container">
            <p></p>
            <div class="card">
                <div class="card-header">
                    <h3>Crear Tipo de Elemento</h3>
                </div>
                <div class="card-body">
                    <?php if ($consulta) { ?>
                        <div class="alert alert-success" role="alert">
                            Operaci&oacute;n realizada con &eacute;xito.
                        </div>
                    <?php } ?>   
                    <?php if (!$consulta) { ?>
                        <div class="alert alert-danger" role="alert">
                            Ha ocurrido un error.
                        </div>
                    <?php } ?>
                    <hr />
                    <h5 class="card-text">Opciones</h5>
                    <a href="tipos_elemento.php">
                        <button type="button" class="btn btn-primary">
                            <i class="fa fa-arrow-left fa-fw mr-1"></i> Volver
                        </button>
                    </a>
                </div>
            </div>
        </div>
        <?php include_once '../gui/footer.php'; ?>
    </body>
</html>