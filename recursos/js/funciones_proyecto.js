$(function() {
    $('.ir-arriba').click(desplazarHaciaArriba);

    function desplazarHaciaArriba() {
        $('body, html').animate({
            scrollTop: '0px'
        }, 300);
    }

    $(window).scroll(mostrarBotonIrArriba);

    function mostrarBotonIrArriba() {
        if ($(this).scrollTop() > 0) {
            $('.ir-arriba').slideDown(300);
        } else {
            $('.ir-arriba').slideUp(300);
        }
    }

    $('.titulo-indice').click(desplazarAlContenido);

    function desplazarAlContenido() {
        $('html, body').animate({
            scrollTop: $('#titulo-manual').offset().top
        }, 2000);
    }
});