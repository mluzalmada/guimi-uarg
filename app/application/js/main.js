import { openDB } from 'idb';
import { wrap } from 'comlink';

import swURL from 'sw:../service-worker.js';

if ('serviceWorker' in navigator) {
  window.addEventListener('load', async () => {
    try {
      const reg = await navigator.serviceWorker.register(swURL);
      console.log('¡Service worker registrado con éxito!', reg);
    } catch (err) {
      console.log('No se pudo registrar el service worker: ', err);
    }
  });
}

window.addEventListener('DOMContentLoaded', async () => {
  const db = await openDB('settings-store', 1, {
    upgrade(db) {
      db.createObjectStore('settings');
    },
  });

  const worker = new SharedWorker('/js/worker.js', {
    type: 'module',
  });
  const compiler = wrap(worker.port);

  
  const { NightMode } = await import('./app/night-mode.js');
  new NightMode(
    document.querySelector('#mode'),
    async (mode) => {
      editor.setTheme(mode);
      await db.put('settings', mode, 'night-mode');
    },
    await db.get('settings', 'night-mode'),
  );

  const { Install } = await import('./lib/install.js');
  new Install(document.querySelector('#install'));
});
