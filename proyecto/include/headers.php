<?php
setlocale(LC_TIME, 'es_AR.utf8');
const NOMBRE_SISTEMA = "GuIMI";
?>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Guía interactiva para el Museo de Informática">
<meta name="author" content="GuIMI Enterprise">
<link rel="shortcut icon" type="image/png" href="../recursos/img/favicons/favicon.ico" />

<!--Bootstrap v4.6.1-->
<link rel="stylesheet" href="../recursos/css/bootstrap_4.6.1.min.css">
<link rel="stylesheet" href="../recursos/fontawesome/css/all.min.css">
<link rel="stylesheet" href="../recursos/css/estilos_nucleo.css">
<link rel="stylesheet" href="../recursos/css/estilos_proyecto.css">

<script type="text/javascript" src="../recursos/js/jquery-3.6.0.min.js"></script>
<script type="text/javascript" src="../recursos/js/bootstrap_4.6.1.bundle.min.js"></script>
<script type="text/javascript" src="../recursos/fontawesome/js/all.min.js"></script>
