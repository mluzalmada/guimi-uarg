<main role="main">
    <div class="container">

        <div class="card p-2 mx-0 mb-3">
            <a href="index.php" class="text-uppercase small text-orange-guimi">
                <i class="fas fa-arrow-left mr-2"></i>
                Volver al Inicio</a>
        </div>

        <section>
            <div class="row">
                <aside class="col-md-4 col-md-pull-8">
                    <h4 id="titulo-menu_manual" class="text-orange-guimi">PROYECTO GUIMI</h4>
                    <nav class="nav flex-column nav-pills col-12" id="nav-manual">
                        <a href="#como_surge_guimi" role="tab" data-toggle="tab" class="nav-link titulo-indice active text-orange-guimi">¿Cómo surge?</a>
                        <a href="#en_que_consiste_guimi" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">¿En qué consiste?</a>
                        <a href="#cual_es_la_importancia" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">¿Cuál es la importancia?</a>
                        <a href="#como_se_desarrolla" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">¿Cómo se desarrolla?</a>
                        <a href="#como_se_implementa" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">¿Cómo se implementa?</a>
                        <!--                        <a href="#funciones_principales" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Funciones principales</a>-->

                        <div role="separator" class="dropdown-divider"></div>
                        <a href="#propuesta" role="button" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi"><strong>PROPUESTA</strong></a>
                        <a href="#propuesta_desarrollo" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Propuesta de desarrollo</a>

                        <div role="separator" class="dropdown-divider"></div>
                        <a href="#requerimientos" role="button" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi"><strong>REQUERIMIENTOS</strong></a>
                        <a href="#resumen_entrevista" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Resumen de entrevista</a>
                        <a href="#reunion_requerimientos" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Reunión de requerimientos</a>
                        <a href="#especificacion_requerimientos" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Especificacion de requerimientos</a>

                        <div role="separator" class="dropdown-divider"></div>
                        <a href="#analisis_disenio" role="button" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi"><strong>ANÁLISIS Y DISEÑO</strong></a>
                        <a href="#estudio_factibilidad" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Estudio de factibilidad</a>
                        <a href="#modelo_cu" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Modelo de Casos de Uso</a>
                        <a href="#modelo_datos" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Modelo de datos</a>
                        <a href="#modelo_disenio" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Modelo de diseño</a>
                        <a href="#modelo_negocio" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Modelo de negocio</a>
                        <a href="#prototipo_interfaz" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Prototipo de interfaz</a>
                        <a href="#arquitectura_sistema" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Arquitectura del sistema</a>
                        <a href="#modelo_vision" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Modelo de Visión</a>
                        <a href="#especificacion_cu" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Especificación de Casos de Uso</a>
                        <a href="#glosario" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Glosario</a>

                        <div role="separator" class="dropdown-divider"></div>
                        <a href="#implementacion" role="button" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi"><strong>IMPLEMENTACIÓN</strong></a>
                        <a href="#plan_integracion" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Plan de integración</a>
                        <a href="#estandar_programacion" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Estándar programación</a>

                        <div role="separator" class="dropdown-divider"></div>
                        <a href="#pruebas" role="button" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi"><strong>PRUEBAS</strong></a>
                        <a href="#caso_prueba" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Caso de prueba</a>
                        <a href="#pruebaaaa" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">... </a>

                        <div role="separator" class="dropdown-divider"></div>
                        <a href="#implantacion" role="button" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi"><strong>IMPLANTACIÓN</strong></a>
                        <a href="#manual_usuario" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Manual de usuario</a>
                        <a href="#manual_instalacion" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Manual de instalación</a>
                        <a href="#encuesta_satisfaccion" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Encuesta de satisfacción</a>

                        <div role="separator" class="dropdown-divider"></div>
                        <a href="#gestion_calidad" role="button" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi"><strong>GESTIÓN DE CALIDAD</strong></a>
                        <a href="#gestion_configuraciones" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Gestión de configuraciones</a>
                        <a href="#gestion_riesgos" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Gestión de riesgos</a>
                        <a href="#estandar_documentacion" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Estándar de documentación</a>

                        <div role="separator" class="dropdown-divider"></div>
                        <a href="#gestion_proyecto" role="button" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi"><strong>GESTIÓN DEL PROYECTO</strong></a>
                        <a href="#estimacion" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Estimación</a>
                        <a href="#plan_estimacion" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Plan de estimación</a>
                        <a href="#plan_iteracion" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Plan de iteración</a>
                        <a href="#plan_proyecto" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Plan de proyecto</a>
                        <a href="#resumen_reunion" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi">Resumen de reunión</a>

                        <div role="separator" class="dropdown-divider"></div>
                        <a href="#descargas" role="tab" data-toggle="tab" class="nav-link titulo-indice text-orange-guimi"><strong><i class="fas fa-download mr-1"></i>DESCARGAS</strong></a>
                    </nav>
                </aside>

                <article class="content_format col-md-8 col-md-push-4 mb-4">

                    <div data-spy="scroll" data-target="#nav-manual" data-offset="0">
                        <div class="tab-content">
                            <br class="my-4 simostrar">
                            <h2 id="titulo-manual">PROYECTO GUIMI</h2>
                            <hr>
                            
                            <!-- SECCIÓN: INTRODUCCIÓN PROYECTO GUIMI-->
                            <?php include_once('secciones/proyecto_guimi.php');?>
                            
                            <!-- SECCIÓN: PROPUESTA-->
                            <?php include_once('secciones/propuesta.php');?>

                            <!--SECCIÓN: REQUERIMIENTOS-->
                            <?php include_once('secciones/requerimientos.php');?>

                            <!--SECCIÓN: ANÁLISIS Y DISEÑO-->
                            <?php include_once('secciones/analisis_disenio.php');?>
                            
                            <!--SECCIÓN: IMPLEMENTACIÓN-->
                            <?php include_once('secciones/implementacion.php');?>

                            <!--SECCIÓN: PRUEBAS-->
                            <?php include_once('secciones/pruebas.php');?>
                            
                            <!--SECCIÓN: IMPLANTACIÓN-->
                            <?php include_once('secciones/implantacion.php');?>

                            <!--SECCIÓN: GESTIÓN DE CALIDAD-->
                            <?php include_once('secciones/gestion_calidad.php');?>
                            
                            <!--SECCIÓN: GESTIÓN DEL PROYECTO-->
                            <?php include_once('secciones/gestion_proyecto.php');?>

                            <!--SECCIÓN: DESCARGAS-->
                            <?php include_once('secciones/descargas.php');?>

                            <!--
                        <div id="nombredeseccion" class="tab-pane fade" role="tabpanel">
                            <h3>Título</h3>
                            <p class="text-justify">Lorem ipsum dolor sit amet...</p>

                            <figure class="figure">
                              <img src="{% static 'ayuda/img/imagen.png' %}" class="figure-img img-fluid borde-gris rounded" alt="[Descripcion]">
                              <figcaption class="figure-caption">Texto descriptivo</figcaption>
                            </figure>

                            <p class="text-justify">Lorem ipsum dolor sit amet...</p>
                            <ul>
                                <li>Ítem 1</li>
                                <li>Ítem 2</li>
                            </ul>

                           <div class="observacion observacion-info">
                            <p class="text-justify">
                            <strong>NOTA:</strong>
                                <span>Lorem ipsum... </span>
                            </p>
                            </div>

                        </div>
-->

                            <button class="ir-arriba btn btn-circle btn-secondary btn-md">
                                <span class="fas fa-chevron-up fa-2x mb-1"></span>
                            </button>

                        </div>
                    </div>
                </article>
            </div>
        </section>
    </div>

</main>
