<!--SECCIÓN: IMPLANTACIÓN-->

<div id="implantacion" class="tab-pane fade" role="tabpanel">
    <h3>Implantación</h3>
    <p class="text-justify">El análisis consiste en obtener una visión del sistema que se preocupa de ver qué hace, de modo que sólo se interesa por los requisitos funcionales. El diseño, por su parte, es un refinamiento del análisis que tiene en cuenta los requisitos no funcionales, es decir, cómo cumple el sistema sus objetivos.</p>
    <p class="text-justify">Con estos objetivos, se desarrollarán a continuación los siguientes documentos:</p>
    <ul>
        <li class="text-uppercase small">Manual de usuario</li>
        <li class="text-uppercase small">Manual de instalación</li>
        <li class="text-uppercase small">Encuesta de satisfacción</li>
    </ul>

    <div class="observacion observacion-info">
        <p class="text-justify">
            <span class="font-weight-bold mr-1">NOTA:</span>
            <span>Los documentos aquí presentados constituyen la versión final de los mismos, luego de haber sido modificados a lo largo del proyecto.</span>
        </p>
    </div>
</div>

<!-- Manual de usuario-->
<div id="manual_usuario" class="tab-pane fade" role="tabpanel">
    <h3>Manual de usuario</h3>
    <p class="text-justify">...</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>

<!-- Manual de instalación-->
<div id="manual_instalacion" class="tab-pane fade" role="tabpanel">
    <h3>Manual de instalación</h3>
    <p class="text-justify">...</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>

<!-- Encuesta de satisfacción-->
<div id="encuesta_satisfaccion" class="tab-pane fade" role="tabpanel">
    <h3>Encuesta de satisfacción</h3>
    <p class="text-justify">...</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>
