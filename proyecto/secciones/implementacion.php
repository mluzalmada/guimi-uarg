<!--SECCIÓN: IMPLEMENTACIÓN-->
<div id="implementacion" class="tab-pane fade" role="tabpanel">
    <h3>Implementación</h3>
    <p class="text-justify">El análisis consiste en obtener una visión del sistema que se preocupa de ver qué hace, de modo que sólo se interesa por los requisitos funcionales. El diseño, por su parte, es un refinamiento del análisis que tiene en cuenta los requisitos no funcionales, es decir, cómo cumple el sistema sus objetivos.</p>
    <p class="text-justify">Con estos objetivos, se desarrollarán a continuación los siguientes documentos:</p>
    <ul>
        <li class="text-uppercase small">Plan de Integración</li>
        <li class="text-uppercase small">Estándar de programación</li>
    </ul>

    <div class="observacion observacion-info">
        <p class="text-justify">
            <span class="font-weight-bold mr-1">NOTA:</span>
            <span>Los documentos aquí presentados constituyen la versión final de los mismos, luego de haber sido modificados a lo largo del proyecto.</span>
        </p>
    </div>
</div>

<!-- Plan de Integración-->
<div id="plan_integracion" class="tab-pane fade" role="tabpanel">
    <h3>Plan de Integración</h3>
    <p class="text-justify">...</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>

<!-- Estándar de programación-->
<div id="estandar_programacion" class="tab-pane fade" role="tabpanel">
    <h3>Estándar de programación</h3>
    <p class="text-justify">...</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>
