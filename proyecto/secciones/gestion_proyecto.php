<!--SECCIÓN: GESTIÓN DEL PROYECTO-->

<div id="gestion_proyecto" class="tab-pane fade" role="tabpanel">
    <h3>Gestión del proyecto</h3>
    <p class="text-justify">El análisis consiste en obtener una visión del sistema que se preocupa de ver qué hace, de modo que sólo se interesa por los requisitos funcionales. El diseño, por su parte, es un refinamiento del análisis que tiene en cuenta los requisitos no funcionales, es decir, cómo cumple el sistema sus objetivos.</p>
    <p class="text-justify">Con estos objetivos, se desarrollarán a continuación los siguientes documentos:</p>
    <ul>
        <li class="text-uppercase small">Estimación</li>
        <li class="text-uppercase small">Plan de estimación</li>
        <li class="text-uppercase small">Plan de iteración</li>
        <li class="text-uppercase small">Plan de proyecto</li>
        <li class="text-uppercase small">Resumen de reunión</li>
    </ul>

    <div class="observacion observacion-info">
        <p class="text-justify">
            <span class="font-weight-bold mr-1">NOTA:</span>
            <span>Los documentos aquí presentados constituyen la versión final de los mismos, luego de haber sido modificados a lo largo del proyecto.</span>
        </p>
    </div>
</div>

<!-- Estimación-->
<div id="estimacion" class="tab-pane fade" role="tabpanel">
    <h3>Estimación</h3>
    <p class="text-justify">...</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>

<!-- Plan de estimación-->
<div id="pan_estimacion" class="tab-pane fade" role="tabpanel">
    <h3>Plan de estimación</h3>
    <p class="text-justify">...</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>

<!-- Plan de iteración-->
<div id="plan_iteracion" class="tab-pane fade" role="tabpanel">
    <h3>Plan de iteración</h3>
    <p class="text-justify">El objetivo de este plan es definir detalladamente para cada una de las iteraciones a realizarse un conjunto de tareas, actividades y recursos, por tal motivo existirá para cada iteración del ciclo de vida del proyecto un artefacto de este tipo.</p>
    <p class="text-justify">
        Para cada iteración existe una serie de objetivos los cuales son usados como referencia de evaluación para determinar diferentes aspectos, como grado de terminación de una determinada función, rendimiento, niveles de calidad, etc.
    </p>

    <h5 class="text-uppercase font-weight-bold">Inicio</h5>
    <ul>
        <li>Inicio: Iteración 1 (28-Agosto-2018 al 18-Septiembre-2018)
            <button type="button" class="btn btn-outline-dark btn-sm mx-3" data-toggle="modal" data-target="#modal-iteracion01">
                <i class="fas fa-file-pdf fa-fw mr-1"></i>Ver documento
            </button>
        </li>
    </ul>
    <hr>
    <h5 class="text-uppercase font-weight-bold">Elaboración</h5>
    <ul>
        <li>Elaboración: Iteración 1 (19-Septiembre-2018)</li>
        <li>Elaboración: Iteración 2 (2018)</li>
    </ul>
    <hr>
    <h5 class="text-uppercase font-weight-bold">Construcción</h5>
    <ul>
        <li>Construcción: Iteración 1 (2018)</li>
        <li>Construcción: Iteración 2 (2018)</li>
        <li>Construcción: Iteración 3 (... al 30-Noviembre-2018)</li>

        <li>Construcción: Iteración 4 (8-Noviembre-2021 al 28-Noviembre-2021)</li>
        <li>Construcción: Iteración 5 (29-Noviembre-2021 al 19-Diciembre-2021)</li>
        <li>Construcción: Iteración 6 (17-Enero-2022 al 6-Febrero-2022)</li>
        <li>Construcción: Iteración 7 (7-Febrero-2022 al 20-Febrero-2022)</li>
        <li>Construcción: Iteración 8 (21-Febrero-2022 al 28-Febrero-2022)</li>
    </ul>
    <hr>
    <h5 class="text-uppercase font-weight-bold">Transición</h5>
    <ul>
        <li>Transición: Iteración 1 (21-Febrero-2022 al 28-Febrero-2022)</li>
    </ul>
    <hr>
    <h5 class="text-uppercase font-weight-bold">Seguimiento</h5>
    <ul>
        <li>Seguimiento: Iteración 1 (21-Febrero-2022 al 28-Febrero-2022)</li>
    </ul>
    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>

<!-- Plan de proyecto-->
<div id="plan_proyecto" class="tab-pane fade" role="tabpanel">
    <h3>Plan de proyecto</h3>
    <p class="text-justify">...</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>


<!-- Resumen de reunión-->
<div id="resumen_reunion" class="tab-pane fade" role="tabpanel">
    <h3>Resumen de reunión</h3>
    <p class="text-justify">...</p>
    <ul>
        <li>ítem 1</li>
    </ul>

    <p class="text-justify">
        <span class="font-weight-bold mr-1">descripción</span>
        Texto...
    </p>

    <div class="observacion observacion-info">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>item 1...</li>
        </ul>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="modal-iteracion01" tabindex="-1" aria-labelledby="label-iteracion01" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="label-iteracion01">Inicio: Iteración 1</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <embed src='../documentacion/Gesti%25C3%25B3n%2520del%2520proyecto/Planificaci%C3%B3n/Plan_Iteracion_Inicio_01.pdf' 'type=application/pdf' width="100%" height="600px">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
