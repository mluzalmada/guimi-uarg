<!--SECCIÓN: REQUERIMIENTOS-->

<div id="requerimientos" class="tab-pane fade" role="tabpanel">
    <h3>Requerimientos</h3>
    <p class="text-justify">Los requerimientos para un sistema son descripciones de lo que el sistema debe hacer: el servicio que ofrece y las restricciones en su operació. Tales requerimientos <span class="font-weight-bold">reflejan las necesidades de los clientes</span> por un sistema que atienda cierto propósito, como sería controlar un dispositivo, hacer pedidos en una tienda, o agregar realidad aumentada a los elementos de un museo.</p>
    <p class="text-justify">Con estos objetivos, se desarrollarán a continuación los siguientes documentos:</p>
    <ul>
        <li class="text-uppercase small">Resumen de entrevista</li>
        <li class="text-uppercase small">Reunión de requerimientos</li>
        <li class="text-uppercase small">Especificación de requerimientos</li>
    </ul>

    <div class="observacion observacion-info">
        <p class="text-justify text-90">
            <span class="font-weight-bold mr-1">NOTA:</span>
            <span>Los documentos aquí presentados constituyen la versión final de los mismos, luego de haber sido modificados a lo largo del proyecto.</span>
        </p>
    </div>
</div>
<!-- resumen_entrevista-->
<div id="resumen_entrevista" class="tab-pane fade" role="tabpanel">
    <h3>Resumen de entrevista</h3>
    <p class="text-justify">Las entrevistas resultan una técnica muy aceptada dentro de la ingeniería de requisitos y su uso está ampliamente extendido.</p>
    <p class="text-justify">Estas le permiten al analista tomar conocimiento del problema y comprender los objetivos de la solución buscada. A través de esta técnica el equipo de trabajo se acerca al problema de una forma natural.</p>
    <ul>
        <li>
            <span class="font-weight-bold mr-1">Entrevista 1:</span>
            30-Agosto-2018
            <button type="button" class="btn btn-outline-dark btn-sm mx-3" data-toggle="modal" data-target="#modal-entrevista01">
                <i class="fas fa-file-pdf fa-fw mr-1"></i>Ver documento
            </button>
        </li>
        <li>
            <span class="font-weight-bold mr-1">Entrevista 2:</span>
            11-Septiembre-2018
        </li>
        <li>
            <span class="font-weight-bold mr-1">Entrevista 3:</span>
            28-Septiembre-2018
        </li>
    </ul>

    <div class="observacion observacion-info text-90">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>
        <ul>
            <li>Si bien se realizaron otros encuentros (informales) durante el segundo cuatrimestre de 2018, estos no fueron documentados en un Resumen de entrevista. El objetivo de los mismos fue resolver dudas puntuales y/o comentar brevemente el proyecto que planteamos a otros participantes del museo (<a href="https://bit.ly/3segXLz" target="_blank">Programa de intercambio del Smithsonian Institution</a>, por ejemplo).</li>
            <li>Dado que el cliente del proyecto GuIMI coincidió con el equipo de cátedra de esta asignatura, pudimos continuar en comunicación con ellos, comentando a veces los avances que realizamos una vez finalizada la cursada.</li>
        </ul>
    </div>
</div>

<!--reunion_requerimientos-->
<div id="reunion_requerimientos" class="tab-pane fade" role="tabpanel">
    <h3>Reunión de requerimientos</h3>
    <p class="text-justify">El objetivo de este documento es el de documentar todos los requerimientos del sistema, este describe las funciones del sistema, los requerimientos no funcionales, características del diseño, y otros elementos necesarios para proporcionar una descripción completa y comprensiva de los requerimientos para el software a desarrollar.</p>
    <p class="text-justify">Los requerimientos pueden ser levantados con diferentes herramientas, también se pueden encontrar dispersos en varios artefactos y herramientas. Es por ello, que esta metodología propone capturar todos los requerimientos en el documento de <span class="font-weight-bold">Reunión de requerimientos</span></p>

    <div class="observacion observacion-info text-90">
        <p class="text-justify font-weight-bold mr-1">NOTA:</p>

        <p class="text-justify">En el caso de GuIMI, las fuentes de información en el proceso de adquisición de requerimientos incluyeron interacción con los participantes a través de entrevistas y observaciones. Se usaron, además, prototipos de interfaz (no funcionales) para ayudar a los participantes a entender cómo sería el sistema.</p>
        <ul>
            <li><span class="font-weight-bold mr-1">Diferentes puntos de vista:</span>Por lo general, los participantes en un sistema pueden ser administradores, usuarios finales, y hasta participantes externos. Al adquirir los requerimientos, además de los participantes, se debe tener en cuenta el dominio de aplicación y de otros sistemas con los que deberá interactuar. Estas diferentes fuentes de requerimientos representan diferentes puntos de vista.</li>
            <li><span class="font-weight-bold mr-1">No saben qué quieren:</span>Los participantes con frecuencia no saben lo que quieren de un sistema de cómputo, excepto en términos muy generales; por ello, puede resultar difícil articular qué quieren que haga el sistema.</li>
        </ul>
        <p class="text-justify mr-1">Como el contacto con los participantes, en este caso, se redujo a los docentes de la cátedra, no se presentaron múltiples puntos de vista ni conflictos de intereses al plantear los requisitos. Además, como los clientes son profesionales del área del software tampoco se presentó la dificultad de entender los requerimientos que plantearon ni se hicieron peticiones inalcanzables.</p>
        <p class="text-justify mr-1">Por estos motivos, consideramos que no era necesaria una Reunión de requerimientos, ya que los mismos quedaron explícitos y claros luego de las primeras entrevistas.</p>
    </div>
</div>

<!--especificacion_requerimientos-->
<div id="especificacion_requerimientos" class="tab-pane fade" role="tabpanel">
    <h3>Especificación de requerimientos</h3>
    <p class="text-justify">La Especificación de Requisitos de Software describe completamente el comportamiento externo de la aplicación o sistema identificado. También describe requerimientos no funcionales, restricciones de diseño y factores necesarios que den una descripción comprensiva de los requerimientos para el software.</p>
    <p class="text-justify">En el siguiente documento se controla la evolución del sistema durante todo el ciclo de desarrollo del proyecto, cuando las nuevas características son añadidas o modificadas al artefacto de visión, son aclarados dentro del mismo.</p>
    <button type="button" class="btn btn-outline-dark my-3" data-toggle="modal" data-target="#modal-especificacion_requerimientos">
        <i class="fas fa-file-pdf fa-fw mr-1"></i>Ver documento
    </button>
</div>


<!-- Modal -->
<div class="modal fade" id="modal-entrevista01" tabindex="-1" aria-labelledby="label-entrevista01" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="label-entrevista01">Entrevista 1</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <embed src='../documentacion/Requerimientos/Entrevistas/Resumen%20de%20Entrevista_2018-08-30.pdf' 'type=application/pdf' width="100%" height="600px">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modal-especificacion_requerimientos" tabindex="-1" aria-labelledby="label-especificacion_requerimientos" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="label-especificacion_requerimientos">Especificación de Requerimientos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <embed src='../documentacion/Requerimientos/Especificacion%20de%20Requerimientos.pdf' 'type=application/pdf' width="100%" height="600px">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
