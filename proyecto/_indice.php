<html>

<head>
    <meta charset="UTF-8">
    <?php include_once('include/headers.php'); ?>
    <title><?php echo NOMBRE_SISTEMA; ?> - Proceso de Desarrollo</title>
</head>

<body class="sticky-footer">

    <?php include_once('include/navbar.php'); ?>

    <div class="container mt-4">
        <?php
        include_once('estructura.php');
        ?>
    </div>
    <?php include_once('include/footer.php'); ?>
    <script type="application/javascript" src="../recursos/js/funciones_proyecto.min.js"></script>
</body>

</html>
